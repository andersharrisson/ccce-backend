package eu.ess.ics.ccce.util;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.mockserver.client.MockServerClient;
import org.mockserver.configuration.ConfigurationProperties;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.matchers.Times;
import org.mockserver.model.Header;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.Parameter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public abstract class ExternalServiceMock {
    private static ClientAndServer mockServer;
    protected static final String TEST_URL = "http://localhost:1080";
    private final String resourcePath;

    public ExternalServiceMock(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    @BeforeAll
    public static void startServer() {
        ConfigurationProperties.logLevel("ERROR");
        ConfigurationProperties.disableSystemOut(true);
        mockServer = ClientAndServer.startClientAndServer(1080);
    }

    @AfterAll
    public static void stopServer() {
        mockServer.stop();
        while (!mockServer.hasStopped(3,100L, TimeUnit.MILLISECONDS)){}
    }

    @AfterEach
    public void clean() {
        mockServer.reset();
    }

    protected void createExpectationFail(final String method, final String postfix, final List<Parameter> parameters, int statusCode) {
        new MockServerClient("localhost", 1080)
                .when(
                        HttpRequest.request()
                                .withMethod(method)
                                .withPath(postfix)
                                .withQueryStringParameters(parameters), Times.exactly(1))
                .respond(
                        HttpResponse.response()
                                .withStatusCode(statusCode)
                                .withDelay(TimeUnit.MILLISECONDS,1)
                );
    }

    protected void createGetExpectationSuccess(final String postfix, final List<Parameter> parameters, final String resource) {
        new MockServerClient("localhost", 1080)
                .when(
                        HttpRequest.request()
                                .withMethod("GET")
                                .withPath(postfix)
                                .withQueryStringParameters(parameters), Times.exactly(1))
                .respond(
                        HttpResponse.response()
                                .withStatusCode(200)
                                .withHeaders(
                                        new Header("Content-Type", "application/json; charset=utf-8"))
                                .withBody(getResponseBody(resource))
                                .withDelay(TimeUnit.MILLISECONDS,1)
                );
    }

    protected void createPostExpectationSuccess(final String postfix, final List<Parameter> parameters, final String body, final String resource, final int statusCode) {
        new MockServerClient("localhost", 1080)
                .when(
                        HttpRequest.request()
                                .withMethod("POST")
                                .withBody(body)
                                .withPath(postfix)
                                .withQueryStringParameters(parameters), Times.exactly(1))
                .respond(
                        HttpResponse.response()
                                .withStatusCode(statusCode)
                                .withHeaders(
                                        new Header("Content-Type", "application/json; charset=utf-8"))
                                .withBody(getResponseBody(resource))
                                .withDelay(TimeUnit.MILLISECONDS,1)
                );
    }

    private String getResponseBody(String fileName) {
        try {
            return IOUtils.toString(
                    this.getClass().getResourceAsStream(resourcePath + "/" + fileName),
                    StandardCharsets.UTF_8
            );
        } catch (IOException e) {
            return null;
        }
    }
}
