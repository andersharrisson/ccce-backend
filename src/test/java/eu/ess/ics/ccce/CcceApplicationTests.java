package eu.ess.ics.ccce;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Tag("IntegrationTest")
class CcceApplicationTests {

	@Test
	void contextLoads() {
	}

}
