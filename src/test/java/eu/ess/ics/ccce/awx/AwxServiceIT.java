package eu.ess.ics.ccce.awx;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.awx.model.Command;
import eu.ess.ics.ccce.awx.model.CommandDetails;
import eu.ess.ics.ccce.awx.model.CommandEvent;
import eu.ess.ics.ccce.awx.model.Credential;
import eu.ess.ics.ccce.awx.model.Inventory;
import eu.ess.ics.ccce.awx.model.Job;
import eu.ess.ics.ccce.awx.model.JobDetails;
import eu.ess.ics.ccce.awx.model.JobEvent;
import eu.ess.ics.ccce.awx.model.JobHostSummary;
import eu.ess.ics.ccce.awx.model.JobStatus;
import eu.ess.ics.ccce.awx.model.JobTemplate;
import eu.ess.ics.ccce.awx.service.AwxService;
import eu.ess.ics.ccce.exceptions.AwxException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.util.ExternalServiceMock;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockserver.model.Parameter;
import org.springframework.core.env.Environment;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@ExtendWith(MockitoExtension.class)
@Tag("IntegrationTest")
public class AwxServiceIT extends ExternalServiceMock {
    private AwxService awxService;
    @Mock
    private Environment environment;

    public AwxServiceIT() {
        super("/awx.response");
    }

    @BeforeEach
    public void setup() {
        Mockito.when(environment.getProperty(AwxService.AWX_URL)).thenReturn(ExternalServiceMock.TEST_URL);
        awxService = new AwxService(environment);
    }

    @Test
    void getJobTemplatesSuccess() throws AwxException {
        createGetExpectationSuccess("/api/v2/job_templates", ImmutableList.of(new Parameter("name", "ccce-dummy-playbook")), "templates.json");
        JobTemplate dummyPlaybook = awxService.getJobTemplate("ccce-dummy-playbook");
        assertNotNull(dummyPlaybook);
        assertEquals("ccce-dummy-playbook", dummyPlaybook.getName());
        assertEquals("/api/v2/job_templates/26/", dummyPlaybook.getUrl());
        assertNotNull(dummyPlaybook.getRelated());
        assertEquals("/api/v2/job_templates/26/launch/", dummyPlaybook.getRelated().getLaunch());
    }

    @Test
    void getJobTemplatesFail() {
        createExpectationFail("GET", "/api/v2/job_templates", ImmutableList.of(new Parameter("name", "ccce-dummy-playbook")), 401);
        assertThrows(AwxException.class, () -> awxService.getJobTemplate("ccce-dummy-playbook"));
    }

    @Test
    void launchJobTemplatesSuccess() throws AwxException {
        String limit = "ccce-demo-1.esss.lu.se";
        createPostExpectationSuccess("/api/v2/job_templates/26/launch", Collections.emptyList(), null, "launch_job.json", 201);

        Job job = awxService.launchJob("/api/v2/job_templates/26/launch", limit);
        assertNotNull(job);
        assertEquals(952, job.getId());
        assertEquals("job", job.getType());
        assertEquals(952, job.getJob());
        assertEquals("/api/v2/jobs/952/", job.getUrl());
        assertEquals("run", job.getJobType());
        assertEquals(JobStatus.PENDING, job.getStatus());
        assertNull(job.getStarted());
        assertNull(job.getFinished());
        assertNotNull(job.getRelated());
        assertEquals("/api/v2/jobs/952/cancel/", job.getRelated().getCancel());
        assertEquals("/api/v2/jobs/952/relaunch/", job.getRelated().getRelaunch());
    }

    @Test
    void launchJobTemplatesFail() {
        String limit = "ccce-demo-1.esss.lu.se";
        createExpectationFail("POST","/api/v2/job_templates/26/launch/", Collections.emptyList(), 401);
        assertThrows(AwxException.class, () -> awxService.launchJob("/api/v2/job_templates/26/launch/", limit));
    }

    @Test
    void getJobDetailsSuccess() throws EntityNotFoundException, AwxException {
        createGetExpectationSuccess( "/api/v2/jobs/856", Collections.emptyList(), "job_details.json");
        createGetExpectationSuccess("/api/v2/jobs/856/job_events/", Collections.emptyList(), "job_events.json");
        createGetExpectationSuccess("/api/v2/jobs/856/job_host_summaries/", Collections.emptyList(), "job_host_summaries.json");
        createGetExpectationSuccess("/api/v2/hosts", ImmutableList.of(new Parameter("id", "9")), "host.json");
        createGetExpectationSuccess("/api/v2/jobs/856/stdout/", ImmutableList.of(new Parameter("format", "html")), "job_stdout.html");
        createGetExpectationSuccess("/api/v2/hosts/9/ansible_facts/", Collections.emptyList(), "ansible_facts.json");

        JobDetails details = awxService.getJobDetails(856);
        assertNotNull(details.getJob());
        assertEquals(856, details.getJob().getId());
        assertEquals(JobStatus.SUCCESSFUL, details.getJob().getStatus());
        assertNotNull(details.getEvents());
        // Events query is removed temporary: not used by front-end
//        assertEquals(11, details.getEvents().size());
        for (JobEvent event : details.getEvents()) {
            assertEquals("job_event", event.getType());
            assertNotNull(event.getCreated());
        }
//        JobEvent event = details.getEvents().get(9);
//        assertEquals(34222, event.getId());
//        assertEquals("ccce-demo-04.cslab.esss.lu.se", event.getHostName());
//        assertEquals("Print debug message", event.getTask());
//        assertEquals(new DateTime(2021, 3, 4, 9, 10, 25, 295, DateTimeZone.UTC).toDate(),
//                event.getCreated());
//        assertEquals(new DateTime(2021, 3, 4, 9, 10, 25, 302, DateTimeZone.UTC).toDate(), event.getModified());
//        assertEquals("Host Started", event.getEventDisplay());
//        assertNotNull(event.getStdout());

        assertNotNull(details.getStdout());
        assertNotNull(details.getHostSummaries());
        assertEquals(1, details.getHostSummaries().size());
        for (JobHostSummary summary : details.getHostSummaries()) {
            assertNotEquals(0, summary.getId());
            assertEquals("job_host_summary", summary.getType());
            assertNotNull(summary.getCreated());
        }
        JobHostSummary hostSummary = details.getHostSummaries().get(0);
        assertEquals(267, hostSummary.getId());
        assertEquals("ccce-demo-04.cslab.esss.lu.se", hostSummary.getHostName());
        assertNull(hostSummary.getHost());
        assertEquals(new DateTime(2021, 3, 4, 9, 10, 25, 512, DateTimeZone.UTC).toDate(), hostSummary.getCreated());
        assertEquals(new DateTime(2021, 3, 4, 9, 10, 25, 512, DateTimeZone.UTC).toDate(), hostSummary.getModified());
        assertEquals(2, hostSummary.getOk());
        assertEquals(856, hostSummary.getJob());
        assertEquals(0, hostSummary.getChanged());
        assertEquals(0, hostSummary.getDark());
        assertEquals(0, hostSummary.getFailures());
        assertEquals(1, hostSummary.getProcessed());
        assertEquals(0, hostSummary.getIgnored());
        assertEquals(0, hostSummary.getRescued());
        assertFalse(hostSummary.isFailed());

        assertNotNull(hostSummary.getSummaryFields().getHost());
        assertEquals(9, hostSummary.getSummaryFields().getHost().getId());
        assertEquals("ccce-demo-04.cslab.esss.lu.se", hostSummary.getSummaryFields().getHost().getName());
        assertEquals("imported", hostSummary.getSummaryFields().getHost().getDescription());

        assertNotNull(hostSummary.getSummaryFields().getJob());
        assertEquals(856, hostSummary.getSummaryFields().getJob().getId());
        assertEquals("ccce-dummy-playbook", hostSummary.getSummaryFields().getJob().getName());
        assertEquals("Dummy", hostSummary.getSummaryFields().getJob().getDescription());
        assertEquals("successful", hostSummary.getSummaryFields().getJob().getStatus());
        assertFalse(hostSummary.getSummaryFields().getJob().isFailed());
        assertEquals(3.565, hostSummary.getSummaryFields().getJob().getElapsed());
        assertEquals("job", hostSummary.getSummaryFields().getJob().getType());
        assertEquals(26, hostSummary.getSummaryFields().getJob().getJobTemplateId());
        assertEquals("ccce-dummy-playbook", hostSummary.getSummaryFields().getJob().getJobTemplateName());

        assertNotNull(details.getIocDeploymentResults());
        assertTrue(details.getIocDeploymentResults().containsKey("TS2-010CRM:Cryo-PLC-001"));
        assertTrue(details.getIocDeploymentResults().get("TS2-010CRM:Cryo-PLC-001").containsKey("successful"));
        assertTrue(Boolean.parseBoolean(details.getIocDeploymentResults().get("TS2-010CRM:Cryo-PLC-001").get("successful").toString()));
        assertTrue(details.getIocDeploymentResults().containsKey("AccPss:PSS-IOC-1"));
        assertTrue(details.getIocDeploymentResults().get("AccPss:PSS-IOC-1").containsKey("successful"));
        assertFalse(Boolean.parseBoolean(details.getIocDeploymentResults().get("AccPss:PSS-IOC-1").get("successful").toString()));
    }

    @Test
    void getJobDetailsNoIocDeploymentResultSuccess() throws EntityNotFoundException, AwxException {
        createGetExpectationSuccess( "/api/v2/jobs/856", Collections.emptyList(), "job_details.json");
        createGetExpectationSuccess("/api/v2/jobs/856/job_events/", Collections.emptyList(), "job_events.json");
        createGetExpectationSuccess("/api/v2/jobs/856/job_host_summaries/", Collections.emptyList(), "job_host_summaries.json");
        createGetExpectationSuccess("/api/v2/hosts", ImmutableList.of(new Parameter("id", "9")), "host.json");
        createGetExpectationSuccess("/api/v2/jobs/856/stdout/", ImmutableList.of(new Parameter("format", "html")), "job_stdout.html");
        createGetExpectationSuccess("/api/v2/hosts/9/ansible_facts/", Collections.emptyList(), "ansible_facts_no_result.json");

        JobDetails details = awxService.getJobDetails(856);
        assertNotNull(details.getJob());
        assertEquals(856, details.getJob().getId());
        assertEquals(JobStatus.SUCCESSFUL, details.getJob().getStatus());
        assertNotNull(details.getEvents());
        // Events query is removed temporary: not used by front-end
//        assertEquals(11, details.getEvents().size());
        for (JobEvent event : details.getEvents()) {
            assertEquals("job_event", event.getType());
            assertNotNull(event.getCreated());
        }
//        JobEvent event = details.getEvents().get(9);
//        assertEquals(34222, event.getId());
//        assertEquals("ccce-demo-04.cslab.esss.lu.se", event.getHostName());
//        assertEquals("Print debug message", event.getTask());
//        assertEquals(new DateTime(2021, 3, 4, 9, 10, 25, 295, DateTimeZone.UTC).toDate(),
//                event.getCreated());
//        assertEquals(new DateTime(2021, 3, 4, 9, 10, 25, 302, DateTimeZone.UTC).toDate(), event.getModified());
//        assertEquals("Host Started", event.getEventDisplay());
//        assertNotNull(event.getStdout());

        assertNotNull(details.getStdout());
        assertNotNull(details.getHostSummaries());
        assertEquals(1, details.getHostSummaries().size());
        for (JobHostSummary summary : details.getHostSummaries()) {
            assertNotEquals(0, summary.getId());
            assertEquals("job_host_summary", summary.getType());
            assertNotNull(summary.getCreated());
        }
        JobHostSummary hostSummary = details.getHostSummaries().get(0);
        assertEquals(267, hostSummary.getId());
        assertEquals("ccce-demo-04.cslab.esss.lu.se", hostSummary.getHostName());
        assertNull(hostSummary.getHost());
        assertEquals(new DateTime(2021, 3, 4, 9, 10, 25, 512, DateTimeZone.UTC).toDate(), hostSummary.getCreated());
        assertEquals(new DateTime(2021, 3, 4, 9, 10, 25, 512, DateTimeZone.UTC).toDate(), hostSummary.getModified());
        assertEquals(2, hostSummary.getOk());
        assertEquals(856, hostSummary.getJob());
        assertEquals(0, hostSummary.getChanged());
        assertEquals(0, hostSummary.getDark());
        assertEquals(0, hostSummary.getFailures());
        assertEquals(1, hostSummary.getProcessed());
        assertEquals(0, hostSummary.getIgnored());
        assertEquals(0, hostSummary.getRescued());
        assertFalse(hostSummary.isFailed());

        assertNotNull(hostSummary.getSummaryFields().getHost());
        assertEquals(9, hostSummary.getSummaryFields().getHost().getId());
        assertEquals("ccce-demo-04.cslab.esss.lu.se", hostSummary.getSummaryFields().getHost().getName());
        assertEquals("imported", hostSummary.getSummaryFields().getHost().getDescription());

        assertNotNull(hostSummary.getSummaryFields().getJob());
        assertEquals(856, hostSummary.getSummaryFields().getJob().getId());
        assertEquals("ccce-dummy-playbook", hostSummary.getSummaryFields().getJob().getName());
        assertEquals("Dummy", hostSummary.getSummaryFields().getJob().getDescription());
        assertEquals("successful", hostSummary.getSummaryFields().getJob().getStatus());
        assertFalse(hostSummary.getSummaryFields().getJob().isFailed());
        assertEquals(3.565, hostSummary.getSummaryFields().getJob().getElapsed());
        assertEquals("job", hostSummary.getSummaryFields().getJob().getType());
        assertEquals(26, hostSummary.getSummaryFields().getJob().getJobTemplateId());
        assertEquals("ccce-dummy-playbook", hostSummary.getSummaryFields().getJob().getJobTemplateName());

        assertNotNull(details.getIocDeploymentResults());
        assertTrue(details.getIocDeploymentResults().isEmpty());
    }

    @Test
    void getJobDetailsFail() throws EntityNotFoundException {
        createExpectationFail("GET", "/api/v2/jobs/856", Collections.emptyList(), 404);
        assertThrows(EntityNotFoundException.class, () -> awxService.getJobDetails(856));
    }

    @Test
    void getInventorySuccess() throws AwxException {
        createGetExpectationSuccess("/api/v2/inventories", ImmutableList.of(new Parameter("name", "test-inventory")), "inventory.json");
        Inventory inventory = awxService.getInventory("test-inventory");
        assertNotNull(inventory);
        assertEquals(8, inventory.getId());
        assertEquals("test-inventory", inventory.getName());
    }

    @Test
    void getInventoryFail() {
        createGetExpectationSuccess("/api/v2/inventories", ImmutableList.of(new Parameter("name", "test-inventory")), "awx_not_found_empty_array.json");
        assertThrows(EntityNotFoundException.class, () -> awxService.getInventory("test-inventory"));
    }

    @Test
    void getCredentialSuccess() throws AwxException {
        createGetExpectationSuccess("/api/v2/credentials", ImmutableList.of(new Parameter("name", "demo")), "credentail.json");
        Credential credential = awxService.getCredential("demo");
        assertNotNull(credential);
        assertEquals(1, credential.getId());
        assertEquals("demo", credential.getName());
    }

    @Test
    void getCredentialFail() {
        createGetExpectationSuccess("/api/v2/credentials", ImmutableList.of(new Parameter("name", "demo")), "awx_not_found_empty_array.json");
        assertThrows(EntityNotFoundException.class, () -> awxService.getCredential("demo"));
    }

    @Test
    void launchAdHocCommandSuccess() throws AwxException {
        String limit = "ccce-demo-1.esss.lu.se";
        String moduleName = "module";
        String moduleArgs = "arg";
        Inventory inventory = new Inventory();
        inventory.setId(1);
        Credential credential = new Credential();
        credential.setId(1);

        String body = "{\"job_type\":\"run\",\"inventory\":" + inventory.getId() + ",\"limit\":\"" + limit + "\",\"credential\":" + credential.getId() + ",\"module_name\":\"" + moduleName + "\",\"module_args\":\"" + moduleArgs + "\",\"forks\":0,\"verbosity\":3,\"become_enabled\":true,\"diff_mode\":false}";
        createPostExpectationSuccess("/api/v2/ad_hoc_commands/", Collections.emptyList(), body, "launch_command.json", 201);

        Command command = awxService.launchAdHocCommand(inventory, limit, credential, moduleName, moduleArgs, null);
        assertNotNull(command);
        assertEquals(1225, command.getId());
        assertEquals("ad_hoc_command", command.getType());
        assertEquals("/api/v2/ad_hoc_commands/1225/", command.getUrl());
    }

    @Test
    void launchAdHocCommandFail() {
        String limit = "ccce-demo-1.esss.lu.se";
        String moduleName = "module";
        String moduleArgs = "arg";
        Inventory inventory = new Inventory();
        inventory.setId(1);
        Credential credential = new Credential();
        credential.setId(1);
        createExpectationFail("POST","/api/v2/ad_hoc_commands/", Collections.emptyList(), 401);
        assertThrows(AwxException.class, () -> awxService.launchAdHocCommand(inventory, limit, credential, moduleName, moduleArgs, null));
    }

    @Test
    void getCommandDetailsSuccess() throws AwxException {
        createGetExpectationSuccess( "/api/v2/ad_hoc_commands/1225", Collections.emptyList(), "command_details.json");
        createGetExpectationSuccess("/api/v2/ad_hoc_commands/1225/events/", Collections.emptyList(), "command_events.json");
        createGetExpectationSuccess("/api/v2/ad_hoc_commands/1225/stdout/", ImmutableList.of(new Parameter("format", "html")), "command_stdout.html");

        CommandDetails details = awxService.getCommandDetails(1225);
        assertNotNull(details.getCommand());
        assertEquals(1225, details.getCommand().getId());
        assertEquals("successful", details.getCommand().getStatus());
        assertNotNull(details.getEvents());
        assertEquals(2, details.getEvents().size());
        for (CommandEvent event : details.getEvents()) {
            assertEquals("ad_hoc_command_event", event.getType());
            assertNotNull(event.getCreated());
        }
    }

    @Test
    void getCommandDetailsFail() throws EntityNotFoundException {
        createExpectationFail("GET", "/api/v2/ad_hoc_commands/1225", Collections.emptyList(), 404);
        assertThrows(EntityNotFoundException.class, () -> awxService.getCommandDetails(1225));
    }
}
