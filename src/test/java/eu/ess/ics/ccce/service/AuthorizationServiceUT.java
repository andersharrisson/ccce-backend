package eu.ess.ics.ccce.service;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.exceptions.UnauthorizedException;
import eu.ess.ics.ccce.model.UserDetails;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.Assert.assertThrows;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@ExtendWith(MockitoExtension.class)
public class AuthorizationServiceUT {
    private static final String PROJECT_URL_1 = "https://gitlab.esss.lu.se/ccce/dev/e3-ioc-ccce-example-1.git";
    private static final String PROJECT_URL_2 = "https://gitlab.esss.lu.se/ccce/dev/e3-ioc-ccce-example-2.git";

    @Mock
    private GitLabService gitLabService;

    @Test
    public void authorizeIocSuccess() {
        UserDetails user1 = new UserDetails();
        user1.setUserName("user1");
        Mockito.when(gitLabService.getAllProjectMembers(PROJECT_URL_1)).thenReturn(ImmutableList.of("user1", "user2"));
        AuthorizationService authorizationService = createService();
        authorizationService.authorizeIoc(PROJECT_URL_1, user1);
    }

    @Test
    public void authorizeIocFail() {
        UserDetails user1 = new UserDetails();
        user1.setUserName("user1");
        Mockito.when(gitLabService.getAllProjectMembers(PROJECT_URL_1)).thenReturn(ImmutableList.of("user2", "user3"));
        AuthorizationService authorizationService = createService();
        assertThrows(UnauthorizedException.class, () -> authorizationService.authorizeIoc(PROJECT_URL_1, user1));
    }

    @Test
    public void authorizeIocPostfixUserNameSuccess() {
        UserDetails user1 = new UserDetails();
        user1.setUserName("user");
        Mockito.when(gitLabService.getAllProjectMembers(PROJECT_URL_1)).thenReturn(ImmutableList.of("user1", "test"));
        AuthorizationService authorizationService = createService();
        authorizationService.authorizeIoc(PROJECT_URL_1, user1);
    }

    @Test
    public void authorizeIocsSuccess() {
        UserDetails user1 = new UserDetails();
        user1.setUserName("user1");
        Mockito.when(gitLabService.getAllProjectMembers(PROJECT_URL_1)).thenReturn(ImmutableList.of("user1", "user2"));
        Mockito.when(gitLabService.getAllProjectMembers(PROJECT_URL_2)).thenReturn(ImmutableList.of("user1", "user3"));
        AuthorizationService authorizationService = createService();
        authorizationService.authorizeIocs(ImmutableList.of(PROJECT_URL_1, PROJECT_URL_2), user1);
    }

    @Test
    public void authorizeIocsFail() {
        UserDetails user2 = new UserDetails();
        user2.setUserName("user2");
        Mockito.when(gitLabService.getAllProjectMembers(PROJECT_URL_1)).thenReturn(ImmutableList.of("user1", "user2"));
        Mockito.when(gitLabService.getAllProjectMembers(PROJECT_URL_2)).thenReturn(ImmutableList.of("user1", "user3"));
        AuthorizationService authorizationService = createService();
        assertThrows(UnauthorizedException.class,
                () -> authorizationService.authorizeIocs(ImmutableList.of(PROJECT_URL_1, PROJECT_URL_2), user2));
    }

    private AuthorizationService createService() {
        return new AuthorizationService(gitLabService);
    }
}
