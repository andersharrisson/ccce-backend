package eu.ess.ics.ccce.service;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import eu.ess.ics.ccce.awx.model.Job;
import eu.ess.ics.ccce.awx.model.JobStatus;
import eu.ess.ics.ccce.awx.model.JobTemplate;
import eu.ess.ics.ccce.awx.service.AwxService;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.repo.IDeploymentRepository;
import eu.ess.ics.ccce.repo.IHostRepository;
import eu.ess.ics.ccce.repo.model.AwxJobEntity;
import eu.ess.ics.ccce.repo.model.DeploymentEntity;
import eu.ess.ics.ccce.repo.model.DeploymentJobMappingEntity;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.HostMappingEntity;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import eu.ess.ics.ccce.rest.model.Deployment;
import eu.ess.ics.ccce.rest.model.DeploymentStatus;
import eu.ess.ics.ccce.rest.model.request.DeploymentOrder;
import eu.ess.ics.ccce.rest.model.response.PagedDeploymentResponse;
import eu.ess.ics.ccce.service.deployment.DeploymentTask;
import eu.ess.ics.ccce.util.PagingLimitDto;
import eu.ess.ics.ccce.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@ExtendWith(MockitoExtension.class)
public class DeploymentServiceUT {
    @Mock
    private Utils utils;
    @Mock
    private IDeploymentRepository deploymentRepository;
    @Mock
    private IHostRepository hostRepository;
    @Mock
    private IocVersionService iocVersionService;
    @Mock
    private HostService hostService;
    @Mock
    private AwxService awxService;
    @Mock
    private AuthorizationService authorizationService;

    private final HostEntity host1;
    private final HostEntity host2;
    private final IocEntity ioc1;
    private final IocEntity ioc2;
    private final IocVersionEntity iocVersion1;
    private final IocVersionEntity iocVersion2;
    private final DeploymentEntity deploymentEntityActive1;
    private final DeploymentEntity deploymentEntityActive2;
    private final DeploymentEntity deploymentEntityIntended1;
    private final DeploymentEntity deploymentEntityIntended2;
    private final HostMappingEntity hostMapping1;
    private final HostMappingEntity hostMapping2;
    private final Job job1;
    private final AwxJobEntity jobEntity1;

    DeploymentServiceUT() {
        host1 = new HostEntity();
        host1.setId(1L);
        host1.setHost("demo-1.esss.lu.se");
        host1.setNetwork("CSLab");
        host1.setCsEntryId(10L);

        host2 = new HostEntity();
        host2.setId(2L);
        host2.setHost("demo-2.esss.lu.se");
        host2.setNetwork("CSLab");
        host2.setCsEntryId(20L);

        ioc1 = new IocEntity();
        ioc1.setCustomName("IOC-1");
        ioc1.setId(1L);

        ioc2 = new IocEntity();
        ioc2.setCustomName("IOC-2");
        ioc2.setId(2L);

        iocVersion1 = new IocVersionEntity();
        iocVersion1.setId(1L);
        iocVersion1.setIoc(ioc1);
        iocVersion1.setNamingName("TEST-IOC-1");
        iocVersion1.setEpicsVersion("1.0.0");
        iocVersion1.setRequireVersion("2.0.0");

        iocVersion2 = new IocVersionEntity();
        iocVersion2.setId(2L);
        iocVersion2.setIoc(ioc2);
        iocVersion2.setNamingName("TEST-IOC-2");
        iocVersion2.setEpicsVersion("1.0.0");
        iocVersion2.setRequireVersion("2.0.0");

        hostMapping1 = new HostMappingEntity();
        hostMapping1.setId(1L);
        hostMapping1.setHost(host1);
        hostMapping1.setIoc(ioc1);
        hostMapping1.setCreatedBy("testuser");
        hostMapping1.setCreatedAt(new Date());

        hostMapping2 = new HostMappingEntity();
        hostMapping2.setId(2L);
        hostMapping2.setHost(host2);
        hostMapping2.setIoc(ioc2);
        hostMapping2.setCreatedBy("testuser");
        hostMapping2.setCreatedAt(new Date());

        deploymentEntityActive1 = new DeploymentEntity();
        deploymentEntityActive1.setId(1L);
        deploymentEntityActive1.setIocVersion(iocVersion1);
        deploymentEntityActive1.setActive(true);
        deploymentEntityActive1.setIntended(false);
        deploymentEntityActive1.setHostMapping(hostMapping1);
        deploymentEntityActive1.setDeploymentState(DeploymentStatus.SUCCESSFUL.name());

        deploymentEntityActive2 = new DeploymentEntity();
        deploymentEntityActive2.setId(2L);
        deploymentEntityActive2.setIocVersion(iocVersion2);
        deploymentEntityActive2.setActive(true);
        deploymentEntityActive2.setIntended(false);
        deploymentEntityActive2.setHostMapping(hostMapping2);
        deploymentEntityActive2.setDeploymentState(DeploymentStatus.SUCCESSFUL.name());

        deploymentEntityIntended1 = new DeploymentEntity();
        deploymentEntityIntended1.setId(3L);
        deploymentEntityIntended1.setStartTime(new Date());
        deploymentEntityIntended1.setDeploymentState(DeploymentStatus.PENDING.name());
        deploymentEntityIntended1.setCreatedBy("testuser");
        deploymentEntityIntended1.setIocVersion(iocVersion1);
        deploymentEntityIntended1.setActive(false);
        deploymentEntityIntended1.setIntended(true);
        deploymentEntityIntended1.setDescription("");
        deploymentEntityIntended1.setHostMapping(hostMapping2);

        deploymentEntityIntended2 = new DeploymentEntity();
        deploymentEntityIntended2.setId(4L);
        deploymentEntityIntended2.setStartTime(new Date());
        deploymentEntityIntended2.setDeploymentState(DeploymentStatus.PENDING.name());
        deploymentEntityIntended2.setCreatedBy("testuser");
        deploymentEntityIntended2.setIocVersion(iocVersion2);
        deploymentEntityIntended2.setActive(false);
        deploymentEntityIntended2.setIntended(true);
        deploymentEntityIntended2.setDescription("");
        deploymentEntityIntended2.setHostMapping(hostMapping1);

        job1 = new Job();
        job1.setId(100);
        job1.setStatus(JobStatus.PENDING);

        jobEntity1 = new AwxJobEntity();
        jobEntity1.setId(1L);
        jobEntity1.setAwxJobId(job1.getId());
        jobEntity1.setStatus(JobStatus.PENDING.name());
    }

    @Test
    public void createDeploymentsSuccess() {
        DeploymentService deploymentService = createDeploymentService();

        Mockito.when(hostRepository.findLatestMappingByIocId(ioc1.getId())).thenReturn(hostMapping1);
        List<DeploymentEntity> deploymentEntities = deploymentService.createDeployments(
                ImmutableList.of(iocVersion1, iocVersion2),
                "comment", "testuser", false, false,
                "limit", "iocs to deploy");
        assertEquals(2, deploymentEntities.size());
        ArgumentCaptor<DeploymentEntity> argument = ArgumentCaptor.forClass(DeploymentEntity.class);
        Mockito.verify(deploymentRepository, Mockito.times(2)).createDeployment(argument.capture());
    }

    @Test
    public void createDeploymentsFail() {
        DeploymentService deploymentService = createDeploymentService();
        Mockito.when(hostRepository.findLatestMappingByIocId(ioc1.getId())).thenThrow(new EmptyResultDataAccessException(1));
        assertThrows(EntityNotFoundException.class, () -> deploymentService.createDeployments(
                ImmutableList.of(iocVersion1, iocVersion2), "comment", "testuser", false,
                false, "limit", "iocs to deploy"));
    }

    @Test
    public void createDeploymentSuccess() throws Exception {
        DeploymentService deploymentService = createDeploymentService();
        Mockito.when(iocVersionService.findIocLatestVersionByIocId(ioc1.getId())).thenReturn(iocVersion1);
        Mockito.when(iocVersionService.findIocLatestVersionByIocId(ioc2.getId())).thenReturn(iocVersion2);
        JobTemplate template = new JobTemplate();
        template.setName("dummy-template");
        JobTemplate.Related related = new JobTemplate.Related();
        related.setLaunch("http://awx.job/1");
        template.setRelated(related);
        Mockito.when(awxService.getJobTemplateName()).thenReturn("dummy-template");
        Mockito.when(awxService.getJobTemplate("dummy-template")).thenReturn(template);
        Mockito.when(awxService.launchJob("http://awx.job/1", "demo-1.esss.lu.se, demo-2.esss.lu.se",
                StringUtils.join(
                        ImmutableList.of(iocVersion1.getNamingName(), iocVersion2.getNamingName()), ", ")))
                .thenReturn(job1);

        Mockito.when(deploymentRepository.findActiveDeploymentForIocVersions(ImmutableList.of(iocVersion1.getId())))
                .thenReturn(deploymentEntityActive1);
        Mockito.when(deploymentRepository.findActiveDeploymentForIocVersions(ImmutableList.of(iocVersion2.getId())))
                .thenReturn(deploymentEntityActive2);

        Mockito.when(hostRepository.findLatestMappingByIocId(ioc1.getId())).thenReturn(hostMapping1);
        Mockito.when(hostRepository.findLatestMappingByIocId(ioc2.getId())).thenReturn(hostMapping2);

        UserDetails user = new UserDetails();
        user.setUserName("user");
        List<Deployment> deployments = deploymentService.createDeployment(DeploymentTask
                .createDeploymentTask(ImmutableList.of(ioc1.getId(), ioc2.getId()),"comment", user));
        assertEquals(2, deployments.size());
        assertEquals(job1.getId(), deployments.get(0).getAwxJobId());
        assertNull(deployments.get(0).getEndDate());
        assertEquals("comment", deployments.get(0).getComment());
        assertEquals(host1.getHost(), deployments.get(0).getHost().getHost());
        assertEquals(iocVersion1.getId(), deployments.get(0).getVersion().getId());

        assertEquals(job1.getId(), deployments.get(1).getAwxJobId());
        assertNull(deployments.get(1).getEndDate());
        assertEquals("comment", deployments.get(1).getComment());
        assertEquals(host2.getHost(), deployments.get(1).getHost().getHost());
        assertEquals(iocVersion2.getId(), deployments.get(1).getVersion().getId());
    }

    @Test
    public void createAwxJobSuccess() {
        DeploymentService deploymentService = createDeploymentService();
        AwxJobEntity jobEntity = deploymentService.createAwxJob(job1, ImmutableList.of(deploymentEntityIntended1, deploymentEntityIntended2));
        assertEquals(job1.getId(), jobEntity.getAwxJobId());
        ArgumentCaptor<DeploymentJobMappingEntity> argument = ArgumentCaptor.forClass(DeploymentJobMappingEntity.class);
        Mockito.verify(deploymentRepository, Mockito.times(2)).createDeploymentJobMapping(argument.capture());
    }

    @Test
    public void updateAwxJobAndDeploymentToRunning() {
        DeploymentService deploymentService = createDeploymentService();
        Date start = new DateTime(2021, 3, 23, 10, 0,0).toDate();
        AwxJobEntity jobEntity = new AwxJobEntity();
        jobEntity.setId(1L);
        jobEntity.setAwxJobId(job1.getId());
        jobEntity.setStatus(JobStatus.PENDING.name());
        Mockito.when(deploymentRepository.findByAwxJobId(job1.getId())).thenReturn(jobEntity);
        DeploymentJobMappingEntity deploymentJobMapping = new DeploymentJobMappingEntity();
        deploymentJobMapping.setDeployment(deploymentEntityIntended1);
        deploymentJobMapping.setAwxJob(jobEntity);
        Mockito.when(deploymentRepository.findDeploymentMappingForAwxJobId(jobEntity.getId())).thenReturn(ImmutableList.of(deploymentJobMapping));
        deploymentService.updateAwxJobAndDeployment(job1.getId(), JobStatus.RUNNING, start, null, null);
        ArgumentCaptor<AwxJobEntity> argument = ArgumentCaptor.forClass(AwxJobEntity.class);
        Mockito.verify(deploymentRepository, Mockito.times(1)).updateAwxJob(argument.capture());
        assertEquals(JobStatus.RUNNING.name(), argument.getValue().getStatus());
        assertEquals(start, argument.getValue().getStartAt());
        assertNull(argument.getValue().getFinishedAt());

        ArgumentCaptor<DeploymentEntity> argument2 = ArgumentCaptor.forClass(DeploymentEntity.class);
        Mockito.verify(deploymentRepository, Mockito.times(1)).updateDeployment(argument2.capture());
        assertEquals(start, argument2.getValue().getStartTime());
        assertEquals(DeploymentStatus.RUNNING.name(), argument2.getValue().getDeploymentState());
    }

    @Test
    public void updateAwxJobAndDeploymentToFailed() {
        DeploymentService deploymentService = createDeploymentService();
        Date start = new DateTime(2021, 3, 23, 10, 0,0).toDate();
        Date finish = new DateTime(2021, 3, 23, 10, 1,0).toDate();
        Mockito.when(deploymentRepository.findByAwxJobId(job1.getId())).thenReturn(jobEntity1);
        DeploymentJobMappingEntity deploymentJobMapping = new DeploymentJobMappingEntity();
        deploymentJobMapping.setDeployment(deploymentEntityIntended1);
        deploymentJobMapping.setAwxJob(jobEntity1);
        Mockito.when(deploymentRepository.findDeploymentMappingForAwxJobId(jobEntity1.getId())).thenReturn(ImmutableList.of(deploymentJobMapping));
        Map<String, Map<String, Object>> iocDeploymentResult = ImmutableMap.of("IOC-1", ImmutableMap.of("successful", false));
        deploymentService.updateAwxJobAndDeployment(job1.getId(), JobStatus.FAILED, start, finish, iocDeploymentResult);
        ArgumentCaptor<AwxJobEntity> argument = ArgumentCaptor.forClass(AwxJobEntity.class);
        Mockito.verify(deploymentRepository, Mockito.times(1)).updateAwxJob(argument.capture());
        assertEquals(JobStatus.FAILED.name(), argument.getValue().getStatus());
        assertEquals(start, argument.getValue().getStartAt());
        assertEquals(finish, argument.getValue().getFinishedAt());

        ArgumentCaptor<DeploymentEntity> argument2 = ArgumentCaptor.forClass(DeploymentEntity.class);
        Mockito.verify(deploymentRepository, Mockito.times(1)).updateDeployment(argument2.capture());
        assertEquals(start, argument2.getValue().getStartTime());
        assertEquals(finish, argument2.getValue().getFinishedAt());
        assertEquals(DeploymentStatus.FAILED.name(), argument2.getValue().getDeploymentState());
    }

    @Test
    public void updateAwxJobAndDeploymentToSuccessful() {
        DeploymentService deploymentService = createDeploymentService();
        Date start = new DateTime(2021, 3, 23, 10, 0,0).toDate();
        Date finish = new DateTime(2021, 3, 23, 10, 1,0).toDate();
        AwxJobEntity jobEntity = new AwxJobEntity();
        jobEntity.setId(1L);
        jobEntity.setAwxJobId(job1.getId());
        jobEntity.setStatus(JobStatus.PENDING.name());
        Mockito.when(deploymentRepository.findByAwxJobId(job1.getId())).thenReturn(jobEntity);
        DeploymentJobMappingEntity deploymentJobMapping = new DeploymentJobMappingEntity();
        deploymentJobMapping.setDeployment(deploymentEntityIntended1);
        deploymentJobMapping.setAwxJob(jobEntity);
        Mockito.when(deploymentRepository.findDeploymentMappingForAwxJobId(jobEntity.getId())).thenReturn(ImmutableList.of(deploymentJobMapping));
        Mockito.when(iocVersionService.findVersionIdsForIoc(ioc1.getId())).thenReturn(ImmutableList.of(iocVersion1.getId()));
        Mockito.when(deploymentRepository.findActiveDeploymentForIocVersions(ImmutableList.of(iocVersion1.getId()))).thenReturn(deploymentEntityActive1);
        Map<String, Map<String, Object>> iocDeploymentResult = ImmutableMap.of("TEST-IOC-1", ImmutableMap.of("successful", true));

        deploymentService.updateAwxJobAndDeployment(job1.getId(), JobStatus.SUCCESSFUL, start, finish, iocDeploymentResult);

        ArgumentCaptor<AwxJobEntity> argument = ArgumentCaptor.forClass(AwxJobEntity.class);
        Mockito.verify(deploymentRepository, Mockito.times(1)).updateAwxJob(argument.capture());
        assertEquals(JobStatus.SUCCESSFUL.name(), argument.getValue().getStatus());
        assertEquals(start, argument.getValue().getStartAt());
        assertEquals(finish, argument.getValue().getFinishedAt());

        ArgumentCaptor<DeploymentEntity> deploymentCaptor = ArgumentCaptor.forClass(DeploymentEntity.class);
        Mockito.verify(deploymentRepository, Mockito.times(2)).updateDeployment(deploymentCaptor.capture());
        List<DeploymentEntity> capturedDeployments = deploymentCaptor.getAllValues();
        assertFalse(capturedDeployments.get(0).isActive());

        assertEquals(start, capturedDeployments.get(1).getStartTime());
        assertEquals(finish, capturedDeployments.get(1).getFinishedAt());
        assertEquals(DeploymentStatus.SUCCESSFUL.name(), capturedDeployments.get(1).getDeploymentState());
    }

    @Test
    public void findDeploymentForIocVersions() {
        DeploymentService deploymentService = createDeploymentService();
        Mockito.when(deploymentRepository.findActiveDeploymentForIocVersions(ImmutableList.of(iocVersion1.getId())))
                .thenReturn(deploymentEntityActive1);
        Mockito.when(deploymentRepository.findJobForDeployment(deploymentEntityActive1.getId()))
                .thenReturn(jobEntity1);
        Deployment deployment = deploymentService.findDeploymentForIocVersions(ImmutableList.of(iocVersion1.getId()));
        assertEquals(job1.getId(), deployment.getAwxJobId());
        assertEquals(host1.getHost(), deployment.getHost().getHost());
        assertEquals(DeploymentStatus.SUCCESSFUL, deployment.getStatus());
    }

    @Test
    public void findAllDeployments() {
        Mockito.when(utils.pageLimitConverter(0, 2)).thenReturn(new PagingLimitDto(0,2));
        Mockito.when(deploymentRepository.findAllDeployments(DeploymentStatus.SUCCESSFUL.name(), null, null,"testuser",
                "demo", DeploymentOrder.FINISHED_AT, true, 0, 2))
                .thenReturn(ImmutableList.of(deploymentEntityActive1, deploymentEntityActive2));
        Mockito.when(deploymentRepository.countForPaging(DeploymentStatus.SUCCESSFUL.name(), null, null, "testuser","demo")).thenReturn(2L);
        DeploymentService deploymentService = createDeploymentService();
        PagedDeploymentResponse response = deploymentService.findAll(DeploymentStatus.SUCCESSFUL, null, null,"testuser",
                "demo", DeploymentOrder.FINISHED_AT, true, 0, 2);
        assertNotNull(response);
        assertEquals(2, response.getTotalCount());
        assertEquals(0, response.getPageNumber());
        assertEquals(2, response.getLimit());
        assertEquals(2, response.getListSize());
        assertEquals(2, response.getDeployments().size());
        assertEquals(1, response.getDeployments().get(0).getId());
        assertEquals(2, response.getDeployments().get(1).getId());
    }

    private DeploymentService createDeploymentService() {
        return new DeploymentService(deploymentRepository, hostRepository,
                iocVersionService, utils, hostService, awxService, authorizationService);
    }
}