/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.ess.ics.ccce.exceptions.AuthenticationException;
import eu.ess.ics.ccce.model.LoginTokenDto;
import eu.ess.ics.ccce.model.RBACToken;
import eu.ess.ics.ccce.repo.RBACRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@SpringBootTest
public class RBACServiceUT {

    @Mock
    private RBACRepository rbacRepository;

    @Test
    void loginFail(){
        RBACService rbacService = createService();
        Mockito.when(rbacRepository.loginUser("test", "test")).thenThrow(AuthenticationException.class);
        assertThrows(AuthenticationException.class, () -> {
            rbacService.loginUser("test", "test");
        });
    }

    @Test
    void loginSuccess() throws JsonProcessingException {
        RBACService rbacService = createService();
        RBACToken rbacLoginResponse = createLogin();
        Mockito.when(rbacRepository.loginUser("test", "test"))
                .thenReturn(rbacLoginResponse);

        LoginTokenDto tokenDto = rbacService.loginUser("test", "test");

        assertNotNull(tokenDto);
        assertNotNull(tokenDto.getToken());
        assertEquals(rbacLoginResponse.getId(), tokenDto.getToken());
    }

    private RBACToken createLogin() throws JsonProcessingException {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<token>\n" +
                "    <id>AAAA-BBBBB-CCCC-DDDD-EEFFGGHH</id>\n" +
                "    <username>johndoe</username>\n" +
                "    <firstName>John</firstName>\n" +
                "    <lastName>Doe</lastName>\n" +
                "    <creationTime>1620823391053</creationTime>\n" +
                "    <expirationTime>1620852191053</expirationTime>\n" +
                "    <ip>10.0.42.5</ip>\n" +
                "    <roles>\n" +
                "        <role>CCDBAdministrator</role>\n" +
                "        <role>CableAdministrator</role>\n" +
                "        <role>IOCFactoryAdministrator</role>\n" +
                "        <role>NamingAdministrator</role>\n" +
                "    </roles>\n" +
                "    <signature>764AAF5B29498D3424226C6FF54E5BD7FC0751C35E8986CEBFA25E8867FBEBC2F9482C6189CDEFC8312BEB244D3E122B2E1876344BD8CC7C9B08420A3A536169</signature>\n" +
                "</token>";

        XmlMapper xmlMapper = new XmlMapper();

        return xmlMapper.readValue(xml, RBACToken.class);
    }

    private RBACService createService(){
        return new RBACService(rbacRepository);
    }
}
