package eu.ess.ics.ccce.service;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.repo.IIocRepository;
import eu.ess.ics.ccce.repo.IIocVersionRepository;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.repo.model.IocMetaDataEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import eu.ess.ics.ccce.rest.model.BaseIocVersion;
import eu.ess.ics.ccce.rest.model.IocVersion;
import eu.ess.ics.ccce.rest.model.response.PagedIocVersionResponse;
import eu.ess.ics.ccce.util.PagingLimitDto;
import eu.ess.ics.ccce.util.Utils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@ExtendWith(MockitoExtension.class)
public class IocVersionServiceUT {
    @Mock
    private Utils utils;
    @Mock
    private IIocRepository iocRepository;
    @Mock
    private IIocVersionRepository iocVersionRepository;
    @Mock
    private MetadataService metadataService;

    private final IocEntity ioc1;
    private final IocVersionEntity iocVersion1;
    private final IocVersionEntity iocVersion2;


    IocVersionServiceUT() {
        ioc1 = new IocEntity();
        ioc1.setCustomName("IOC-1");
        ioc1.setId(1L);

        iocVersion1 = new IocVersionEntity();
        iocVersion1.setId(1L);
        iocVersion1.setVersion(1L);
        iocVersion1.setIoc(ioc1);

        iocVersion2 = new IocVersionEntity();
        iocVersion2.setId(2L);
        iocVersion2.setVersion(2L);
        iocVersion2.setIoc(ioc1);
    }

    @Test
    public void createIocVersion() {
        Mockito.when(iocRepository.findById(1L)).thenReturn(ioc1);
        IocVersionService versionService = createIocVersionService();
        BaseIocVersion version = new BaseIocVersion("source-url", "1.0.0");
        IocMetaDataEntity metaData = new IocMetaDataEntity();
        metaData.setInfo("");
        metaData.setFileHash("");
        metaData.setVersion("1.0");
        versionService.createIocVersion(ioc1.getId(), version, metaData, "testName",
                "abd1cecd-1ed5-4c0e-85a4-ad171867bc90 ", "ccce");
        ArgumentCaptor<IocVersionEntity> captor = ArgumentCaptor.forClass(IocVersionEntity.class);
        Mockito.verify(iocVersionRepository, Mockito.times(1)).createIocVersion(captor.capture());
        assertEquals(ioc1.getId(), captor.getValue().getIoc().getId());
        assertEquals(1, captor.getValue().getVersion());
        assertEquals(version.getSourceUrl(), captor.getValue().getSourceUrl());
        assertEquals(version.getSourceVersion(), captor.getValue().getSourceVersion());
    }

    @Test
    public void findAllVersions() {
        Mockito.when(utils.pageLimitConverter(0, 2)).thenReturn(new PagingLimitDto(0,2));
        Mockito.when(iocRepository.findById(1L)).thenReturn(ioc1);
        Mockito.when(iocVersionRepository.findAllByIocId(ioc1.getId(), 0, 2)).thenReturn(ImmutableList.of(iocVersion1, iocVersion2));
        Mockito.when(iocVersionRepository.countForPaging(ioc1.getId())).thenReturn(10L);
        IocVersionService versionService = createIocVersionService();
        PagedIocVersionResponse response = versionService.findAllVersions(ioc1.getId(), 0, 2);
        assertNotNull(response);
        assertEquals(10, response.getTotalCount());
        assertEquals(0, response.getPageNumber());
        assertEquals(2, response.getLimit());
        assertEquals(2, response.getListSize());
        assertEquals(2, response.getIocVersionList().size());
        assertEquals(1, response.getIocVersionList().get(0).getId());
        assertEquals(2, response.getIocVersionList().get(1).getId());
    }

    @Test
    public void findVersionByIds() {
        Mockito.when(iocVersionRepository.findByVersionId(ioc1.getId(), iocVersion1.getVersion())).thenReturn(iocVersion1);
        IocVersionService versionService = createIocVersionService();
        IocVersion version = versionService.findVersionByIds(ioc1.getId(), iocVersion1.getVersion());
        assertEquals(iocVersion1.getId(), version.getId());
        assertEquals(iocVersion1.getVersion(), version.getVersion());
    }

    private IocVersionService createIocVersionService() {
        return new IocVersionService(iocRepository, iocVersionRepository, utils, metadataService);
    }
}
