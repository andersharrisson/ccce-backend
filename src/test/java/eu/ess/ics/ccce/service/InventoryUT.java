package eu.ess.ics.ccce.service;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.model.DeploymentDto;
import eu.ess.ics.ccce.rest.model.InventoryHost;
import eu.ess.ics.ccce.service.deployment.InventoryCalculation;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class InventoryUT {
    private static final String IOC_1 = "ioc-1";
    private static final String IOC_2 = "ioc-2";
    private static final String IOC_1_VERSION_SRC_URL = "http://test1.git";
    private static final String IOC_1_VERSION_SRC_VERSION = "1.0.0";
    private static final String IOC_2_VERSION_SRC_URL = "http://test2.git";
    private static final String IOC_2_VERSION_SRC_VERSION = "2.0.0";
    private static final String HOST_1 = "ccce-test-1.esss.lu.se";
    private static final String HOST_2 = "ccce-test-2.esss.lu.se";

    @Test
    void emptyInventory() {
        Map<String, InventoryHost> inventory = getInventory(Collections.emptyList());
        assertEquals(0, inventory.size());
    }

    @Test
    void inventoryForOneIntended() {
        Map<String, InventoryHost> inventory = getInventory(ImmutableList.of(
                new DeploymentDto(IOC_1, IOC_1_VERSION_SRC_URL, IOC_1_VERSION_SRC_VERSION,"7.0.5", "3.4.0", HOST_1,  false, true)
        ));
        assertEquals(1, inventory.size());
        assertTrue(inventory.containsKey(HOST_1));
        assertEquals(1, inventory.get(HOST_1).getIocs().size());
        assertEquals(IOC_1, inventory.get(HOST_1).getIocs().get(0).getName());
        assertEquals(IOC_1_VERSION_SRC_URL, inventory.get(HOST_1).getIocs().get(0).getGit());
        assertEquals(IOC_1_VERSION_SRC_VERSION, inventory.get(HOST_1).getIocs().get(0).getVersion());
    }

    @Test
    void inventoryForOneActive() {
        Map<String, InventoryHost> inventory = getInventory(ImmutableList.of(
                new DeploymentDto(IOC_1, IOC_1_VERSION_SRC_URL, IOC_1_VERSION_SRC_VERSION,"7.0.5", "3.4.0", HOST_1, true, false)
        ));
        assertEquals(1, inventory.size());
        assertTrue(inventory.containsKey(HOST_1));
        assertEquals(1, inventory.get(HOST_1).getIocs().size());
        assertEquals(IOC_1, inventory.get(HOST_1).getIocs().get(0).getName());
        assertEquals(IOC_1_VERSION_SRC_URL, inventory.get(HOST_1).getIocs().get(0).getGit());
        assertEquals(IOC_1_VERSION_SRC_VERSION, inventory.get(HOST_1).getIocs().get(0).getVersion());
    }

    @Test
    void inventoryForActiveAndIntendedForOneIOC() {
        Map<String, InventoryHost> inventory = getInventory(ImmutableList.of(
                new DeploymentDto(IOC_1, IOC_1_VERSION_SRC_URL, IOC_1_VERSION_SRC_VERSION,"7.0.5", "3.4.0", HOST_1, true, false),
                new DeploymentDto(IOC_1, IOC_1_VERSION_SRC_URL, IOC_1_VERSION_SRC_VERSION,"7.0.5", "3.4.0", HOST_2,  false, true)
        ));
        assertEquals(2, inventory.size());
        assertTrue(inventory.containsKey(HOST_1));
        assertTrue(inventory.containsKey(HOST_2));
        assertEquals(0, inventory.get(HOST_1).getIocs().size());
        assertEquals(1, inventory.get(HOST_2).getIocs().size());
        assertEquals(IOC_1, inventory.get(HOST_2).getIocs().get(0).getName());
        assertEquals(IOC_1_VERSION_SRC_URL, inventory.get(HOST_2).getIocs().get(0).getGit());
        assertEquals(IOC_1_VERSION_SRC_VERSION, inventory.get(HOST_2).getIocs().get(0).getVersion());
    }

    @Test
    void inventoryForActiveAndIntendedForOneHost() {
        Map<String, InventoryHost> inventory = getInventory(ImmutableList.of(
                new DeploymentDto(IOC_1, IOC_1_VERSION_SRC_URL, IOC_1_VERSION_SRC_VERSION,"7.0.5", "3.4.0", HOST_1, true, false),
                new DeploymentDto(IOC_2, IOC_2_VERSION_SRC_URL, IOC_2_VERSION_SRC_VERSION,"7.0.5", "3.4.0", HOST_1, false, true)
        ));
        assertEquals(1, inventory.size());
        assertTrue(inventory.containsKey(HOST_1));
        assertFalse(inventory.containsKey(HOST_2));
        assertEquals(2, inventory.get(HOST_1).getIocs().size());
        assertEquals(IOC_1, inventory.get(HOST_1).getIocs().get(0).getName());
        assertEquals(IOC_1_VERSION_SRC_URL, inventory.get(HOST_1).getIocs().get(0).getGit());
        assertEquals(IOC_1_VERSION_SRC_VERSION, inventory.get(HOST_1).getIocs().get(0).getVersion());
        assertEquals(IOC_2, inventory.get(HOST_1).getIocs().get(1).getName());
        assertEquals(IOC_2_VERSION_SRC_URL, inventory.get(HOST_1).getIocs().get(1).getGit());
        assertEquals(IOC_2_VERSION_SRC_VERSION, inventory.get(HOST_1).getIocs().get(1).getVersion());
    }

    private Map<String, InventoryHost> getInventory(List<DeploymentDto> deployments) {
        InventoryCalculation inventoryCalculation = new InventoryCalculation(deployments);
        inventoryCalculation.createInventory();
        return inventoryCalculation.getInventory();
    }
}
