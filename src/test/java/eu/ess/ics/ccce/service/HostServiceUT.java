package eu.ess.ics.ccce.service;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.csentry.model.CSEntryHost;
import eu.ess.ics.ccce.csentry.model.CSEntryHostById;
import eu.ess.ics.ccce.csentry.model.CSEntryInterface;
import eu.ess.ics.ccce.csentry.service.CSEntryService;
import eu.ess.ics.ccce.exceptions.CSEntryException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.repo.IDeploymentRepository;
import eu.ess.ics.ccce.repo.IHostRepository;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.HostMappingEntity;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.rest.model.CSEntryHostWithStatus;
import eu.ess.ics.ccce.rest.model.Host;
import eu.ess.ics.ccce.rest.model.HostStatus;
import eu.ess.ics.ccce.rest.model.request.HostOrder;
import eu.ess.ics.ccce.rest.model.response.PagedCSEntryHostResponse;
import eu.ess.ics.ccce.rest.model.response.PagedHostResponse;
import eu.ess.ics.ccce.util.PagingLimitDto;
import eu.ess.ics.ccce.util.Utils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Collections;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@ExtendWith(MockitoExtension.class)
public class HostServiceUT {
    @Mock
    private Utils utils;
    @Mock
    private IHostRepository hostRepository;
    @Mock
    private StatusService statusService;
    @Mock
    private CSEntryService csEntryService;
    @Mock
    private IDeploymentRepository deploymentRepository;

    private final HostEntity host1;
    private final HostEntity host2;

    private final CSEntryHostById csEntryHost1;
    private final CSEntryHostById csEntryHost2;

    HostServiceUT() {
        host1 = new HostEntity();
        host1.setId(1L);
        host1.setHost("demo-1.esss.lu.se");
        host1.setNetwork("CSLab");
        host1.setCsEntryId(10L);

        host2 = new HostEntity();
        host2.setId(2L);
        host2.setHost("demo-2.esss.lu.se");
        host2.setNetwork("CSLab");
        host2.setCsEntryId(20L);

        csEntryHost1 = new CSEntryHostById(10, "test1.esss.lu.se", "Test 1", "Scope 1", true,
                "DevType1", "Test host 1", "user", new Date(), null, null, null);
        csEntryHost2 = new CSEntryHostById(20, "test2.esss.lu.se", "Test 2", "Scope 2", true,
                "DevType2", "Test host 2", "user", new Date(), null, null, null);
    }

    @Test
    public void getHostDetailsAvailable() {
        Mockito.when(hostRepository.findHostById(1)).thenReturn(host1);
        Mockito.when(statusService.checkHostIsActive("demo-1.esss.lu.se")).thenReturn(true);
        HostService hostService = createHostService();
        Host host = hostService.hostDetails(1);
        assertEquals("demo-1.esss.lu.se", host.getHost());
        assertEquals("CSLab", host.getNetwork());
        assertEquals(10, host.getCsEntryId());
        assertEquals(HostStatus.AVAILABLE, host.getStatus());
    }

    @Test
    public void getHostDetailsNotAvailable() {
        HostEntity hostEntity = new HostEntity();
        hostEntity.setId(1L);
        hostEntity.setHost("demo-1.esss.lu.se");
        hostEntity.setNetwork("CSLab");
        hostEntity.setCsEntryId(10L);
        Mockito.when(hostRepository.findHostById(1)).thenReturn(hostEntity);
        Mockito.when(statusService.checkHostIsActive("demo-1.esss.lu.se")).thenReturn(false);
        HostService hostService = createHostService();
        Host host = hostService.hostDetails(1);
        assertEquals("demo-1.esss.lu.se", host.getHost());
        assertEquals("CSLab", host.getNetwork());
        assertEquals(10, host.getCsEntryId());
        assertEquals(HostStatus.UNREACHABLE, host.getStatus());
    }

    @Test
    public void findAllHosts() {
        Mockito.when(utils.pageLimitConverter(0, 2)).thenReturn(new PagingLimitDto(0,2));
        Mockito.when(hostRepository.findAll("demo", HostOrder.HOST, true, 0, 2))
                .thenReturn(ImmutableList.of(host1, host2));
        Mockito.when(hostRepository.countForPaging("demo")).thenReturn(2L);
        HostService hostService = createHostService();
        PagedHostResponse response = hostService.findAll("demo", HostOrder.HOST, true, 0, 2);
        assertNotNull(response);
        assertEquals(2, response.getTotalCount());
        assertEquals(0, response.getPageNumber());
        assertEquals(2, response.getLimit());
        assertEquals(2, response.getListSize());
        assertEquals(2, response.getHostList().size());
        assertEquals(10, response.getHostList().get(0).getCsEntryId());
        assertEquals(20, response.getHostList().get(1).getCsEntryId());
    }

    @Test
    public void findAllHostsFromCSEntrySuccess() throws CSEntryException {
        Mockito.when(utils.pageLimitConverter(0, 2, CSEntryService.CSENTRY_PAGE_LIMIT)).thenReturn(new PagingLimitDto(0,2));
        Mockito.when(csEntryService.searchHost("demo", 0, 2)).
                thenReturn(ImmutableList.of(new CSEntryHost(csEntryHost1, Collections.emptyList()), new CSEntryHost(csEntryHost2, Collections.emptyList())));
        HostService hostService = createHostService();
        PagedCSEntryHostResponse response = hostService.findAllFromCSEntry("demo", 0,2);
        assertNotNull(response);
        assertEquals(0, response.getTotalCount());
        assertEquals(0, response.getPageNumber());
        assertEquals(2, response.getLimit());
        assertEquals(2, response.getListSize());
        assertEquals(2, response.getCsEntryHosts().size());
        assertEquals(10, response.getCsEntryHosts().get(0).getCsEntryHost().getId());
        assertEquals(20, response.getCsEntryHosts().get(1).getCsEntryHost().getId());
    }

    @Test
    public void findAllHostsFromCSEntryFail() throws CSEntryException {
        Mockito.when(utils.pageLimitConverter(0, 2, CSEntryService.CSENTRY_PAGE_LIMIT)).thenReturn(new PagingLimitDto(0, 2));
        Mockito.when(csEntryService.searchHost("demo", 0, 2)).thenThrow(new CSEntryException("Error"));
        HostService hostService = createHostService();
        assertThrows(CSEntryException.class, () -> hostService.findAllFromCSEntry("demo", 0,2));
    }

    @Test
    public void findHostByCSEntryIdSuccess() throws CSEntryException {
        Mockito.when(csEntryService.findHostById(10)).thenReturn(new CSEntryHost(csEntryHost1, Collections.emptyList()));
        HostService hostService = createHostService();
        CSEntryHostWithStatus host = hostService.findHostByCSEntryId(10);
        assertNotNull(host);
        assertEquals(10, host.getCsEntryHost().getId());
    }

    @Test
    public void findHostByCSEntryIdFail() throws CSEntryException {
        Mockito.when(csEntryService.findHostById(10)).thenThrow(new EntityNotFoundException("CSEntry host", 10));
        HostService hostService = createHostService();
        assertThrows(EntityNotFoundException.class, () -> hostService.findHostByCSEntryId(10));
    }

    @Test
    public void linkHostToIocSuccess() {
        IocEntity ioc = new IocEntity();
        ioc.setId(1L);

        Mockito.when(statusService.checkHostIsActive("demo-1.esss.lu.se")).thenReturn(true);
        Mockito.when(hostRepository.findHostById(1)).thenReturn(host1);
        HostService hostService = createHostService();
        Host host = hostService.linkHostToIoc(1L, ioc, "testuser");
        ArgumentCaptor<HostMappingEntity> argument = ArgumentCaptor.forClass(HostMappingEntity.class);
        Mockito.verify(hostRepository, Mockito.times(1)).createHostMapping(argument.capture());
        assertEquals(host1.getId(), argument.getValue().getHost().getId());
        assertEquals(host1.getHost(), argument.getValue().getHost().getHost());
        assertEquals("testuser", argument.getValue().getCreatedBy());
        assertEquals(1, argument.getValue().getIoc().getId());

        assertEquals(host1.getHost(), host.getHost());
        assertEquals(host1.getNetwork(), host.getNetwork());
        assertEquals(host1.getCsEntryId(), host.getCsEntryId());
        assertEquals(HostStatus.AVAILABLE, host.getStatus());
    }

    @Test
    public void findLatestByIocIdSuccess() {
        IocEntity ioc = new IocEntity();
        ioc.setId(1L);

        HostMappingEntity hostMapping = new HostMappingEntity();
        hostMapping.setId(1L);
        hostMapping.setHost(host1);
        hostMapping.setIoc(ioc);
        hostMapping.setCreatedBy("testuser");
        hostMapping.setCreatedAt(new Date());

        Mockito.when(statusService.checkHostIsActive("demo-1.esss.lu.se")).thenReturn(true);
        Mockito.when(hostRepository.findLatestMappingByIocId(1)).thenReturn(hostMapping);
        HostService hostService = createHostService();
        Host host = hostService.findLatestByIocId(1);
        assertEquals(host1.getHost(), host.getHost());
        assertEquals(host1.getNetwork(), host.getNetwork());
        assertEquals(host1.getCsEntryId(), host.getCsEntryId());
        assertEquals(HostStatus.AVAILABLE, host.getStatus());
    }

    @Test
    public void findLatestByIocIdNotFound() {
        Mockito.when(hostRepository.findLatestMappingByIocId(1)).thenThrow(new EmptyResultDataAccessException(1));
        HostService hostService = createHostService();
        Host host = hostService.findLatestByIocId(1);
        assertNull(host);
    }

    @Test
    public void getHostStatusEmptyHostname() {
        HostService hostService = createHostService();
        assertEquals(HostStatus.UNREACHABLE, hostService.getHostStatus(""));
    }

    @Test
    public void hostIdForCSEntryHostIfAlreadyExists() {
        Mockito.when(hostRepository.findHostByCSEntryId(10)).thenReturn(host1);
        HostService hostService = createHostService();
        assertEquals(host1.getCsEntryId(), hostService.hostIdForCSEntryHostIfAlreadyExists(10).getCsEntryId());
    }

    @Test
    public void hostIdForCSEntryHostIfNotExists() {
        CSEntryInterface if1 = new CSEntryInterface();
        if1.setMain(true);
        if1.setNetwork("Lab1");
        CSEntryHost host = new CSEntryHost(csEntryHost1, ImmutableList.of(if1));
        host.setFqdn("demo-1.esss.lu.se");
        HostService hostService = createHostService();
        hostService.hostIdForCSEntryHostIfNotExists(host);
        ArgumentCaptor<HostEntity> argument = ArgumentCaptor.forClass(HostEntity.class);
        Mockito.verify(hostRepository).createHost(argument.capture());
        assertEquals(host.getId(), argument.getValue().getCsEntryId());
        assertEquals(if1.getNetwork(), argument.getValue().getNetwork());
        assertEquals(host.getFqdn(), argument.getValue().getHost());
    }

    private HostService createHostService() {
        return new HostService(utils, hostRepository, statusService, csEntryService, deploymentRepository);
    }
}
