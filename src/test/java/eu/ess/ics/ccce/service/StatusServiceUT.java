/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import com.google.gson.Gson;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.prometheus.PrometheusRepository;
import eu.ess.ics.ccce.prometheus.PrometheusResponse;
import okhttp3.OkHttpClient;
import okhttp3.Headers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import eu.ess.ics.ccce.service.HttpClientService.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@SpringBootTest
public class StatusServiceUT {
    @Mock
    private HttpClientService httpClientService;
    @Mock
    private Environment env;

    @Test
    void testHostIsActive() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = hostUp();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        Boolean hostStatus = service.checkHostIsActive("ccce-plcf-vm-01.cslab.esss.lu.se");
        assertTrue(hostStatus);
    }

    @Test
    void testHostStateUnknown() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = hostStateUnknown();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        Boolean hostStatus = service.checkHostIsActive("testHost");
        assertNull(hostStatus);
    }

    @Test
    void testHostDown() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = hostDown();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        Boolean hostStatus = service.checkHostIsActive("ccce-plcf-vm-01.cslab.esss.lu.se");
        assertFalse(hostStatus);
    }

    @Test
    void testIocUpOnHost() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = iocUpOnHost();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        Boolean hostStatus = service.checkIocIsActive("CCCE-PLCF_Vac-IOC-001", "ccce-demo-03.cslab.esss.lu.se");
        assertTrue(hostStatus);
    }

    @Test
    void testIocDownOnHost() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = iocDownOnHost();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        Boolean hostStatus = service.checkIocIsActive("CCCE-PLCF_Vac-IOC-001", "ccce-demo-03.cslab.esss.lu.se");
        assertFalse(hostStatus);
    }

    @Test
    void testIocUp() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = iocUpOnHost();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        Boolean hostStatus = service.checkIocIsActive("CCCE-PLCF_Vac-IOC-001", "");
        assertTrue(hostStatus);
    }

    @Test
    void testIocDown() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = iocDownOnHost();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        Boolean hostStatus = service.checkIocIsActive("CCCE-PLCF_Vac-IOC-001", null);
        assertFalse(hostStatus);
    }

    @Test
    void testHostList() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = hostList();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        List<String> hostList = service.listIocHosts();
        assertFalse(hostList.isEmpty());
        assertEquals(4, hostList.size());
    }

    @Test
    void testEmptyHostList() throws RemoteServiceException {

        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("http://test.host/");

        ServiceResponse<PrometheusResponse> httpResponse = emptyHostList();

        Mockito.when(httpClientService.executeGetRequest(ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(PrometheusResponse.class))).thenReturn(httpResponse);

        StatusService service = createService();
        List<String> hostList = service.listIocHosts();
        assertTrue(hostList.isEmpty());
    }

    private ServiceResponse<PrometheusResponse> hostUp() {
        Gson g = new Gson();
        String jsonString = "{\n" +
                "  \"status\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"resultType\": \"vector\",\n" +
                "    \"result\": [\n" +
                "      {\n" +
                "        \"metric\": {\n" +
                "          \"__name__\": \"up\",\n" +
                "          \"csentry_device_type\": \"VirtualMachine\",\n" +
                "          \"csentry_domain\": \"cslab.esss.lu.se\",\n" +
                "          \"csentry_group_cslab_generallab\": \"1\",\n" +
                "          \"csentry_group_e3_exporter\": \"1\",\n" +
                "          \"csentry_group_iocs\": \"1\",\n" +
                "          \"csentry_group_labnetworks\": \"1\",\n" +
                "          \"csentry_group_servers\": \"1\",\n" +
                "          \"csentry_group_virtualmachine\": \"1\",\n" +
                "          \"csentry_is_ioc\": \"1\",\n" +
                "          \"csentry_vlan\": \"CSLab_GeneralLab\",\n" +
                "          \"csentry_vlan_id\": \"3805\",\n" +
                "          \"instance\": \"ccce-plcf-vm-01.cslab.esss.lu.se\",\n" +
                "          \"job\": \"node\"\n" +
                "        },\n" +
                "        \"value\": [\n" +
                "          1614615642.956,\n" +
                "          \"1\"\n" +
                "        ]\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";
        PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

        return new ServiceResponse<>(p, 200);
    }

    private ServiceResponse<PrometheusResponse> hostDown() {
        Gson g = new Gson();
        String jsonString = "{\n" +
                "  \"status\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"resultType\": \"vector\",\n" +
                "    \"result\": [\n" +
                "      {\n" +
                "        \"metric\": {\n" +
                "          \"__name__\": \"up\",\n" +
                "          \"csentry_device_type\": \"VirtualMachine\",\n" +
                "          \"csentry_domain\": \"cslab.esss.lu.se\",\n" +
                "          \"csentry_group_cslab_generallab\": \"1\",\n" +
                "          \"csentry_group_e3_exporter\": \"1\",\n" +
                "          \"csentry_group_iocs\": \"1\",\n" +
                "          \"csentry_group_labnetworks\": \"1\",\n" +
                "          \"csentry_group_servers\": \"1\",\n" +
                "          \"csentry_group_virtualmachine\": \"1\",\n" +
                "          \"csentry_is_ioc\": \"1\",\n" +
                "          \"csentry_vlan\": \"CSLab_GeneralLab\",\n" +
                "          \"csentry_vlan_id\": \"3805\",\n" +
                "          \"instance\": \"ccce-plcf-vm-01.cslab.esss.lu.se\",\n" +
                "          \"job\": \"node\"\n" +
                "        },\n" +
                "        \"value\": [\n" +
                "          1614615642.956,\n" +
                "          \"0\"\n" +
                "        ]\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";
        PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

        return new ServiceResponse<>(p ,200);
    }

    private ServiceResponse<PrometheusResponse> hostStateUnknown() {
        Gson g = new Gson();
        String jsonString = "{\n" +
                "  \"status\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"resultType\": \"vector\",\n" +
                "    \"result\": []\n" +
                "  }\n" +
                "}";
        PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

        return new ServiceResponse<>(p ,200);
    }

    private ServiceResponse<PrometheusResponse> iocUpOnHost() {
        Gson g = new Gson();
        String jsonString = "{\n" +
                "  \"status\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"resultType\": \"vector\",\n" +
                "    \"result\": [\n" +
                "      {\n" +
                "        \"metric\": {\n" +
                "          \"__name__\": \"node_systemd_unit_state\",\n" +
                "          \"csentry_device_type\": \"VirtualMachine\",\n" +
                "          \"csentry_domain\": \"cslab.esss.lu.se\",\n" +
                "          \"csentry_group_e3_exporter\": \"1\",\n" +
                "          \"csentry_group_iocs\": \"1\",\n" +
                "          \"csentry_group_labnetworks\": \"1\",\n" +
                "          \"csentry_group_servers\": \"1\",\n" +
                "          \"csentry_group_utgard\": \"1\",\n" +
                "          \"csentry_group_utgard_dev\": \"1\",\n" +
                "          \"csentry_group_virtualmachine\": \"1\",\n" +
                "          \"csentry_is_ioc\": \"1\",\n" +
                "          \"csentry_vlan\": \"Utgard_Dev\",\n" +
                "          \"csentry_vlan_id\": \"3974\",\n" +
                "          \"instance\": \"ccce-demo-03.cslab.esss.lu.se\",\n" +
                "          \"job\": \"node\",\n" +
                "          \"name\": \"ioc@CCCE-PLCF_Vac-IOC-001.service\",\n" +
                "          \"state\": \"active\",\n" +
                "          \"type\": \"forking\"\n" +
                "        },\n" +
                "        \"value\": [\n" +
                "          1615970157.5,\n" +
                "          \"1\"\n" +
                "        ]\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";
        PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

        return new ServiceResponse<>(p, 200);
    }

    private ServiceResponse<PrometheusResponse> iocDownOnHost() {
        Gson g = new Gson();
        String jsonString = "{\n" +
                "  \"status\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"resultType\": \"vector\",\n" +
                "    \"result\": [\n" +
                "      {\n" +
                "        \"metric\": {\n" +
                "          \"__name__\": \"node_systemd_unit_state\",\n" +
                "          \"csentry_device_type\": \"VirtualMachine\",\n" +
                "          \"csentry_domain\": \"cslab.esss.lu.se\",\n" +
                "          \"csentry_group_e3_exporter\": \"1\",\n" +
                "          \"csentry_group_iocs\": \"1\",\n" +
                "          \"csentry_group_labnetworks\": \"1\",\n" +
                "          \"csentry_group_servers\": \"1\",\n" +
                "          \"csentry_group_utgard\": \"1\",\n" +
                "          \"csentry_group_utgard_dev\": \"1\",\n" +
                "          \"csentry_group_virtualmachine\": \"1\",\n" +
                "          \"csentry_is_ioc\": \"1\",\n" +
                "          \"csentry_vlan\": \"Utgard_Dev\",\n" +
                "          \"csentry_vlan_id\": \"3974\",\n" +
                "          \"instance\": \"ccce-demo-03.cslab.esss.lu.se\",\n" +
                "          \"job\": \"node\",\n" +
                "          \"name\": \"ioc@CCCE-PLCF_Vac-IOC-001.service\",\n" +
                "          \"state\": \"active\",\n" +
                "          \"type\": \"forking\"\n" +
                "        },\n" +
                "        \"value\": [\n" +
                "          1615970157.5,\n" +
                "          \"0\"\n" +
                "        ]\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";
        PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

        return new ServiceResponse<>(p, 200);
    }

    private ServiceResponse<PrometheusResponse> hostList() {
        Gson g = new Gson();
        String jsonString = "{\n" +
                "  \"status\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"resultType\": \"vector\",\n" +
                "    \"result\": [\n" +
                "      {\n" +
                "        \"metric\": {\n" +
                "          \"__name__\": \"up\",\n" +
                "          \"csentry_device_type\": \"MTCA-AMC\",\n" +
                "          \"csentry_domain\": \"cslab.esss.lu.se\",\n" +
                "          \"csentry_group_beam_diagnostics_machines\": \"1\",\n" +
                "          \"csentry_group_iocs\": \"1\",\n" +
                "          \"csentry_group_labnetworks\": \"1\",\n" +
                "          \"csentry_group_mtca_amc\": \"1\",\n" +
                "          \"csentry_is_ioc\": \"1\",\n" +
                "          \"csentry_vlan\": \"CSLab_BeamDiag\",\n" +
                "          \"csentry_vlan_id\": \"3880\",\n" +
                "          \"instance\": \"bd-cpu19.cslab.esss.lu.se\",\n" +
                "          \"job\": \"node\"\n" +
                "        },\n" +
                "        \"value\": [\n" +
                "          1620819560.746,\n" +
                "          \"0\"\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"metric\": {\n" +
                "          \"__name__\": \"up\",\n" +
                "          \"csentry_device_type\": \"MTCA-AMC\",\n" +
                "          \"csentry_domain\": \"cslab.esss.lu.se\",\n" +
                "          \"csentry_group_cslab_generallab\": \"1\",\n" +
                "          \"csentry_group_deploy_cct\": \"1\",\n" +
                "          \"csentry_group_iocs\": \"1\",\n" +
                "          \"csentry_group_labnetworks\": \"1\",\n" +
                "          \"csentry_group_mtca_amc\": \"1\",\n" +
                "          \"csentry_is_ioc\": \"1\",\n" +
                "          \"csentry_vlan\": \"CSLab_GeneralLab\",\n" +
                "          \"csentry_vlan_id\": \"3805\",\n" +
                "          \"instance\": \"ccpu-35564-007.cslab.esss.lu.se\",\n" +
                "          \"job\": \"node\"\n" +
                "        },\n" +
                "        \"value\": [\n" +
                "          1620819560.746,\n" +
                "          \"0\"\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"metric\": {\n" +
                "          \"__name__\": \"up\",\n" +
                "          \"csentry_device_type\": \"MTCA-AMC\",\n" +
                "          \"csentry_domain\": \"cslab.esss.lu.se\",\n" +
                "          \"csentry_group_cslab_generallab\": \"1\",\n" +
                "          \"csentry_group_iocs\": \"1\",\n" +
                "          \"csentry_group_labnetworks\": \"1\",\n" +
                "          \"csentry_group_mtca_amc\": \"1\",\n" +
                "          \"csentry_is_ioc\": \"1\",\n" +
                "          \"csentry_vlan\": \"CSLab_GeneralLab\",\n" +
                "          \"csentry_vlan_id\": \"3805\",\n" +
                "          \"instance\": \"dtl-llrf5-cpu-ioc.cslab.esss.lu.se\",\n" +
                "          \"job\": \"node\"\n" +
                "        },\n" +
                "        \"value\": [\n" +
                "          1620819560.746,\n" +
                "          \"0\"\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"metric\": {\n" +
                "          \"__name__\": \"up\",\n" +
                "          \"csentry_device_type\": \"MTCA-AMC\",\n" +
                "          \"csentry_domain\": \"cslab.esss.lu.se\",\n" +
                "          \"csentry_group_iocs\": \"1\",\n" +
                "          \"csentry_group_labnetworks\": \"1\",\n" +
                "          \"csentry_group_mtca_amc\": \"1\",\n" +
                "          \"csentry_is_ioc\": \"1\",\n" +
                "          \"csentry_vlan\": \"CSLab_BeamDiag\",\n" +
                "          \"csentry_vlan_id\": \"3880\",\n" +
                "          \"instance\": \"bcm01-cpu-201.cslab.esss.lu.se\",\n" +
                "          \"job\": \"node\"\n" +
                "        },\n" +
                "        \"value\": [\n" +
                "          1620819560.746,\n" +
                "          \"1\"\n" +
                "        ]\n" +
                "      }" +
                "    ]\n" +
                "  }\n" +
                "}";
        PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

        return new ServiceResponse<>(p, 200);
    }

    private ServiceResponse<PrometheusResponse> emptyHostList() {
        Gson g = new Gson();
        String jsonString = "{\n" +
                "  \"status\": \"success\",\n" +
                "  \"data\": {\n" +
                "    \"resultType\": \"vector\",\n" +
                "    \"result\": []\n" +
                "  }\n" +
                "}";
        PrometheusResponse p = g.fromJson(jsonString, PrometheusResponse.class);

        return new ServiceResponse<>(p, 200);
    }

    private StatusService createService() {
        PrometheusRepository prometheusRepository = new PrometheusRepository(env, httpClientService);

        return new StatusService(prometheusRepository);
    }
}
