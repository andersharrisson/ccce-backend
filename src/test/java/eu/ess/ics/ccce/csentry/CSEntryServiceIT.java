package eu.ess.ics.ccce.csentry;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.awx.service.AwxService;
import eu.ess.ics.ccce.csentry.model.AnsibleGroup;
import eu.ess.ics.ccce.csentry.model.CSEntryHost;
import eu.ess.ics.ccce.csentry.model.CSEntryInterface;
import eu.ess.ics.ccce.csentry.service.CSEntryService;
import eu.ess.ics.ccce.exceptions.CSEntryException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.util.ExternalServiceMock;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockserver.model.Parameter;
import org.springframework.core.env.Environment;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@ExtendWith(MockitoExtension.class)
@Tag("IntegrationTest")
public class CSEntryServiceIT extends ExternalServiceMock {

    private CSEntryService csentryService;
    @Mock
    private Environment environment;

    public CSEntryServiceIT() {
        super("/csentry.response");
    }

    @BeforeEach
    public void setup() {
        Mockito.when(environment.getProperty(CSEntryService.CSENTRY_URL)).thenReturn(ExternalServiceMock.TEST_URL);
        csentryService = new CSEntryService(environment);
    }

    @Test
    void searchHostSuccess() throws CSEntryException {
        createGetExpectationSuccess("/api/v1/network/hosts/search", Collections.emptyList(), "host_search.json");
        List<CSEntryHost> hosts = csentryService.searchHost("zoltan", 0,10);
        assertEquals(1, hosts.size());
    }

    @Test
    void searchHostFail() throws CSEntryException {
        createExpectationFail("GET","/api/v1/network/hosts/search", Collections.emptyList(), 422);
        assertThrows(CSEntryException.class, () -> csentryService.searchHost("zoltan", 0,10));
    }

    @Test
    void findHostByIdSuccess() throws CSEntryException {
        createGetExpectationSuccess("/api/v1/network/hosts",
                ImmutableList.of(new Parameter("id", "836")), "host_by_id.json");
        createGetExpectationSuccess("/api/v1/network/interfaces",
                ImmutableList.of(new Parameter("name", "zoltan-test-host")), "host_by_id_interface_1.json");
        createGetExpectationSuccess("/api/v1/network/interfaces",
                ImmutableList.of(new Parameter("name", "zoltan-test-host-2")), "host_by_id_interface_2.json");
        CSEntryHost host = csentryService.findHostById(836);
        assertNotNull(host);
        assertEquals(836, host.getId());
        assertEquals("zoltan-test-host.cslab.esss.lu.se", host.getFqdn());
        assertTrue(host.isIoc());
        assertEquals("MTCA-AMC", host.getDeviceType());
        assertEquals("Zoltan's test host", host.getDescription());
        assertEquals("zoltan-test-host", host.getName());
        assertEquals("zoltanrunyo", host.getUser());
        assertEquals("LabNetworks", host.getScope());
        assertEquals(new DateTime(2021, 2, 18, 10, 13).toDate().getTime(), host.getCreatedAt().getTime());
        assertEquals(1, ((Double) host.getAnsibleVars().get("var1")).longValue());
        assertEquals(2, ((Double) host.getAnsibleVars().get("var2")).longValue());
        assertEquals(2, ((List)host.getAnsibleVars().get("vm_owner")).size());
        assertEquals("zoltanrunyo", ((List)host.getAnsibleVars().get("vm_owner")).get(0));
        assertEquals("imretoth", ((List)host.getAnsibleVars().get("vm_owner")).get(1));

        assertEquals(2, host.getInterfaces().size());
        CSEntryInterface interface1 = host.getInterfaces().get(0);
        assertEquals(950, interface1.getId());
        assertEquals("zoltan-test-host", interface1.getName());
        assertEquals("cslab.esss.lu.se", interface1.getDomain());
        assertTrue(interface1.isMain());
        assertEquals("Utgard-DMSC", interface1.getNetwork());
        assertEquals("172.30.11.23", interface1.getIp());
        assertNull(interface1.getMac());
        assertEquals(2, interface1.getCnames().size());


        CSEntryInterface interface2 = host.getInterfaces().get(1);
        assertEquals(951, interface2.getId());
        assertEquals("zoltan-test-host-2", interface2.getName());
        assertEquals("cslab.esss.lu.se", interface2.getDomain());
        assertFalse(interface2.isMain());
        assertEquals("CSLab-BeamDiag", interface2.getNetwork());
        assertEquals("172.30.150.43", interface2.getIp());
        assertEquals("02:42:42:50:64:fa", interface2.getMac());
        assertEquals(1, interface2.getCnames().size());
    }

    @Test
    void findHostByIdFail() {
        createExpectationFail("GET", "/api/v1/network/hosts",
                ImmutableList.of(new Parameter("id", "836")), 422);
        assertThrows(CSEntryException.class, () -> csentryService.findHostById(836));
    }

    @Test
    void findHostByIdInterfaceFail() {
        createGetExpectationSuccess("/api/v1/network/hosts",
                ImmutableList.of(new Parameter("id", "836")), "host_by_id.json");
        createGetExpectationSuccess("/api/v1/network/interfaces",
                ImmutableList.of(new Parameter("name", "zoltan-test-host")), "host_by_id_interface_1.json");
        createExpectationFail("GET", "/api/v1/network/interfaces",
                ImmutableList.of(new Parameter("id", "836")), 422);
        assertThrows(CSEntryException.class, () -> csentryService.findHostById(836));
    }

    @Test
    void fetchAnsibleGroupByNameSuccess() throws CSEntryException {
        createGetExpectationSuccess("/api/v1/network/groups",
                ImmutableList.of(new Parameter("name", "test-group")), "ansible_group_by_name.json");
        List<AnsibleGroup> groups = csentryService.fetchAnsibleGroupByName("test-group");
        assertNotNull(groups);
        assertEquals(1, groups.size());
        assertEquals("test-group", groups.get(0).getName());
        assertEquals(0, groups.get(0).getHosts().size());
        assertNull(groups.get(0).getVars());
        assertEquals(2, groups.get(0).getChildren().size());
        assertEquals("test-group_idmz", groups.get(0).getChildren().get(0));
        assertEquals("test-group_tn", groups.get(0).getChildren().get(1));
    }

    @Test
    void fetchAnsibleGroupByNameNotFound() {
        createGetExpectationSuccess("/api/v1/network/groups",
                ImmutableList.of(new Parameter("name", "test-group")), "empty_array.json");
        assertThrows(EntityNotFoundException.class, () -> csentryService.fetchAnsibleGroupByName("test-group"));
    }

    @Test
    void fetchAnsibleGroupByNameFail() {
        createExpectationFail("GET", "/api/v1/network/groups",
                ImmutableList.of(new Parameter("name", "test-group")), 422);
        assertThrows(CSEntryException.class, () -> csentryService.fetchAnsibleGroupByName("test-group"));
    }
}
