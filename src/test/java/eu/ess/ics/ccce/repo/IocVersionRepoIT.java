/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo;

import eu.ess.ics.ccce.repo.impl.IocRepository;
import eu.ess.ics.ccce.repo.impl.IocVersionRepository;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.repo.model.IocMetaDataEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@SpringBootTest
public class IocVersionRepoIT {

    @Autowired
    private IocVersionRepository iocVersionRepository;
    @Autowired
    private IocRepository iocRepository;

    @AfterEach
    void clean() throws SQLException {
        Connection conn = DriverManager.
                getConnection("jdbc:h2:mem:db", "sa", "sa");
        Statement st = conn.createStatement();
        st.executeUpdate("delete from ioc_version");
        st.executeUpdate("delete from ioc");
        conn.close();
    }

    @Test
    void countEmptyDatabase(){
        long realCount = iocVersionRepository.countForPaging(0);
        assertEquals(0, realCount);
    }

    @Test
    void createIocVersionTest() {
        IocEntity ioc = createIoc();

        Long versionNumber = 1L;

        IocVersionEntity iocVersionEntity = createIocVersion("http://gitlab.com", "master",
                "CC-E2:AB-CD", null, null, null, ioc,"testUser",
                versionNumber);

        IocVersionEntity searchedVersion = iocVersionRepository.findByVersionId(ioc.getId(), versionNumber);
        assertNotNull(searchedVersion);
        assertEquals(searchedVersion.getNamingName(), iocVersionEntity.getNamingName());
        assertEquals(searchedVersion.getSourceUrl(), iocVersionEntity.getSourceUrl());
        assertEquals(searchedVersion.getSourceVersion(), iocVersionEntity.getSourceVersion());
    }

    @Test
    void findVersionShouldFail() {
        IocEntity ioc = createIoc();

        Long version = 1L;

        createIocVersion("http://git.com", "tag-v1", "TRE-45:86-BT", null, null,
                null, ioc, "test user", version);
        assertThrows(EmptyResultDataAccessException.class, () -> {
            iocVersionRepository.findByVersionId(ioc.getId(), version + 1);
        });
    }

    @Test
    void findVersion() {
        IocEntity ioc = createIoc();

        Long version = 1L;

        IocVersionEntity iocVersionEntity = createIocVersion("http://git.com", "tag-v1", "TRE-45:86-BT", null,
                null, null, ioc, "test user", version);

        IocVersionEntity foundVersion = iocVersionRepository.findByVersionId(ioc.getId(), version);

        assertEquals(foundVersion.getSourceUrl(), iocVersionEntity.getSourceUrl());
        assertEquals(foundVersion.getSourceVersion(), iocVersionEntity.getSourceVersion());
        assertEquals(foundVersion.getVersion(), iocVersionEntity.getVersion());
        assertEquals(foundVersion.getNamingName(), iocVersionEntity.getNamingName());
    }

    @Test
    void findLatestVersionShouldFail() {
        IocEntity ioc = createIoc();

        createIocVersion("http://git.com", "tag-v1", "TRE-45:86-BT", null, null,
                null, ioc, "test user", 1L);
        assertThrows(EmptyResultDataAccessException.class, () -> {
            iocVersionRepository.findLatest(ioc.getId() + 1);
        });
    }

    @Test
    void findAllVersions() {
        IocEntity ioc = createIoc();

        int number = 5;

        createIocVersions(ioc, number);

        List<IocVersionEntity> foundVersions = iocVersionRepository.findAllByIocId(ioc.getId(), 0, 100);
        assertEquals(number, foundVersions.size());
        assertEquals("test-user", foundVersions.get(1).getCreatedBy());
        assertEquals( "master-branch", foundVersions.get(3).getSourceVersion());
    }

    @Test
    void findAllVersionsShouldBeEmpty() {
        IocEntity ioc = createIoc();

        int number = 5;
        createIocVersions(ioc, number);

        List<IocVersionEntity> foundVersions = iocVersionRepository.findAllByIocId(ioc.getId(), 1, number +  1);
        assertEquals(foundVersions.size(), 0);
    }

    @Test
    void findVersionIds() {
        IocEntity ioc = createIoc();
        Long version = 1L;
        createIocVersion("http://test-host.url", "tag-01", "ABC-556:HPR-339",
                null,null, null, ioc, "test-user", version);
        createIocVersion("http://test-host.url", "tag-02", "ABC-556:HPR-339",
                null,null, null, ioc, "test-user", version + 1);
        createIocVersion("http://test-host.url", "tag-03", "ABC-556:HPR-339",
                null,null, null, ioc, "test-user", version + 2);

        List<Long> versionIdsForIoc = iocVersionRepository.findVersionIdsForIoc(ioc.getId());
        assertEquals(3, versionIdsForIoc.size());
    }


    private void createIocVersions(IocEntity ioc, int numberOfIocs) {

        for(int i = 1; i <= numberOfIocs; i++) {
            createIocVersion("http:test.repo.url", "master-branch", "ABC-DE:234-56" + i,
                    null, null, null, ioc, "test-user", Long.valueOf(i));
        }
    }


    private IocEntity createIoc() {
        IocEntity ioc = new IocEntity();
        ioc.setCustomName("test-ioc");
        ioc.setCreatedBy("test-user");

        iocRepository.createIoc(ioc);

        return ioc;
    }

    private IocVersionEntity createIocVersion(String sourceUrl, String sourceVersion, String namingName,
                                      String epicsVersion, String requireVersion, IocMetaDataEntity metadata,
                                      IocEntity ioc, String createdBy, Long version) {


        IocVersionEntity iocVersion = new IocVersionEntity();

        iocVersion.setVersion(version);
        iocVersion.setEpicsVersion(epicsVersion);
        iocVersion.setRequireVersion(requireVersion);
        iocVersion.setNamingName(namingName);
        iocVersion.setSourceUrl(sourceUrl);
        iocVersion.setSourceVersion(sourceVersion);
        iocVersion.setCreatedBy(createdBy);
        iocVersion.setIocMetaData(metadata);
        iocVersion.setIoc(ioc);

        iocVersionRepository.createIocVersion(iocVersion);

        return iocVersion;
    }
}
