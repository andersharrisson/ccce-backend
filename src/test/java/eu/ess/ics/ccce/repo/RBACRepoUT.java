/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import eu.ess.ics.ccce.exceptions.AuthenticationException;
import eu.ess.ics.ccce.exceptions.ParseException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.model.RBACToken;
import eu.ess.ics.ccce.service.HttpClientService;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import eu.ess.ics.ccce.service.HttpClientService.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@SpringBootTest
public class RBACRepoUT {
    @Mock
    private Environment env;
    @Mock
    private HttpClientService httpClientService;


    @Test
    void successfulLogin() throws RemoteServiceException, JsonProcessingException, ParseException {

        RBACRepository rbacRepo = createRepo();
        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");

        ServiceResponse<RBACToken> loginToken = createToken();

        Mockito.when(httpClientService.executePostRequest(
                ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.isNull(), ArgumentMatchers.eq(HttpClientService.XML_MEDIA_TYPE),
                ArgumentMatchers.eq(RBACToken.class))).thenReturn(loginToken);

        RBACToken rbacToken = rbacRepo.loginUser("testUser", "testPassword");

        assertEquals(loginToken.getEntity().getId(), rbacToken.getId());
        assertEquals(loginToken.getEntity().getUserName(), rbacToken.getUserName());
        assertEquals(loginToken.getEntity().getSignature(), rbacToken.getSignature());
    }

    @Test
    void badLogin() throws RemoteServiceException, JsonProcessingException, ParseException {

        RBACRepository rbacRepo = createRepo();
        Mockito.when(env.getProperty(ArgumentMatchers.anyString())).thenReturn("test-env-value");

        ServiceResponse<RBACToken> loginToken = badToken();

        Mockito.when(httpClientService.executePostRequest(
                ArgumentMatchers.any(OkHttpClient.class),
                ArgumentMatchers.any(Headers.class), ArgumentMatchers.anyString(),
                ArgumentMatchers.isNull(), ArgumentMatchers.eq(HttpClientService.XML_MEDIA_TYPE),
                ArgumentMatchers.eq(RBACToken.class))).thenReturn(loginToken);

        assertThrows(AuthenticationException.class, () -> {
            rbacRepo.loginUser("testUser", "testPassword");
        });
    }

    private ServiceResponse<RBACToken> createToken() throws JsonProcessingException {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<token>\n" +
                "    <id>AAAA-BBBBB-CCCC-DDDD-EEFFGGHH</id>\n" +
                "    <username>johndoe</username>\n" +
                "    <firstName>John</firstName>\n" +
                "    <lastName>Doe</lastName>\n" +
                "    <creationTime>1620823391053</creationTime>\n" +
                "    <expirationTime>1620852191053</expirationTime>\n" +
                "    <ip>10.0.42.5</ip>\n" +
                "    <roles>\n" +
                "        <role>CCDBAdministrator</role>\n" +
                "        <role>CableAdministrator</role>\n" +
                "        <role>IOCFactoryAdministrator</role>\n" +
                "        <role>NamingAdministrator</role>\n" +
                "    </roles>\n" +
                "    <signature>764AAF5B29498D3424226C6FF54E5BD7FC0751C35E8986CEBFA25E8867FBEBC2F9482C6189CDEFC8312BEB244D3E122B2E1876344BD8CC7C9B08420A3A536169</signature>\n" +
                "</token>";

        XmlMapper xmlMapper = new XmlMapper();

        RBACToken rbacToken = xmlMapper.readValue(xml, RBACToken.class);

        return new ServiceResponse<>(rbacToken, 200);
    }

    private ServiceResponse<RBACToken> badToken() throws JsonProcessingException {

        return new ServiceResponse<>(null, 401);
    }

    private RBACRepository createRepo() {
        return new RBACRepository(env, httpClientService);
    }
}
