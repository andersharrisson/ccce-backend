package eu.ess.ics.ccce.repo;

import eu.ess.ics.ccce.repo.impl.IocRepository;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.rest.model.request.IocOrder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/

@SpringBootTest
public class IocRepoIT {

    @Autowired
    private IocRepository repository;

    @AfterEach
    void clean() throws SQLException {
        Connection conn = DriverManager.
                getConnection("jdbc:h2:mem:db", "sa", "sa");
        Statement st = conn.createStatement();
        st.executeUpdate("delete from ioc");
        conn.close();
    }

    @Test
    public void saveIoc() {
        IocEntity ioc = createIoc("test-ioc", "Test IOC", "user");
        List<IocEntity> iocs = repository.findAll(null, null, null, null, 0, 10);
        IocEntity savedIoc = iocs.stream().filter(i -> ioc.getCustomName().equals(i.getCustomName())).findFirst().orElse(null);
        assertNotNull(savedIoc);
        assertEquals(ioc.getCustomName(), savedIoc.getCustomName());
        assertEquals(ioc.getDescription(), savedIoc.getDescription());
        assertEquals(ioc.getCreatedBy(), savedIoc.getCreatedBy());
    }

    @Test
    public void updateIoc() {
        IocEntity ioc = createIoc("test-ioc", "Test IOC", "user");
        List<IocEntity> iocs = repository.findAll(null, null, null, null, 0, 10);
        IocEntity savedIoc = iocs.stream().filter(i -> ioc.getCustomName().equals(i.getCustomName())).findFirst().orElse(null);
        assertNotNull(savedIoc);
        assertEquals(ioc.getCustomName(), savedIoc.getCustomName());
        assertEquals(ioc.getDescription(), savedIoc.getDescription());
        assertEquals(ioc.getCreatedBy(), savedIoc.getCreatedBy());
        savedIoc.setCustomName("test-ioc-2");
        savedIoc.setDescription("Test IOC 2");
        savedIoc.setCreatedBy("user2");
        repository.updateIoc(savedIoc);
        IocEntity updatedIoc = iocs.stream().filter(i -> savedIoc.getCustomName().equals(i.getCustomName())).findFirst().orElse(null);
        assertNotNull(updatedIoc);
        assertEquals(savedIoc.getCustomName(), updatedIoc.getCustomName());
        assertEquals(savedIoc.getDescription(), updatedIoc.getDescription());
        assertEquals(savedIoc.getCreatedBy(), updatedIoc.getCreatedBy());
    }

    @Test
    public void findIocById() {
        IocEntity ioc = createIoc("test-ioc", "Test IOC", "user");
        List<IocEntity> iocs = repository.findAll(null, null, null, null, 0, 10);
        IocEntity iocFound = iocs.stream().filter(i -> ioc.getCustomName().equals(i.getCustomName())).findFirst().orElse(null);
        assertNotNull(iocFound);
        IocEntity savedIoc = repository.findById(iocFound.getId());
        assertNotNull(savedIoc);
        assertEquals(ioc.getCustomName(), savedIoc.getCustomName());
        assertEquals(ioc.getDescription(), savedIoc.getDescription());
        assertEquals(ioc.getCreatedBy(), savedIoc.getCreatedBy());
    }

    @Test
    public void findAll() {
        createIocs();
        List<IocEntity> iocs = repository.findAll("ccce", "test", IocOrder.CUSTOM_NAME, false, 0, 10);
        assertEquals(5, iocs.size());
        assertEquals("test-9", iocs.get(0).getCustomName());
        assertEquals("test-8", iocs.get(1).getCustomName());
        assertEquals("test-7", iocs.get(2).getCustomName());
        assertEquals("test-6", iocs.get(3).getCustomName());
        assertEquals("test-5", iocs.get(4).getCustomName());
    }

    @Test
    public void countForPaging() {
        createIocs();
        assertEquals(5, repository.countForPaging("ccce", "test"));
    }

    @Test
    public void countEmptyTable(){
        Long emptyCount = repository.countAll();
        assertEquals(0L, emptyCount);
    }

    @Test
    public void countIocs(){
        int realNumberOfIocs = createIocs();
        Long iocCount = repository.countAll();
        assertEquals(realNumberOfIocs, iocCount);
    }

    private IocEntity createIoc(String name, String description, String user) {
        IocEntity ioc = new IocEntity();
        ioc.setCustomName(name);
        ioc.setDescription(description);
        ioc.setCreatedBy(user);
        repository.createIoc(ioc);
        return ioc;
    }

    private int createIocs() {
        int createdIocCount = 0;
        for(int i = 0; i < 5; i++) {
            createIoc("ioc-" + i, "IOC " + i, "user");
            createdIocCount++;
        }
        for(int i = 5; i < 10; i++) {
            createIoc("test-" + i, "Test " + i, "ccce");
            createdIocCount++;
        }

        return createdIocCount;
    }
}
