CREATE SEQUENCE IF NOT EXISTS public.awx_job_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;
-- DROP SEQUENCE public.deployment_id_seq;

CREATE SEQUENCE IF NOT EXISTS public.deployment_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;
-- DROP SEQUENCE public.deployment_job_mapping_id_seq;

CREATE SEQUENCE IF NOT EXISTS public.deployment_job_mapping_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;
-- DROP SEQUENCE public.host_id_seq;

CREATE SEQUENCE IF NOT EXISTS public.host_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;
-- DROP SEQUENCE public.host_mapping_id_seq;

CREATE SEQUENCE IF NOT EXISTS public.host_mapping_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;
-- DROP SEQUENCE public.ioc_id_seq;

CREATE SEQUENCE IF NOT EXISTS public.ioc_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;
-- DROP SEQUENCE public.ioc_version_id_seq;

CREATE SEQUENCE IF NOT EXISTS public.ioc_version_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;-- public.awx_job definition

-- Drop table

-- DROP TABLE awx_job;

CREATE TABLE IF NOT EXISTS awx_job (
                         id bigserial NOT NULL,
                         awx_job_id int8 NOT NULL,
                         start_at timestamp NULL,
                         finished_at timestamp NULL,
                         status varchar(32) NULL,
                         awx_job_url text NOT NULL,
                         CONSTRAINT awx_job_pk PRIMARY KEY (id)
);


-- public.host definition

-- Drop table

-- DROP TABLE host;

CREATE TABLE IF NOT EXISTS host (
                      id bigserial NOT NULL,
                      csentry_id int8 NOT NULL,
                      host text NULL,
                      network varchar(32) NULL,
                      CONSTRAINT host_pk PRIMARY KEY (id)
);


-- public.ioc definition

-- Drop table

-- DROP TABLE ioc;

CREATE TABLE IF NOT EXISTS ioc (
                     id bigserial NOT NULL,
                     custom_name varchar(64) NOT NULL,
                     description varchar(128) NULL,
                     created_by varchar(32) NOT NULL,
                     created_at timestamp NULL DEFAULT now(),
                     CONSTRAINT ioc_pk PRIMARY KEY (id),
                     CONSTRAINT ioc_un UNIQUE (custom_name)
);


-- public.host_mapping definition

-- Drop table

-- DROP TABLE host_mapping;

CREATE TABLE IF NOT EXISTS host_mapping (
                              id bigserial NOT NULL,
                              host int8 NULL,
                              ioc int8 NOT NULL,
                              created_by varchar(32) NOT NULL,
                              created_at timestamp(0) NOT NULL DEFAULT now(),
                              CONSTRAINT host_mapping_pk PRIMARY KEY (id),
                              CONSTRAINT host_mapping_fk FOREIGN KEY (ioc) REFERENCES ioc(id),
                              CONSTRAINT host_mapping_fk_1 FOREIGN KEY (host) REFERENCES host(id)
);


-- public.ioc_meta_data definition

-- Drop table

-- DROP TABLE public.ioc_meta_data;

CREATE TABLE IF NOT EXISTS ioc_meta_data (
                                             id bigserial NOT NULL,
                                             info json NOT NULL,
                                             "version" varchar NOT NULL,
                                             created_at timestamp NOT NULL DEFAULT now(),
                                             file_hash varchar NOT NULL,
                                             CONSTRAINT ioc_meta_data_pk PRIMARY KEY (id)
);


-- public.ioc_version definition

-- Drop table

-- DROP TABLE ioc_version;

CREATE TABLE IF NOT EXISTS ioc_version (
                             id bigserial NOT NULL,
                             created_at timestamp NOT NULL DEFAULT now(),
                             created_by varchar(32) NOT NULL,
                             source_url text NOT NULL,
                             source_version varchar(64) NULL,
                             ioc int8 NOT NULL,
                             "version" int8 NOT NULL,
                             meta_data int8 NULL,
                             naming_name varchar NULL,
                             epics_version varchar NULL,
                             require_version varchar NULL,
                             CONSTRAINT ioc_version_pk PRIMARY KEY (id),
                             CONSTRAINT ioc_version_fk FOREIGN KEY (ioc) REFERENCES ioc(id),
                             CONSTRAINT ioc_version_meta_data_fk FOREIGN KEY (meta_data) REFERENCES ioc_meta_data(id)
);


-- public.deployment definition

-- Drop table

-- DROP TABLE deployment;

CREATE TABLE IF NOT EXISTS deployment (
                            id bigserial NOT NULL,
                            ioc_version int8 NOT NULL,
                            host_mapping int8 NOT NULL,
                            deployment_state varchar(16) NOT NULL,
                            description varchar(128) NULL,
                            created_by varchar(32) NULL,
                            created_at timestamp NOT NULL DEFAULT now(),
                            start_time timestamp NULL,
                            is_intended bool NULL,
                            is_active bool NULL,
                            finished_at timestamp(0) NULL,
                            CONSTRAINT deployment_pk PRIMARY KEY (id),
                            CONSTRAINT deployment_fk FOREIGN KEY (ioc_version) REFERENCES ioc_version(id),
                            CONSTRAINT deployment_fk2 FOREIGN KEY (host_mapping) REFERENCES host_mapping(id)
);


-- public.deployment_job_mapping definition

-- Drop table

-- DROP TABLE deployment_job_mapping;

CREATE TABLE IF NOT EXISTS deployment_job_mapping (
                                        id bigserial NOT NULL,
                                        deployment int8 NOT NULL,
                                        job int8 NOT NULL,
                                        CONSTRAINT deployment_job_mapping_pk PRIMARY KEY (id),
                                        CONSTRAINT deployment_job_mapping_fk FOREIGN KEY (job) REFERENCES awx_job(id),
                                        CONSTRAINT deployment_job_mapping_fk_1 FOREIGN KEY (deployment) REFERENCES deployment(id)
);