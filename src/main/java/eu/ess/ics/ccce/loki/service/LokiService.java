/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.loki.service;

import eu.ess.ics.ccce.exceptions.LokiException;
import eu.ess.ics.ccce.exceptions.LokiServiceException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.loki.model.Root;
import eu.ess.ics.ccce.rest.model.response.LokiResponse;
import eu.ess.ics.ccce.service.HttpClientService;
import eu.ess.ics.ccce.util.ConversionUtil;
import eu.ess.ics.ccce.util.Utils;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class LokiService {

    private static final String QUERY_ENDPOINT = "loki/api/v1/query_range";
    private static final String QUERY_PARAM = "query";
    private static final String HOST_PARAM = "host";
    private static final String STEP_PARAM = "step";
    private static final String TIME_RANGE_START = "start";
    private static final String DIRECTION = "direction";
    private static final String BACKWARD = "BACKWARD";
    private static final Integer DEFAULT_TIME_RANGE = 1440;
    private static final String STEP_VALUE = "5m";
    private static final String LIMIT = "limit";
    private static final Integer LIMIT_SIZE = 5000;
    private static final String IOC_NAME_FILTER = "|=";

    private final Logger logger = LoggerFactory.getLogger(LokiService.class);

    @Value("${loki.server.address}")
    private String baseUrl;

    private final HttpClientService httpClientService;

    @Autowired
    public LokiService(HttpClientService httpClientService) {
        this.httpClientService = httpClientService;
    }

    /**
     * Gets the logs for a specific host. timeRange is optional, value is in minutes.
     * If hostname contains network -> syslog will be queried.
     * If hostname is without network -> procServ logs will be queried.
     *
     * @param hostName the name of the host.
     * @param iocName name of the ioc for querying procServ logs (optional, can be null)
     * @param timeRange optional field for timeRange. Value is in minutes.
     *                  Default value (if not set) is 120.
     *
     * @return List of strings containing the logs (syslog, or procServ depending on the hostname).
     */
    public LokiResponse getLogLines(String hostName, String iocName, Integer timeRange) {

        final OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

        try {
            HttpClientService.ServiceResponse<Root> lokiResponse =
                    httpClientService.executeGetRequest(client, createAuthHeader(),
                            buildUrl(hostName, iocName, timeRange), Root.class);

            if (HttpClientService.isSuccessHttpStatusCode(lokiResponse.getStatusCode())) {
                return ConversionUtil.convertToLokiResponse(lokiResponse.getEntity());
            }

            throw new LokiException("get log lines (query: " + hostName + ") response code: "
                    + lokiResponse.getStatusCode());

        } catch (MalformedURLException | RemoteServiceException e) {
            logger.error("Exception while trying to get logs from Loki", e);
            throw new LokiServiceException("Unable to call Loki service", e);
        }
    }

    private String getEndpointUrl(String endpointPostfix) throws MalformedURLException {
        return new URL(new URL(baseUrl), endpointPostfix).toString();
    }

    private String buildUrl(String hostname, String iocName, Integer timeRange)
            throws MalformedURLException {

        Integer queryTimeRange = DEFAULT_TIME_RANGE;

        UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(
                getEndpointUrl(QUERY_ENDPOINT));

        //setting up the hostName as query param
        String hostInfo = "{" + HOST_PARAM + "=\"" + hostname + "\"}";

        //if iocName exists, set up as query parameter (to gather procServ log)
        if (StringUtils.isNotEmpty(iocName)) {

            hostInfo = hostInfo + IOC_NAME_FILTER + "\""
                    + ConversionUtil.transformIocName(iocName)
                    + "\"";
        }

        //additional query parameters
        ub.queryParam(DIRECTION, BACKWARD);
        ub.queryParam(QUERY_PARAM, hostInfo);
        ub.queryParam(STEP_PARAM, STEP_VALUE);
        ub.queryParam(LIMIT, LIMIT_SIZE);

        if (timeRange != null) {
            queryTimeRange = timeRange;
        }

        LocalDateTime tmRng = LocalDateTime.ofInstant(new Date().toInstant(),
                ZoneId.systemDefault());

        LocalDateTime startTime = tmRng.minusMinutes(queryTimeRange);

        ub.queryParam(TIME_RANGE_START, startTime.atZone(ZoneId.systemDefault()).toEpochSecond());

        return ub.toUriString();
    }

    /**
     * Creates authentication header for Loki queries.
     *
     * @return headers containing the authentication info
     */
    private Headers createAuthHeader() {
        return Headers.of();
    }
}
