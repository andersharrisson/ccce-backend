/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.loki.model;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
public class Store {
    private Long totalChunksRef;
    private Long totalChunksDownloaded;
    private Double chunksDownloadTime;
    private Long headChunkBytes;
    private Long headChunkLines;
    private Long decompressedBytes;
    private Long decompressedLines;
    private Long compressedBytes;
    private Long totalDuplicates;

    public Long getTotalChunksRef() {
        return totalChunksRef;
    }

    public void setTotalChunksRef(Long totalChunksRef) {
        this.totalChunksRef = totalChunksRef;
    }

    public Long getTotalChunksDownloaded() {
        return totalChunksDownloaded;
    }

    public void setTotalChunksDownloaded(Long totalChunksDownloaded) {
        this.totalChunksDownloaded = totalChunksDownloaded;
    }

    public Double getChunksDownloadTime() {
        return chunksDownloadTime;
    }

    public void setChunksDownloadTime(Double chunksDownloadTime) {
        this.chunksDownloadTime = chunksDownloadTime;
    }

    public Long getHeadChunkBytes() {
        return headChunkBytes;
    }

    public void setHeadChunkBytes(Long headChunkBytes) {
        this.headChunkBytes = headChunkBytes;
    }

    public Long getHeadChunkLines() {
        return headChunkLines;
    }

    public void setHeadChunkLines(Long headChunkLines) {
        this.headChunkLines = headChunkLines;
    }

    public Long getDecompressedBytes() {
        return decompressedBytes;
    }

    public void setDecompressedBytes(Long decompressedBytes) {
        this.decompressedBytes = decompressedBytes;
    }

    public Long getDecompressedLines() {
        return decompressedLines;
    }

    public void setDecompressedLines(Long decompressedLines) {
        this.decompressedLines = decompressedLines;
    }

    public Long getCompressedBytes() {
        return compressedBytes;
    }

    public void setCompressedBytes(Long compressedBytes) {
        this.compressedBytes = compressedBytes;
    }

    public Long getTotalDuplicates() {
        return totalDuplicates;
    }

    public void setTotalDuplicates(Long totalDuplicates) {
        this.totalDuplicates = totalDuplicates;
    }
}
