/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.prometheus.Result;
import eu.ess.ics.ccce.repo.impl.DeploymentRepository;
import eu.ess.ics.ccce.repo.impl.IocRepository;
import eu.ess.ics.ccce.rest.model.response.GeneralStatisticsResponse;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/

@Service
public class StatisticsService {
    private final Logger logger = LoggerFactory.getLogger(StatisticsService.class);

    private final StatusService statusService;
    private final IocRepository iocRepository;
    private final DeploymentRepository deploymentRepository;

    @Autowired
    public StatisticsService(StatusService statusService,
                             IocRepository iocRepository,
                             DeploymentRepository deploymentRepository) {
        this.statusService = statusService;
        this.iocRepository = iocRepository;
        this.deploymentRepository = deploymentRepository;
    }

    public GeneralStatisticsResponse calculateStatistics(UserDetails user) {
        GeneralStatisticsResponse result = new GeneralStatisticsResponse();

        logger.debug("Checking active ioc hosts from Prometheus");
        List<Result> activeHosts = statusService.listActiveIocHosts();

        result.setNumberOfActiveHosts(activeHosts.size());

        logger.debug("Checking active iocs from Prometheus");
        result.setNumberOfActiveIocs(statusService.countAllActiveIocs());

        logger.debug("Checking number of IOCs created");
        result.setNumberOfCreatedIocs(iocRepository.countAll());

        logger.debug("Counting number of total deployments");
        result.setNumberOfDeployments(deploymentRepository.countAllDeployments());

        logger.debug("Counting number of active deployments");
        result.setNumberOfActiveDeployments(deploymentRepository.countActiveDeployments());



        return result;
    }
}
