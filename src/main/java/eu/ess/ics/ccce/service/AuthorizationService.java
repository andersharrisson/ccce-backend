package eu.ess.ics.ccce.service;

import eu.ess.ics.ccce.exceptions.GitException;
import eu.ess.ics.ccce.exceptions.GitRepoNotFoundException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.exceptions.UnauthorizedException;
import eu.ess.ics.ccce.model.UserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Service
public class AuthorizationService {
    Logger logger = LoggerFactory.getLogger(AuthorizationService.class);
    private final GitLabService gitLabService;

    @Autowired
    public AuthorizationService(GitLabService gitLabService){
        this.gitLabService = gitLabService;
    }

    public void authorizeIocs(List<String> sourceUrls, UserDetails userDetails) {
        for (String url : sourceUrls) {
            authorizeIoc(url, userDetails);
        }
    }

    public void authorizeIoc(String sourceUrl, UserDetails userDetails) {
        try {
            List<String> members = gitLabService.getAllProjectMembers(sourceUrl);
            String userName = userDetails.getUserName();
            if (!memberListContains(members, userName)) {
                throw new UnauthorizedException("User (" + userName + ") is not permitted to deploy IOC Version (Source URL: " + sourceUrl + ")");
            }
        } catch (GitRepoNotFoundException e) {
            logger.warn("Git repository (" + sourceUrl + ") was not found while trying to authorize IOC");
        } catch (GitException e) {
            logger.error("GitException while trying to authorize IOC", e);
            throw e;
        } catch (UnauthorizedException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while trying to authorize IOC", e);
            throw new ServiceException(e.getMessage());
        }
    }

    private boolean memberListContains(List<String> members, String userName) {
        return members.stream().anyMatch(memberName -> memberName.contains(userName));
    }
}
