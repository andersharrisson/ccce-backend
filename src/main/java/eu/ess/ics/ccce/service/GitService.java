/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import eu.ess.ics.ccce.exceptions.GitException;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class GitService {

    /**
     * Connects to git with username, and password, and gets all tags
     *
     * @param userName The username to connect with to the Git
     * @param password The password to connect with to the Git
     * @param repoUrl The URL of the Git server
     * @return List of tags. If no tags can be found the function will return with empty list.
     * @throws GitException Exception if any problem occurs during Git call
     */
    public List<String> tagList(String userName, String password, String repoUrl) throws GitException {
        List<String> result = new ArrayList<>();

        try {
            Collection<Ref> tags = Git.lsRemoteRepository()
                    .setTags(true)
                    .setRemote(repoUrl)
                    .setCredentialsProvider(new UsernamePasswordCredentialsProvider(userName, password))
                    .call();

            if (tags != null) {
                result = tags.stream()
                        .map(Ref::getName)
                        .collect(Collectors.toList());
            }
        } catch (GitAPIException e) {
            throw new GitException(e.getMessage());
        }

        return result;
    }
}
