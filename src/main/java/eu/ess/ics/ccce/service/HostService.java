/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import eu.ess.ics.ccce.csentry.model.CSEntryHost;
import eu.ess.ics.ccce.csentry.model.CSEntryInterface;
import eu.ess.ics.ccce.csentry.service.CSEntryService;
import eu.ess.ics.ccce.exceptions.CSEntryException;
import eu.ess.ics.ccce.exceptions.CSEntryServiceException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.repo.IDeploymentRepository;
import eu.ess.ics.ccce.repo.IHostRepository;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.HostMappingEntity;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.rest.model.CSEntryHostWithStatus;
import eu.ess.ics.ccce.rest.model.Host;
import eu.ess.ics.ccce.rest.model.HostStatus;
import eu.ess.ics.ccce.rest.model.request.HostOrder;
import eu.ess.ics.ccce.rest.model.response.PagedCSEntryHostResponse;
import eu.ess.ics.ccce.rest.model.response.PagedHostResponse;
import eu.ess.ics.ccce.util.ConversionUtil;
import eu.ess.ics.ccce.util.PagingLimitDto;
import eu.ess.ics.ccce.util.Utils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class HostService {
    Logger logger = LoggerFactory.getLogger(HostService.class);

    private final Utils utils;
    private final IHostRepository hostRepository;
    private final StatusService statusService;
    private final CSEntryService csEntryService;
    private final IDeploymentRepository deploymentRepository;

    @Autowired
    public HostService(Utils utils, IHostRepository hostRepository, StatusService statusService,
                       @Qualifier("csentryService") CSEntryService csEntryService, IDeploymentRepository deploymentRepository) {
        this.utils = utils;
        this.hostRepository = hostRepository;
        this.statusService = statusService;
        this.csEntryService = csEntryService;
        this.deploymentRepository = deploymentRepository;
    }

    public PagedHostResponse findAll(String query, HostOrder orderBy, Boolean isAsc, Integer page, Integer limit) {

        PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit);

        String searchTextString = StringUtils.isEmpty(query) ? null : query;

        List<HostEntity> hostEntities = hostRepository.findAll(searchTextString, orderBy, isAsc,
                pagingLimitDto.getPage(), pagingLimitDto.getLimit());

        List<Host> hosts = new ArrayList<>();

        if (hostEntities != null) {
            hosts = hostEntities.stream().
                    map(h -> ConversionUtil.convertToHost(h, getHostStatus(h.getHost())))
                    .collect(Collectors.toList());
        }

        long totalCount = hostRepository.countForPaging(searchTextString);

        return new PagedHostResponse(totalCount, hosts.size(), pagingLimitDto.getPage(),
                pagingLimitDto.getLimit(), hosts);
    }

    public PagedCSEntryHostResponse findAllFromCSEntry(String query, Integer page, Integer limit) throws CSEntryServiceException, CSEntryException {
        PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit, CSEntryService.CSENTRY_PAGE_LIMIT);

        String searchTextString = StringUtils.isEmpty(query) ? null : query;
        List<String> activeHosts = statusService.listIocHosts();
        List<CSEntryHostWithStatus> hostExtensions = csEntryService.searchHost(searchTextString, pagingLimitDto.getPage(),
                pagingLimitDto.getLimit()).stream().map(host ->
                new CSEntryHostWithStatus(host, assigned(host.getId()), activeHosts.contains(host.getFqdn()) ? HostStatus.AVAILABLE : HostStatus.UNREACHABLE))
                .collect(Collectors.toList());
        return new PagedCSEntryHostResponse(0, hostExtensions.size(), pagingLimitDto.getPage(), pagingLimitDto.getLimit(), hostExtensions);
    }

    public CSEntryHostWithStatus findHostByCSEntryId(long hostCSEntryId) throws CSEntryServiceException, EntityNotFoundException, CSEntryException {
        CSEntryHost csEntryHost = csEntryService.findHostById(hostCSEntryId);
        return new CSEntryHostWithStatus(csEntryHost, assigned(hostCSEntryId), getHostStatus(csEntryHost.getFqdn()));
    }

    public Host linkHostToIoc(Long hostId, IocEntity ioc, String userName) {
        HostEntity hostEntity = null;

        if (hostId != null) {
            hostEntity = hostRepository.findHostById(hostId);

            HostMappingEntity hostMapping = new HostMappingEntity();

            hostMapping.setHost(hostEntity);
            hostMapping.setIoc(ioc);
            hostMapping.setCreatedBy(userName);

            hostRepository.createHostMapping(hostMapping);
        }

        return ConversionUtil.convertToHost(hostEntity, getHostStatus(hostEntity.getHost()));
    }

    public void createEmptyHostMapping(IocEntity ioc, String userName) {
        HostMappingEntity hostMapping = new HostMappingEntity();

        hostMapping.setHost(null);
        hostMapping.setIoc(ioc);
        hostMapping.setCreatedBy(userName);

        hostRepository.createHostMapping(hostMapping);
    }

    public Host findLatestByIocId(long iocId) {

        try {
            HostMappingEntity latestHostMapping = hostRepository.findLatestMappingByIocId(iocId);

            HostEntity latestHost = latestHostMapping == null ? null : latestHostMapping.getHost();
            String hostName = latestHost == null ? null : latestHost.getHost();

            return ConversionUtil.convertToHost(latestHost, getHostStatus(hostName));
        } catch (EmptyResultDataAccessException e) {
        }

        return null;
    }

    public Host hostDetails(long hostId) {
        HostEntity hostEnt = hostRepository.findHostById(hostId);

        return ConversionUtil.convertToHost(hostEnt, getHostStatus(hostEnt.getHost()));
    }

    public HostStatus getHostStatus(String host) {
        if (StringUtils.isNotEmpty(host)) {
            return BooleanUtils.toBoolean(statusService.checkHostIsActive(host)) ?
                    HostStatus.AVAILABLE : HostStatus.UNREACHABLE;
        }

        return HostStatus.UNREACHABLE;
    }

    public HostEntity hostIdForCSEntryHostIfAlreadyExists(long csEntryHostId) {
        try {
            return hostRepository.findHostByCSEntryId(csEntryHostId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public HostEntity hostIdForCSEntryHostIfNotExists(CSEntryHost csEntryHost) {
        CSEntryInterface mainInterface = csEntryHost.getInterfaces().stream().filter(CSEntryInterface::isMain).findFirst().orElse(null);
        String network = mainInterface != null ? mainInterface.getNetwork() : null;

        HostEntity hostEntity = new HostEntity();
        hostEntity.setCsEntryId(csEntryHost.getId());
        hostEntity.setHost(csEntryHost.getFqdn());
        hostEntity.setNetwork(network);

        hostRepository.createHost(hostEntity);

        return hostEntity;
    }

    public List<IocEntity> findConfiguredIocsByCSEntryHostId(long csEntryHostId) {
        return hostRepository.findLatestMappingsByCSEntryId(csEntryHostId).stream()
                .map(HostMappingEntity::getIoc).collect(Collectors.toList());
    }

    private boolean assigned(long hostCSEntryId) {
        return !findConfiguredIocsByCSEntryHostId(hostCSEntryId).isEmpty()
                || !deploymentRepository.findAllActiveByCSEntryHostId(hostCSEntryId).isEmpty();
    }
}
