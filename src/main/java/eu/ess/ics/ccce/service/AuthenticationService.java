/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import eu.ess.ics.ccce.configuration.JwtTokenUtil;
import eu.ess.ics.ccce.exceptions.AuthenticationException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.model.LoginTokenDto;
import eu.ess.ics.ccce.model.RBACToken;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.rest.model.response.LoginResponse;
import eu.ess.ics.ccce.util.EncryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class AuthenticationService {

    @Value("${jwt.expire.in.minutes}")
    private Integer tokenExpiration;

    private final RBACService rbacService;
    private final JwtTokenUtil jwtTokenUtil;
    private final EncryptUtil encryptUtil;

    @Autowired
    public AuthenticationService(RBACService rbacService, JwtTokenUtil jwtTokenUtil,
                                 EncryptUtil encryptUtil) {
        this.rbacService = rbacService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.encryptUtil = encryptUtil;
    }

    public LoginResponse login(String userName, String password) {

        LoginTokenDto tokenDto = rbacService.loginUser(userName, password);

        UserDetails userDetails = new UserDetails();
        userDetails.setUserName(userName);
        userDetails.setToken(encryptUtil.encrypt(tokenDto.getToken()));

        long tokenExpiresIn = Math.min(tokenExpiration, tokenDto.getExpirationDuration());

        return new LoginResponse(jwtTokenUtil.generateToken(userDetails, tokenExpiresIn));

    }

    public LoginResponse renewToken(UserDetails user) {

        LoginTokenDto token = rbacService.renewToken(user.getToken());
        user.setToken(encryptUtil.encrypt(user.getToken()));

        long tokenExpiresIn = Math.min(tokenExpiration, token.getExpirationDuration());

        return new LoginResponse(jwtTokenUtil.generateToken(user, tokenExpiresIn));
    }

    public void logout(UserDetails user) throws AuthenticationException, ServiceException {
        rbacService.logoutUser(user.getToken());
    }
}
