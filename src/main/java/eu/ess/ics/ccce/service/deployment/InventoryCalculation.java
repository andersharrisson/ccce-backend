package eu.ess.ics.ccce.service.deployment;

import com.google.gson.Gson;
import eu.ess.ics.ccce.model.DeploymentDto;
import eu.ess.ics.ccce.rest.model.InventoryHost;
import eu.ess.ics.ccce.rest.model.InventoryIoc;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class InventoryCalculation {
    private Map<String, InventoryHost> inventory;
    private final List<DeploymentDto> deployments;
    Logger logger = LoggerFactory.getLogger(InventoryCalculation.class);

    public InventoryCalculation(List<DeploymentDto> deployments) {
        this.deployments = deployments;
    }

    public void createInventory() {
        logger.debug("Inventory creation started --------------------");
        inventory = new HashMap<>();
        logger.debug("Active and Intended deployments: ");

        for (DeploymentDto deployment : deployments) {
            logger.debug(deployment.toString());
            if (deployment.isActive()) {
                DeploymentDto intendedForIoc = deployments.stream()
                        .filter(d -> d.isIntended() && d.getIocInventoryName()
                                .equals(deployment.getIocInventoryName())).findFirst().orElse(null);
                if (intendedForIoc == null) {
                    InventoryIoc ioc = new InventoryIoc(deployment);
                    String activeHost = deployment.getHost();
                    putToInventory(activeHost, ioc);
                } else {
                    String activeHost = deployment.getHost();
                    if (!inventory.containsKey(activeHost)) {
                        inventory.put(activeHost, new InventoryHost(new ArrayList<>()));
                    }

                    InventoryIoc ioc = new InventoryIoc(intendedForIoc);
                    String intendedHost = intendedForIoc.getHost();
                    if (!StringUtils.isEmpty(intendedHost)) {
                        putToInventory(intendedHost, ioc);
                    }
                }
            }
            if (deployment.isIntended()) {
                DeploymentDto activeForIoc = deployments.stream()
                        .filter(d -> d.isActive() && d.getIocInventoryName()
                                .equals(deployment.getIocInventoryName())).findFirst().orElse(null);
                if (activeForIoc == null) {
                    InventoryIoc ioc = new InventoryIoc(deployment);
                    String intendedHost = deployment.getHost();
                    if (!StringUtils.isEmpty(intendedHost)) {
                        putToInventory(intendedHost, ioc);
                    }
                }
            }
        }
        logger.debug("====================");
        logger.debug("Inventory: ");
        logger.debug(new Gson().toJson(inventory));
        logger.debug("Inventory creation finished --------------------");
    }

    public Map<String, InventoryHost> getInventory() {
        return inventory;
    }

    private void putToInventory(String host, InventoryIoc ioc) {
        if (inventory.containsKey(host)) {
            if (!containsIoc(inventory.get(host).getIocs(), ioc)) {
                inventory.get(host).getIocs().add(ioc);
            }
        } else {
            List<InventoryIoc> iocs = new ArrayList<>();
            iocs.add(ioc);
            inventory.put(host, new InventoryHost(iocs));
        }
    }

    private boolean containsIoc(List<InventoryIoc> iocs, InventoryIoc ioc) {
        return iocs.stream().anyMatch(i -> StringUtils.equals(i.getName(), ioc.getName()));
    }
}
