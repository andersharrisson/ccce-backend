/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import eu.ess.ics.ccce.exceptions.AuthenticationException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.model.LoginTokenDto;
import eu.ess.ics.ccce.model.RBACToken;
import eu.ess.ics.ccce.repo.RBACRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;


/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class RBACService {

    Logger logger = LoggerFactory.getLogger(RBACService.class);

    private final RBACRepository rbacRepo;

    @Autowired
    public RBACService(RBACRepository rbacRepo) {
        this.rbacRepo = rbacRepo;
    }

    public LoginTokenDto loginUser(String userName, String password) {

        try {
            RBACToken rbacToken = rbacRepo.loginUser(userName, password);

            LocalDateTime createTime = LocalDateTime.ofInstant(rbacToken.getCreationTime().toInstant(),
                    ZoneId.systemDefault());

            LocalDateTime expirationTime = LocalDateTime.ofInstant(rbacToken.getExpirationTime().toInstant(),
                    ZoneId.systemDefault());

            Duration duration = Duration.between(createTime, expirationTime);
            long expireInMinutes = Math.abs(duration.toMinutes());

            return new LoginTokenDto(rbacToken.getId(), expireInMinutes);
        } catch (ServiceException | AuthenticationException e) {
            logger.error("Error while trying to log in user to RBAC", e);
            throw e;
        }
    }

    public RBACToken getUserInfoFromToken(String token) {
        return rbacRepo.userInfoFromToken(token);
    }

    public boolean tokenIsValidCheck(String token) {

        return rbacRepo.checkTokenIsValid(token);

    }

    public LoginTokenDto renewToken(String token) throws ServiceException, AuthenticationException {

        RBACToken rbacToken = rbacRepo.renewToken(token);

        LocalDateTime createTime = LocalDateTime.ofInstant(rbacToken.getCreationTime().toInstant(),
                ZoneId.systemDefault());

        LocalDateTime expirationTime = LocalDateTime.ofInstant(rbacToken.getExpirationTime().toInstant(),
                ZoneId.systemDefault());

        Duration duration = Duration.between(createTime, expirationTime);
        long expireInMinutes = Math.abs(duration.toMinutes());

        return new LoginTokenDto(rbacToken.getId(), expireInMinutes);
    }

    public void logoutUser(String token) throws ServiceException, AuthenticationException {
        rbacRepo.deleteToken(token);
    }
}
