package eu.ess.ics.ccce.service.deployment;

import eu.ess.ics.ccce.model.UserDetails;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class DeploymentTask {
    private final List<Long> iocIds;
    private final boolean undeploy;
    private final String comment;
    private final UserDetails userDetails;

    private  DeploymentTask(List<Long> iocIds, boolean undeploy, String comment, UserDetails userDetails) {
        this.iocIds = iocIds;
        this.undeploy = undeploy;
        this.comment = comment;
        this.userDetails = userDetails;
    }

    public static DeploymentTask createDeploymentTask(List<Long> iocIds, String comment, UserDetails userDetails) {
        return new DeploymentTask(iocIds, false, comment, userDetails);
    }

    public static DeploymentTask createUndeploymentTask(List<Long> iocIds, String comment, UserDetails userDetails) {
        return new DeploymentTask(iocIds, true, comment, userDetails);
    }

    public List<Long> getIocIds() {
        return iocIds;
    }

    public boolean isUndeploy() {
        return undeploy;
    }

    public String getComment() {
        return comment;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }
}
