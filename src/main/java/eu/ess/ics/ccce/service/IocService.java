/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.ess.ics.ccce.ccdb.model.NameDetail;
import eu.ess.ics.ccce.ccdb.service.CCDBService;
import eu.ess.ics.ccce.exceptions.CCDBException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.IncompleteRequestException;
import eu.ess.ics.ccce.exceptions.MetaDataFileException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.repo.IIocRepository;
import eu.ess.ics.ccce.repo.IIocVersionRepository;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.repo.model.IocMetaDataEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import eu.ess.ics.ccce.rest.model.*;
import eu.ess.ics.ccce.rest.model.request.IocOrder;
import eu.ess.ics.ccce.rest.model.response.PagedIocResponse;
import eu.ess.ics.ccce.util.ConversionUtil;
import eu.ess.ics.ccce.util.PagingLimitDto;
import eu.ess.ics.ccce.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class IocService {

    Logger logger = LoggerFactory.getLogger(IocService.class);

    private final IIocRepository iocRepository;
    private final IIocVersionRepository iocVersionRepository;
    private final Utils utils;
    private final IocVersionService iocVersionService;
    private final HostService hostService;
    private final DeploymentService deploymentService;
    private final StatusService statusService;
    private final AuthorizationService authorizationService;
    private final MetadataService metadataService;
    private final CCDBService ccdbService;

    @Autowired
    public IocService(IIocRepository iocRepository, IIocVersionRepository iocVersionRepository,
                      IocVersionService iocVersionService, Utils utils, HostService hostService,
                      DeploymentService deploymentService, StatusService statusService,
                      AuthorizationService authorizationService, MetadataService metadataService,
                      CCDBService ccdbService) {
        this.iocRepository = iocRepository;
        this.iocVersionRepository = iocVersionRepository;
        this.utils = utils;
        this.iocVersionService = iocVersionService;
        this.hostService = hostService;
        this.deploymentService = deploymentService;
        this.statusService = statusService;
        this.authorizationService = authorizationService;
        this.metadataService = metadataService;
        this.ccdbService = ccdbService;
    }

    public Ioc searchIcById(long id) {

        try {

            IocEntity iocEnt = iocRepository.findById(id);

            return extractDeployment(iocEnt);

        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("IOC", id);
        }
    }

    @Transactional
    public Ioc createNewIoc(CreateIoc iocToCreate, HostEntity host, UserDetails userDetails) throws MetaDataFileException, JsonProcessingException {
        String userName = userDetails.getUserName();
        authorizationService.authorizeIoc(iocToCreate.getSourceUrl(), userDetails);

        //parse and store metadata
        IocMetaDataEntity metaDataEntity = null;
        if (StringUtils.isNotEmpty(iocToCreate.getSourceUrl()) && StringUtils.isNotEmpty(iocToCreate.getSourceVersion())) {
            metaDataEntity = metadataService.createAndPersistMetaDataEntity(iocToCreate.getSourceUrl(),
                    iocToCreate.getSourceVersion());
        }

        NameDetail namingDetail;
        try {
            namingDetail = ccdbService.getIocById(iocToCreate.getExternalNameId());
        } catch (CCDBException e) {
            logger.error("Naming name not found in CCDB while trying to create IOC");
            throw new IncompleteRequestException("Naming name not found in CCDB");
        }

        IocEntity ioc = new IocEntity();
        ioc.setCreatedBy(userName);
        ioc.setDescription(iocToCreate.getDescription());
        ioc.setExternalNameId(namingDetail.getId());

        iocRepository.createIoc(ioc);

        if (host != null) {
            hostService.linkHostToIoc(host.getId(), ioc, userName);
        }

        IocVersionEntity version = iocVersionService.createIocVersion(ioc.getId(),
                new BaseIocVersion(iocToCreate.getSourceUrl(), iocToCreate.getSourceVersion()),
                metaDataEntity, namingDetail.getName(), namingDetail.getNameId(), userName);

        Host actualHost = hostService.findLatestByIocId(ioc.getId());

        logger.debug("IOC CREATED by [{}] with parameters: {}", userName, iocToCreate);

        String iocName = ConversionUtil.getIocInventoryName(version);
        String hostName = actualHost == null ? null : actualHost.getHost();

        return ConversionUtil.convertToIoc(ioc, version, null, actualHost,
                statusService.checkIocIsActive(iocName, hostName),
                statusService.checkRepoIsDirty(iocName, hostName),
                statusService.checkHasLocalCommits(iocName, hostName));
    }

    public PagedIocResponse findAll(String owner, String searchText, IocOrder orderBy, Boolean isAsc,
                                    Integer page, Integer limit) {

        List<Ioc> listResult = new ArrayList<>();

        PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit);

        String ownerString = StringUtils.isEmpty(owner) ? null : owner;
        String searchTextString = StringUtils.isEmpty(searchText) ? null : searchText;

        List<IocEntity> iocs = iocRepository.findAll(ownerString, searchTextString, orderBy, isAsc,
                pagingLimitDto.getPage(), pagingLimitDto.getLimit());

        if (iocs != null) {

            for(IocEntity i :iocs) {
                listResult.add(extractDeployment(i));
            }
        }

        long totalCount = iocRepository.countForPaging(owner, searchText);

        return new PagedIocResponse(listResult, totalCount, listResult.size(), pagingLimitDto.getPage(),
                pagingLimitDto.getLimit());
    }

    private Ioc extractDeployment(IocEntity iEnt) {

        List<Long> versionIdsForIoc = iocVersionService.findVersionIdsForIoc(iEnt.getId());

        Host actualHost = hostService.findLatestByIocId(iEnt.getId());
        IocVersionEntity latestVersion = iocVersionRepository.findLatest(iEnt.getId());

        String iocName = ConversionUtil.getIocInventoryName(latestVersion);
        String hostName = actualHost == null ? null : actualHost.getHost();

        return ConversionUtil.convertToIoc(iEnt, latestVersion,
                deploymentService.findDeploymentForIocVersions(versionIdsForIoc),
                actualHost, statusService.checkIocIsActive(iocName, hostName),
                statusService.checkRepoIsDirty(iocName, hostName),
                statusService.checkHasLocalCommits(iocName, hostName));
    }

    /**
     * It is a TRANSACTIONAL function!
     * Updates a specific IOC with the given parameters. Update will create a new IOC version!
     * Metadata file is required to be in the repo!
     * If update fails, the DB operations will be rolled back!
     *
     * @param iocId The Id of the specific IOC that has to be updated.
     * @param request The values that has to be used to update the IOC
     * @param host CSEntry Id of the host, or NULL, if host hast to be removed from the IOC
     * @param userDetails The requester user details
     *
     * @return The update, actual IOC information
     * @throws JsonProcessingException When error occurs when processing the metadata from the repository
     * @throws EntityNotFoundException If IOC is not found with the specific Id
     */
    @Transactional
    public Ioc updateIoc(long iocId, UpdateIoc request, HostEntity host, UserDetails userDetails) throws JsonProcessingException {
        String userName = userDetails.getUserName();
        IocEntity iocEnt = iocRepository.findById(iocId);

        if (request.getSourceUrl() != null) {
            authorizationService.authorizeIoc(request.getSourceUrl(), userDetails);
        }

        if (host != null) {
            hostService.linkHostToIoc(host.getId(), iocEnt, userName);
        }

        //check metadata
        if (StringUtils.isNotEmpty(request.getSourceUrl()) && StringUtils.isNotEmpty(request.getSourceVersion())) {
            IocVersionEntity iocLatestVersion = iocVersionService.findIocLatestVersionByIocId(iocId);

            String repoUrl = StringUtils.isEmpty(request.getSourceUrl()) ? iocLatestVersion.getSourceUrl() : request.getSourceUrl();
            String branchOrTag = StringUtils.isEmpty(request.getSourceVersion()) ? iocLatestVersion.getSourceVersion() : request.getSourceVersion();

            IocMetaDataEntity iocMetaDataEntity = metadataService.parseMetadata(repoUrl,
                    branchOrTag);
            IocMetaDataEntity correctMetaData = iocLatestVersion.getIocMetaData();
            boolean metadataChanged = false;

            //previously there was no meta data OR the new one differs from old => persist to DB
            if ((iocLatestVersion.getIocMetaData() == null) ||
                    (!StringUtils.equals(iocLatestVersion.getIocMetaData().getFileHash(), iocMetaDataEntity.getFileHash()))) {
                metadataService.persistMetaDataEntity(iocMetaDataEntity);

                metadataChanged = true;

                correctMetaData = iocMetaDataEntity;
            }

            createNewVersion(repoUrl, branchOrTag, correctMetaData, metadataChanged, iocLatestVersion,
                    iocLatestVersion.getNamingName(), userName);
        }

        updateIoc(request, iocEnt);

        logger.debug("IOC id[{}] UPDATED by [{}] with parameters {}",
                iocId, userName, request);

        return extractDeployment(iocEnt);
    }

    public List<Ioc> getDeployedIocsForHost(long csEntryHostId) {
        return deploymentService.findDeployedIocsByCSEntryHostId(csEntryHostId).stream()
                .map(this::extractDeployment).collect(Collectors.toList());
    }

    public List<Ioc> getConfiguredIocsForHost(long csEntryHostId) {
        return hostService.findConfiguredIocsByCSEntryHostId(csEntryHostId).stream()
                .map(this::extractDeployment).collect(Collectors.toList());
    }

    private void createNewVersion(String sourceNewUrl, String sourceNewVersion, IocMetaDataEntity metadata,
                                  boolean metadataChanged, IocVersionEntity latestVersion, String namingName, String userName) {

        if ((StringUtils.isNotEmpty(sourceNewUrl)) && (StringUtils.isNotEmpty(sourceNewVersion))) {
            //create new iocVersion
            if ((metadataChanged) || (!StringUtils.equals(sourceNewUrl, latestVersion.getSourceUrl())) ||
                    (!StringUtils.equals(sourceNewVersion, latestVersion.getSourceVersion()))) {

                iocVersionRepository.incrementVersion(latestVersion.getId(), sourceNewUrl, sourceNewVersion,
                        metadata, namingName,
                        metadataService.getEpicsVersion(metadata.getInfo()),
                        metadataService.getRequireVersion(metadata.getInfo()), userName);
            }
        }
    }

    private void updateIoc(UpdateIoc request, IocEntity iocEnt) {
        //update iocEntity
        if(StringUtils.isNotEmpty(request.getDescription())) {

            if (!StringUtils.equals(request.getDescription(), iocEnt.getDescription())) {
                iocEnt.setDescription(request.getDescription());

                iocRepository.updateIoc(iocEnt);
            }
        }
    }
}
