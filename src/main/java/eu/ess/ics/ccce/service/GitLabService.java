/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import com.google.gson.reflect.TypeToken;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.model.CommitsDto;
import eu.ess.ics.ccce.model.GitLabLoginReq;
import eu.ess.ics.ccce.model.GitLabLoginResp;
import eu.ess.ics.ccce.model.GitLabMemberResp;
import eu.ess.ics.ccce.rest.model.response.UserInfoResponse;
import okhttp3.Credentials;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.gitlab.api.AuthMethod;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.TokenType;
import org.gitlab.api.models.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class GitLabService {

    Logger logger = LoggerFactory.getLogger(GitLabService.class);

    private static final String GIT_POSTFIX = ".git";
    private static final String GIT_TOKEN_ENDPOINT = "oauth/token";

    private final HttpClientService httpClientService;

    public GitLabService(@Qualifier("baseHttpService") HttpClientService httpClientService) {
        this.httpClientService = httpClientService;
    }

    @Value("${gitlab.server.address}")
    private String gitServerAddress;
    @Value("${gitlab.client.app.id}")
    private String clientId;
    @Value("${gitlab.client.app.secret}")
    private String clientSecret;
    @Value("${gitlab.tech.user.token}")
    private String token;

    /**
     * Logs in user to GitLab, and returns token that can be used for further GitLab operations.
     * GitLab server address is in properties file
     *
     * @param userName The users username for GitLab
     * @param password The users password for GitLab
     * @return The token that can be used for further GitLab operations
     * @throws GitException Exception if error occurred during login
     */
    public String login(String userName, String password) throws GitException {

        String credential = Credentials.basic(clientId, clientSecret);
        Headers headers = Headers.of("Authorization", credential);

        GitLabLoginReq body = new GitLabLoginReq(userName, password);

        try {
            HttpClientService.ServiceResponse<GitLabLoginResp> gitLabLoginRespServiceResponse =
                    httpClientService.executePostRequest(new OkHttpClient(), headers,
                            gitServerAddress + GIT_TOKEN_ENDPOINT,
                            body, HttpClientService.JSON_MEDIA_TYPE, GitLabLoginResp.class);

            if (HttpClientService.isSuccessHttpStatusCode(gitLabLoginRespServiceResponse.getStatusCode())) {
                return gitLabLoginRespServiceResponse.getEntity().getAccessToken();
            }

            throw new GitException("Bad credentials");

        } catch (RemoteServiceException e) {
            logger.error("Error while trying to log in into GitLab", e);
            throw new GitException("Error while trying to log in into GitLab", e);
        } catch (ParseException e) {
            logger.error("Parser exception while trying to log in into GitLab", e);
            throw new GitException("Parser exception while trying to log in into GitLab", e);
        }
    }

    /**
     * Lists all Tags for a project given in the projectUrl.
     *
     * @param projectUrl The project URL. Preferred to have relative URL without the leading '/'.
     *                   If absolute URL is given the function tries to remove the server URL
     *                   (according what is set in properties).
     * @return List of tags committed for the project. Result will be empty list if no tags could be found.
     */
    public List<String> tagList(String projectUrl) {

        List<String> result = new ArrayList<>();

        //removing the leading gitlab server address because of the API
        String projectToSearch = extractProject(projectUrl);

        GitlabAPI gitlabAPI = GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

        List<GitlabTag> tags = gitlabAPI.getTags(projectToSearch);

        if (tags != null) {
            result = tags.stream()
                    .map(GitlabTag::getName)
                    .collect(Collectors.toList());
        }

        return result;
    }

    /**
     * Lists commit information for a project (will contain information from all commits).
     *
     * @param projectUrl The project URL. Preferred to have relative URL without the leading '/'.
     *                   If absolute URL is given the function tries to remove the server URL
     *                   (according what is set in properties).
     * @return Commit list for given project. Returns empty list if no commit can be found.
     * @throws GitException If an exception occurs while trying to communicate with GitLab
     */
    public List<CommitsDto> getCommits(String projectUrl) throws GitException {
        List<CommitsDto> result = new ArrayList<>();

        String projectToSearch = extractProjectUrl(projectUrl);

        GitlabAPI gitlabAPI = GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

        try {
            List<GitlabCommit> commits = gitlabAPI.getAllCommits(projectToSearch);

            if (commits != null) {
                result = commits.stream()
                        .map(c -> new CommitsDto(c.getAuthorName(), c.getCommittedDate(), c.getMessage(), c.getId()))
                        .collect(Collectors.toList());
            }

            return result;
        } catch (IOException e) {
            throw new GitException("Error while trying to get commits from GitLab", e);
        }
    }

    /**
     * Lists all Branches for a project given in the projectUrl.
     *
     * @param projectUrl The project URL. Preferred to have relative URL without the leading '/'.
     *                   If absolute URL is given the function tries to remove the server URL
     *                   (according what is set in properties).
     * @return List of branches for the project. Result will be empty list if no branch could be found.
     */
    public List<String> branchList(String projectUrl) {

        List<String> result = new ArrayList<>();
        String projectToSearch = extractProject(projectUrl);

        GitlabAPI gitlabAPI = GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

        List<GitlabBranch> branches = gitlabAPI.getBranches(projectToSearch);

        if (branches != null) {
            result = branches.stream()
                    .map(GitlabBranch::getName)
                    .collect(Collectors.toList());
        }

        return result;
    }

    @NotNull
    private String extractProject(String projectUrl) {
        //removing the leading gitlab server address because of the API
        String projectToSearch = extractProjectUrl(projectUrl);
        //remove .GIT ending from repo URL if present
        if (StringUtils.endsWithIgnoreCase(projectToSearch, GIT_POSTFIX)) {
            projectToSearch = StringUtils.removeEndIgnoreCase(projectToSearch, GIT_POSTFIX);
        }

        //remove leading slash character
        if (StringUtils.endsWithIgnoreCase(projectToSearch, "/")) {
            projectToSearch = StringUtils.removeEndIgnoreCase(projectToSearch, "/");
        }

        return projectToSearch;
    }

    /**
     * Gets all member names for the project.
     *
     * @param projectUrl The project URL. Preferred to have relative URL without the leading '/'.
     *                   If absolute URL is given the function tries to remove the server URL
     *                   (according what is set in properties).
     * @return Member names for the project. Returns empty list if no member can be found.
     * @throws GitException If an exception occurs while trying to communicate with GitLab
     */
    public List<String> getAllProjectMembers(String projectUrl) throws GitException, GitRepoNotFoundException {
        List<String> result = new ArrayList<>();

        String projectToSearch = extractProjectUrl(projectUrl);

        Headers headers = Headers.of("Authorization", "Bearer " + token);

        Type listType = new TypeToken<ArrayList<GitLabMemberResp>>() {
        }.getType();

        try {

            String encodedProjectUrl = URLEncoder.encode(projectToSearch, StandardCharsets.UTF_8.toString());
            HttpClientService.ServiceResponse<List<GitLabMemberResp>> allProjectMembers =
                    httpClientService.executeGetRequest(new OkHttpClient(), headers,
                            gitServerAddress + "api/v4/projects/" + encodedProjectUrl + "/members/all",
                            listType, "yyyy-MM-dd HH:mm.sssZ");

            if (HttpClientService.isSuccessHttpStatusCode(allProjectMembers.getStatusCode())) {

                result = allProjectMembers.getEntity().stream()
                        .map(GitLabMemberResp::getUserName)
                        .collect(Collectors.toList());

                return result;
            }

            if (allProjectMembers.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
                throw new GitRepoNotFoundException();
            }

            throw new GitException("Error while trying to fetch all members for GitLab project, status code: " +
                    allProjectMembers.getStatusCode());

        } catch (RemoteServiceException e) {
            throw new GitException("Error while trying to get project members from GitLab", e);
        } catch (UnsupportedEncodingException e) {
            throw new GitException("Encoding error while trying to get members for project", e);
        }
    }

    /**
     * Gets the content of a  specific file from GitLab.
     *
     * @param projectUrl      The project URL. Preferred to have relative URL without the leading '/'.
     *                        If absolute URL is given the function tries to remove the server URL
     *                        (according what is set in properties).
     * @param filePath        the file path without the GitLab-, and without the project URL
     * @param tagOrBranchName Name of the branch, or tag that the file should be gathered from
     * @return The content of the specific file
     * @throws GitException If an exception occurs while trying to communicate with GitLab
     */
    public String getFileContent(String projectUrl, String filePath, String tagOrBranchName) throws GitException {

        GitlabAPI gitlabAPI = GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);

        String projectToSearch = extractProject(projectUrl);

        try {

            byte[] fileContent = gitlabAPI.getRawFileContent(projectToSearch, tagOrBranchName, filePath);

            return new String(fileContent, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new MetaDataFileException("Error while trying to get file content from GitLab", e);
        }
    }

    /**
     * Removes the leading server URL from the projectURL, and gives back relative URL
     *
     * @param projectUrl The project's URL
     * @return The relative URL to the project
     */
    private String extractProjectUrl(String projectUrl) {

        String projectToSearch;
        if (projectUrl.startsWith(gitServerAddress)) {
            projectToSearch = projectUrl.replace(gitServerAddress, "");
        } else {
            projectToSearch = projectUrl;
        }

        return projectToSearch;
    }

    /**
     * Tries to extract user information from GitLab using the login token.
     *
     * @param userName login-name of the current user
     * @return User information extracted from GitLab. If no info can be extracted the fields of the object will be empty.
     * @throws GitException: If an exception occurs while trying to communicate with GitLab
     */
    public UserInfoResponse currentUserInfo(String userName) {

        UserInfoResponse result = new UserInfoResponse();

        List<UserInfoResponse> userInfoList = infoFromUserName(userName);

        if ((userInfoList != null) && (userInfoList.size() == 1)) {
            UserInfoResponse currentUser = userInfoList.get(0);

            result.setAvatar(currentUser.getAvatar());
            result.setEmail(currentUser.getEmail());
            result.setFullName(currentUser.getFullName());
            result.setLoginName(currentUser.getLoginName());
        }

        return result;
    }

    public List<UserInfoResponse> infoFromUserName(String userName) {
        List<UserInfoResponse> result = new ArrayList<>();

        GitlabAPI gitlabAPI = GitlabAPI.connect(gitServerAddress, token, TokenType.ACCESS_TOKEN, AuthMethod.HEADER);


        try {
            List<GitlabUser> users = gitlabAPI.findUsers(userName);

            if (users != null) {
                result = users.stream().map(
                        u -> {
                            UserInfoResponse tmpUsr = new UserInfoResponse();

                            tmpUsr.setAvatar(u.getAvatarUrl());
                            tmpUsr.setEmail(u.getEmail());
                            tmpUsr.setFullName(u.getName());
                            tmpUsr.setLoginName(u.getUsername());

                            return tmpUsr;
                        }).collect(Collectors.toList());
            }

            return result;
        } catch (IOException e) {
            throw new GitException("Error while trying to find user by name in GitLab", e);
        }
    }

}
