/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import eu.ess.ics.ccce.prometheus.Metric;
import eu.ess.ics.ccce.prometheus.PrometheusRepository;
import eu.ess.ics.ccce.prometheus.Result;
import eu.ess.ics.ccce.util.ConversionUtil;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class StatusService {

    private static final Double IS_OK = 1.0;

    Logger logger = LoggerFactory.getLogger(StatusService.class);

    private final PrometheusRepository prometheusRepository;

    @Autowired
    public StatusService(PrometheusRepository prometheusRepository) {
        this.prometheusRepository = prometheusRepository;
    }

    /**
     *  Gets status information about specific IOC running on a specific host from Prometheus.
     *  Requests are cached for a small amount of time (for specific host) in order to reduce network traffic!
     * (Checks for .service along with state=active flag in Prometheus)
     *
     * @param iocName the name of the ioc for which the status has to be determined
     * @param hostName the name of the host on which the ioc may run, for which the status has to be determined
     * @return <code>true</code>, if ioc is available
     *          <code>false</code>, if ioc is not available
     *          <code>null</code> if there was a problem during the Prometheus query
     */
    public Boolean checkIocIsActive(String iocName, String hostName) {

        if (!StringUtils.hasText(hostName)) {
            return checkIOCIsActive(prometheusRepository.checkIocIsActive(iocName));
        }

        List<Result> iocList = prometheusRepository.listActiveIocs();

        boolean found = false;

        for (Result r : iocList) {
            Metric metric = r.getMetric();

            if ((metric.getInstance().equals(hostName))
                    && (metric.getName().equals(
                    "ioc@" + ConversionUtil.transformIocName(iocName) + ".service"))) {
                found = true;
                if (isValid(r.getValue())) {
                    return true;
                }
            }
        }

        if (found) {
            return false;
        }

        return null;
    }

    /**
     * Counts all active IOCs that is gathered from Prometheus.
     *
     * @return number of active IOCs according to Prometheus
     */
    public int countAllActiveIocs(){
        List<Result> results = prometheusRepository.listActiveIocs();

        return results.size();
    }

    private boolean isValid(List<Double> value) {
        for (Double v : value) {
            if (IS_OK.equals(v)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if IOC repository is dirty on a specific host.
     *
     * @param iocName the name of the IOC that has to be checked
     * @param hostName the name of the host where the IOC is running
     *
     * @return <code>true</code> if IOC is dirty according to Prometheus.
     *         <code>false</code> if IOC is not dirty according to Prometheus.
     *         <code>null</code> if no entry found in Prometheus for the IOC on the certain host.
     */
    public Boolean checkRepoIsDirty(String iocName, String hostName) {

        if ((org.apache.commons.lang3.StringUtils.isEmpty(iocName))
                || (org.apache.commons.lang3.StringUtils.isEmpty(hostName))) {

            return null;
        }

        List<Result> iocList = prometheusRepository.allDirtyIoc();

        boolean found = false;

        for (Result r : iocList) {
            Metric metric = r.getMetric();

            if ((metric.getInstance().equals(hostName))
                    && (metric.getIoc().equals(iocName))) {

                found = true;
                if (isValid(r.getValue())) {
                    return true;
                }
            }
        }

        if (found) {
            return false;
        }

        return null;
    }



    /**
     * Checks if an IOC has local commits on a certain host.
     *
     * @param iocName the name of the IOC that has to be checked.
     * @param hostName the name of the host where the IOC runs
     *
     * @return <code>true</code>, if the IOC has local commits on the host.
     *         <code>false</code> if the IOC doesn't have local commits on the host.
     *         <code>null</code> if IOC on the host can't be found in the Prometheus response;
     *         or host or IOC name is empty.
     */
    public Boolean checkHasLocalCommits(String iocName, String hostName) {

        if ((org.apache.commons.lang3.StringUtils.isEmpty(iocName))
                || (org.apache.commons.lang3.StringUtils.isEmpty(hostName))) {

            return null;
        }

        List<Result> iocList = prometheusRepository.allLocalCommits();

        boolean found = false;

        for (Result r : iocList) {
            Metric metric = r.getMetric();

            if ((metric.getInstance().equals(hostName))
                    && (metric.getIoc().equals(iocName))) {

                found = true;
                if (isValid(r.getValue())) {
                    return true;
                }
            }
        }

        if (found) {
            return false;
        }

        return null;
    }

    /**
     * Gets status information about specific host from Prometheus.
     * Requests are cached for a small amount of time (for specific host) in order to reduce network traffic!
     * (Checks for job=node in Prometheus)
     *
     * @param hostName the name of the host for which the status has to be determined
     * @return <code>true</code>, if host is available
     *         <code>false</code>, if host is not available
     *         <code>null</code> if there was a problem during the Prometheus query
     */
    public Boolean checkHostIsActive(String hostName) {

        List<Result> hostList = prometheusRepository.listAllHost();

        boolean found = false;

        for (Result r : hostList) {
            Metric metric = r.getMetric();

            if (metric.getInstance().equals(hostName)) {

                found = true;
                if (isValid(r.getValue())) {
                    return true;
                }
            }
        }

        if (found) {
            return false;
        }

        return null;
    }

    public List<Result> listActiveIocHosts() {
        logger.debug("Trying to fetch all active IOC hosts from Prometheus");

        List<Result> activeHosts = prometheusRepository.listActiveIocHosts();

        return filterTrueResponses(activeHosts);
    }

    public List<String> listIocHosts() {
        logger.debug("Trying to fetch all IOC hosts from Prometheus");

        return prometheusRepository.listIocHosts();
    }

    private List<Result> filterTrueResponses(List<Result> response) {
        List<Result> result = null;

        if (response != null) {
            result = new ArrayList<>();

            for (Result r : response) {
                for (Double value : r.getValue()) {
                    if (IS_OK.equals(value)) {
                        result.add(r);
                    }
                }
            }
        }

        return result;
    }

    private Boolean checkIOCIsActive(List<Result> response) {
        Boolean result = null;

        for (Result r : response) {
            if (r.getValue() != null) {
                for (Double value : r.getValue()) {
                    if (IS_OK.equals(value)) {
                        return true;
                    }
                }
                result = false;
            }
        }

        return result;
    }
}
