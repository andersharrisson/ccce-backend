/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.service;

import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.repo.IIocRepository;
import eu.ess.ics.ccce.repo.IIocVersionRepository;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.repo.model.IocMetaDataEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import eu.ess.ics.ccce.rest.model.BaseIocVersion;
import eu.ess.ics.ccce.rest.model.IocVersion;
import eu.ess.ics.ccce.rest.model.response.PagedIocVersionResponse;
import eu.ess.ics.ccce.util.ConversionUtil;
import eu.ess.ics.ccce.util.PagingLimitDto;
import eu.ess.ics.ccce.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class IocVersionService {

    private final IIocRepository iocRepository;
    private final IIocVersionRepository iocVersionRepository;
    private final Utils utils;
    private final MetadataService metadataService;

    @Autowired
    public IocVersionService(IIocRepository iocRepository, IIocVersionRepository iocVersionRepository,
                             Utils utils, MetadataService metadataService) {
        this.iocRepository = iocRepository;
        this.iocVersionRepository = iocVersionRepository;
        this.utils = utils;
        this.metadataService = metadataService;
    }

    public IocVersionEntity createIocVersion(long iocId, BaseIocVersion request, IocMetaDataEntity metaData,
                                             String namingName, String namingUuid, String createdBy) {

        IocEntity ioc = iocRepository.findById(iocId);
        IocVersionEntity iocVersionEntity = new IocVersionEntity();
        iocVersionEntity.setIoc(ioc);
        iocVersionEntity.setSourceVersion(request.getSourceVersion());
        iocVersionEntity.setSourceUrl(request.getSourceUrl());
        iocVersionEntity.setCreatedBy(createdBy);
        iocVersionEntity.setVersion(1L);
        iocVersionEntity.setNamingName(namingName);
        iocVersionEntity.setNamingUuid(namingUuid);

        if (metaData != null) {
            iocVersionEntity.setIocMetaData(metaData);
            iocVersionEntity.setRequireVersion(metadataService.getRequireVersion(metaData.getInfo()));
            iocVersionEntity.setEpicsVersion(metadataService.getEpicsVersion(metaData.getInfo()));
        }

        iocVersionRepository.createIocVersion(iocVersionEntity);

        return iocVersionEntity;
    }

    public PagedIocVersionResponse findAllVersions(long iocId, Integer page, Integer limit) {

        iocRepository.findById(iocId);
        PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit);
        List<IocVersionEntity> iocVersions = iocVersionRepository.findAllByIocId(iocId, pagingLimitDto.getPage(), pagingLimitDto.getLimit());

        List<IocVersion> versionDto = new ArrayList<>();

        if(iocVersions != null) {
            versionDto = iocVersions.stream()
                    .map(v -> ConversionUtil.convertToIocVersion(v))
                    .collect(Collectors.toList());
        }

        long totalCount = iocVersionRepository.countForPaging(iocId);
        return new PagedIocVersionResponse(totalCount, versionDto.size(), pagingLimitDto.getPage(),
                pagingLimitDto.getLimit(), versionDto);
    }

    public List<Long> findVersionIdsForIoc(long iocId) {
        return iocVersionRepository.findVersionIdsForIoc(iocId);
    }

    public IocVersion findVersionByIds(long iocId, long versionNumber) {

        IocVersionEntity versionEnt = iocVersionRepository.findByVersionId(iocId, versionNumber);
        return ConversionUtil.convertToIocVersion(versionEnt);
    }

    public IocVersionEntity findVersionEntityByIds(long iocId, long versionNumber) {
        return iocVersionRepository.findByVersionId(iocId, versionNumber);
    }

    public IocVersionEntity findIocLatestVersionByIocId(long id) {
        try {
            return iocVersionRepository.findLatest(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("Latest IOC Version", id);
        }
    }
}
