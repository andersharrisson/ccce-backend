package eu.ess.ics.ccce.service;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.awx.model.Command;
import eu.ess.ics.ccce.awx.model.Credential;
import eu.ess.ics.ccce.awx.model.Inventory;
import eu.ess.ics.ccce.awx.model.Job;
import eu.ess.ics.ccce.awx.model.JobDetails;
import eu.ess.ics.ccce.awx.model.JobStatus;
import eu.ess.ics.ccce.awx.model.JobTemplate;
import eu.ess.ics.ccce.awx.service.AwxService;
import eu.ess.ics.ccce.exceptions.AwxException;
import eu.ess.ics.ccce.exceptions.ConcurrentDeploymentFoundException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.model.DeploymentDto;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.repo.IDeploymentRepository;
import eu.ess.ics.ccce.repo.IHostRepository;
import eu.ess.ics.ccce.repo.model.AwxJobEntity;
import eu.ess.ics.ccce.repo.model.DeploymentEntity;
import eu.ess.ics.ccce.repo.model.DeploymentJobMappingEntity;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.HostMappingEntity;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import eu.ess.ics.ccce.rest.model.*;
import eu.ess.ics.ccce.rest.model.request.DeploymentOrder;
import eu.ess.ics.ccce.rest.model.response.PagedDeploymentResponse;
import eu.ess.ics.ccce.service.deployment.DeploymentTask;
import eu.ess.ics.ccce.service.deployment.InventoryCalculation;
import eu.ess.ics.ccce.util.ConversionUtil;
import eu.ess.ics.ccce.util.PagingLimitDto;
import eu.ess.ics.ccce.util.Util;
import eu.ess.ics.ccce.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Service
public class DeploymentService {
    Logger logger = LoggerFactory.getLogger(DeploymentService.class);

    private static final String AWX_ARG_SEPARATOR = " ";
    private static final String HOST_SEPARATOR = ", ";
    private final IDeploymentRepository deploymentRepository;
    private final IHostRepository hostRepository;
    private final IocVersionService iocVersionService;
    private final Utils utils;
    private final HostService hostService;
    private final AwxService awxService;
    private final AuthorizationService authorizationService;

    @Autowired
    public DeploymentService(IDeploymentRepository deploymentRepository, IHostRepository hostRepository,
                             IocVersionService iocVersionService, Utils utils, HostService hostService,
                             AwxService awxService, AuthorizationService authorizationService) {
        this.deploymentRepository = deploymentRepository;
        this.hostRepository = hostRepository;
        this.iocVersionService = iocVersionService;
        this.utils = utils;
        this.hostService = hostService;
        this.awxService = awxService;
        this.authorizationService = authorizationService;
    }

    public List<Deployment> createDeployment(DeploymentTask deploymentTask) throws Exception {
        List<IocVersionEntity> iocVersionsToDeploy = deploymentTask.getIocIds().stream().map(
                iocVersionService::findIocLatestVersionByIocId).collect(Collectors.toList());

        authorizationService.authorizeIocs(iocVersionsToDeploy.stream().map(IocVersionEntity::getSourceUrl)
                .distinct().collect(Collectors.toList()), deploymentTask.getUserDetails());

        List<DeploymentEntity> concurrentDeploymentsByIoc = deploymentRepository.findIntendedOrQueuedDeploymentForIocVersions(iocVersionsToDeploy);

        if (!concurrentDeploymentsByIoc.isEmpty())  {
            List<String> iocs = concurrentDeploymentsByIoc.stream().map(de -> de.getIocVersion().getNamingName()
                    + " (ID: " + de.getIocVersion().getIoc().getId() + ")").collect(Collectors.toList());
            throw new ConcurrentDeploymentFoundException("Concurrent deployments for IOC(s): " + StringUtils.join(iocs, ", "));
        }

        List<IocVersionEntity> ongoingDeployments = deploymentRepository.findAllIntendedOrQueuedDeployments().stream()
                .map(DeploymentEntity::getIocVersion).collect(Collectors.toList());
        List<Long> ongoingAffectedHostIds = findDeploymentHostsForIocLatestVersions(ongoingDeployments).stream()
                .map(HostEntity::getId).collect(Collectors.toList());

        List<HostEntity> hostsForLimit = findDeploymentHostsForIocLatestVersions(iocVersionsToDeploy);
        Set<Long> concurrentHostIds = hostsForLimit.stream().map(HostEntity::getId)
                .distinct()
                .filter(ongoingAffectedHostIds::contains)
                .collect(Collectors.toSet());

        String limit = calculateLimit(hostsForLimit);
        String iocsToDeploy = calculateIocsToDeploy(iocVersionsToDeploy.stream().map(IocVersionEntity::getNamingName).collect(Collectors.toList()));

        if (!concurrentHostIds.isEmpty()) {
            List<DeploymentEntity> deployments = createDeployments(iocVersionsToDeploy, deploymentTask.getComment(),
                    deploymentTask.getUserDetails().getUserName(), true, deploymentTask.isUndeploy(), limit, iocsToDeploy);

            return deployments.stream().map(deploymentEntity -> ConversionUtil.convertToDeployment(
                    deploymentEntity, getHostStatus(deploymentEntity), null)).collect(Collectors.toList());
        }

        List<DeploymentEntity> deployments = createDeployments(iocVersionsToDeploy, deploymentTask.getComment(),
                deploymentTask.getUserDetails().getUserName(), false, deploymentTask.isUndeploy(), limit, iocsToDeploy);

        AwxJobEntity jobEntity = startDeployment(deployments, deploymentTask.isUndeploy(),
                deploymentTask.getUserDetails().getUserName(), deploymentTask.getComment(), limit, iocsToDeploy);

        return deployments.stream().map(deploymentEntity -> ConversionUtil.convertToDeployment(
                deploymentEntity, getHostStatus(deploymentEntity), jobEntity)).collect(Collectors.toList());
    }

    @Transactional
    public void executeNextQueuedDeployment() {
        List<DeploymentEntity> nextQueuedDeployments = deploymentQueueNext();
        List<DeploymentEntity> concurrentDeployments = deploymentRepository
                .findConcurrentIntendedDeploymentsForDeployments(nextQueuedDeployments);

        if (!nextQueuedDeployments.isEmpty() && concurrentDeployments.isEmpty()) {

            List<IocVersionEntity> ongoingDeployments = deploymentRepository.findAllIntendedDeployments().stream()
                    .map(DeploymentEntity::getIocVersion).collect(Collectors.toList());
            List<String> ongoingAffectedHosts = findDeploymentHostsForIocLatestVersions(ongoingDeployments).stream()
                    .map(HostEntity::getHost).filter(Objects::nonNull).collect(Collectors.toList());

            DeploymentEntity first = nextQueuedDeployments.get(0);

            Set<String> concurrentHosts = Arrays.stream(StringUtils.split(first.getLimit(), HOST_SEPARATOR))
                    .distinct()
                    .filter(ongoingAffectedHosts::contains)
                    .collect(Collectors.toSet());
            if (concurrentHosts.isEmpty()) {
                try {
                    startDeployment(nextQueuedDeployments, first.isUndeploy(), first.getCreatedBy(), first.getDescription(),
                            first.getLimit(), first.getIocsToDeploy());
                    updateQueuedToPendingDeployments(nextQueuedDeployments);
                } catch (Exception e) {
                    logger.error("Error during executing queued deployment task: " + e.getMessage(), e);
                }
            }
        }
    }

    private List<DeploymentEntity> deploymentQueueNext() {
        try {
            DeploymentEntity firstQueued = deploymentRepository.findFirstQueued();
            if (firstQueued != null) {
                if (firstQueued.getTaskId() != null) {
                    return deploymentRepository.findQueuedForTaskId(firstQueued.getTaskId());
                }
                return ImmutableList.of(firstQueued);
            }
        } catch (EmptyResultDataAccessException e) {
            logger.info("No queued deployment in database");
        }
        return Collections.emptyList();
    }

    public AwxJobEntity startDeployment(List<DeploymentEntity> deployments, boolean isUndeploy, String userName,
                                        String comment, String limit, String iocsToDeploy) throws Exception {
        if (!deployments.isEmpty()) {
            Long deploymentId = deployments.get(0).getId();
            if (deploymentId != null) {
                AwxJobEntity job = findJobForDeployment(deployments.get(0).getId());
                if (job != null) {
                    logger.warn("AWX job already started for deployment (" + deployments.get(0).getId() + "), AWX job ID: "
                            + job.getId());
                    return job;
                }
            }
        }

        Job job = null;
        Exception awxError = null;
        try {
            JobTemplate ccceDummyPlaybook = awxService.getJobTemplate(awxService.getJobTemplateName());
            // Launch the job
            job = awxService.launchJob(ccceDummyPlaybook.getRelated().getLaunch(), limit,
                    isUndeploy ? null : iocsToDeploy);
        } catch (Exception e) {
            awxError = e;
            logger.error(e.getMessage(), e);
        }

        if (job != null) {
            AwxJobEntity jobEntity = createAwxJob(job, deployments);

            logger.debug("IOCs have been DEPLOYED by [{}] with comment [{}], IOCs to deploy: {} to hosts: {}",
                    userName, comment,
                    iocsToDeploy, limit);
            return jobEntity;
        } else {
            setDeploymentsFailed(deployments);
            if (awxError != null) {
                throw awxError;
            }
            throw new ServiceException("Error during creating deployment");
        }
    }

    @Transactional
    public List<Deployment> createUndeployment(DeploymentTask deploymentTask) throws Exception {
        setIocsToUndeployment(deploymentTask.getIocIds(), deploymentTask.getUserDetails().getUserName());
        return createDeployment(deploymentTask);
    }

    private String calculateLimit(List<HostEntity> hostsForLimit) {
        List<String> hosts = hostsForLimit.stream().map(HostEntity::getHost).filter(Objects::nonNull).collect(Collectors.toList());
        return StringUtils.join(hosts, HOST_SEPARATOR);
    }

    private String calculateIocsToDeploy(final List<String> iocs) {
        return StringUtils.join(iocs, ", ");
    }

    private void setIocsToUndeployment(List<Long> iocIds, String user) {
        List<IocEntity> iocsToUndeploy = iocIds.stream().map(
                iocVersionService::findIocLatestVersionByIocId).map(IocVersionEntity::getIoc).collect(Collectors.toList());
        for (IocEntity ioc : iocsToUndeploy) {
            hostService.createEmptyHostMapping(ioc, user);
        }
    }

    @Transactional
    public List<DeploymentEntity> createDeployments(List<IocVersionEntity> iocVersionEntities, String comment,
                                                    String createdBy, boolean queued, boolean undeploy, String limit,
                                                    String iocsToDeploy) {
        List<DeploymentEntity> deployments = new ArrayList<>();
        Date now = new Date();
        UUID uuid = null;
        if (iocVersionEntities.size() > 1) {
            uuid = UUID.randomUUID();
        }
        for (IocVersionEntity iocVersion : iocVersionEntities) {
            long iocId = iocVersion.getIoc().getId();
            try {
                HostMappingEntity hostMappingEntity = hostRepository.findLatestMappingByIocId(iocId);

                DeploymentEntity deploymentEntity = new DeploymentEntity();
                deploymentEntity.setIocVersion(iocVersion);
                deploymentEntity.setDescription(comment);
                deploymentEntity.setCreatedAt(now);
                deploymentEntity.setTaskId(uuid);
                deploymentEntity.setUndeploy(undeploy);
                deploymentEntity.setLimit(limit);
                deploymentEntity.setIocsToDeploy(iocsToDeploy);
                deploymentEntity.setCreatedBy(createdBy);
                deploymentEntity.setDeploymentState(
                        queued
                        ? DeploymentStatus.QUEUED.name()
                        : DeploymentStatus.PENDING.name());
                deploymentEntity.setActive(false);
                deploymentEntity.setIntended(!queued);
                deploymentEntity.setHostMapping(hostMappingEntity);
                deploymentRepository.createDeployment(deploymentEntity);

                deployments.add(deploymentEntity);
            } catch (EmptyResultDataAccessException e) {
                throw new EntityNotFoundException("Latest host mapping not found for IOC (ID: " + iocId + ")");
            }
        }
        return deployments;
    }

    @Transactional
    public void updateQueuedToPendingDeployments(List<DeploymentEntity> deployments) {
        Date now = new Date();
        for (DeploymentEntity deploymentEntity : deployments) {
            deploymentEntity.setCreatedAt(now);
            deploymentEntity.setDeploymentState(DeploymentStatus.PENDING.name());
            deploymentEntity.setActive(false);
            deploymentEntity.setIntended(true);
            deploymentRepository.updateDeployment(deploymentEntity);
        }
    }

    @Transactional
    public AwxJobEntity createAwxJob(Job job, List<DeploymentEntity> deployments) {
        AwxJobEntity awxJob = new AwxJobEntity();
        awxJob.setAwxJobId(job.getId());
        awxJob.setAwxJobUrl(job.getUrl());
        awxJob.setStartAt(job.getStarted());
        awxJob.setStatus(job.getStatus().name());
        deploymentRepository.createAwxJob(awxJob);

        for (DeploymentEntity deployment : deployments) {
            DeploymentJobMappingEntity deploymentJobMapping = new DeploymentJobMappingEntity();
            deploymentJobMapping.setAwxJob(awxJob);
            deploymentJobMapping.setDeployment(deployment);
            deploymentRepository.createDeploymentJobMapping(deploymentJobMapping);
        }
        return awxJob;
    }

    public JobDetails fetchDeploymentJobDetails(long awxJobId) {
        try {
            JobDetails jobDetails = awxService.getJobDetails(awxJobId);
            updateAwxJob(jobDetails);
            return jobDetails;
        } catch (AwxException e) {
            throw new ServiceException(e);
        }
    }

    public void updateAwxJob(AwxJobMeta job) {
        if (JobStatus.SUCCESSFUL.equals(job.getStatus()) || JobStatus.FAILED.equals(job.getStatus())) {
            fetchDeploymentJobDetails(job.getId());
        } else {
            updateAwxJobAndDeployment(job.getId(), job.getStatus(), job.getStarted(), job.getFinished(), null);
        }
    }

    private void updateAwxJob(JobDetails job) {
        updateAwxJobAndDeployment(job.getJob().getId(), job.getJob().getStatus(), job.getJob().getStarted(),
                job.getJob().getFinished(), job.getIocDeploymentResults());
    }

    public List<DeploymentDto> findAllActiveOrIntended() {
        List<DeploymentEntity> deploymentEntities = deploymentRepository.findAllActiveOrIntended();
        return deploymentEntities.stream().map(ConversionUtil::convert).collect(Collectors.toList());
    }

    @Transactional
    void updateAwxJobAndDeployment(long awxJobId, JobStatus status, Date started, Date finished, Map<String, Map<String, Object>> iocDeploymentResult) {
        try {
            AwxJobEntity awxJobEntity = deploymentRepository.findByAwxJobId(awxJobId);
            if (awxJobEntity != null
                    && !(awxJobEntity.getStatus().equals(JobStatus.SUCCESSFUL.name())
                    || awxJobEntity.getStatus().equals(JobStatus.FAILED.name()))) {
                awxJobEntity.setStatus(status.name());
                awxJobEntity.setStartAt(started);
                awxJobEntity.setFinishedAt(finished);
                deploymentRepository.updateAwxJob(awxJobEntity);

                List<DeploymentJobMappingEntity> mappings = deploymentRepository.findDeploymentMappingForAwxJobId(awxJobEntity.getId());
                for (DeploymentJobMappingEntity mapping : mappings) {
                    DeploymentEntity deployment = mapping.getDeployment();
                    if (JobStatus.RUNNING.equals(status)) {
                        deployment.setStartTime(started);
                        deployment.setDeploymentState(DeploymentStatus.RUNNING.name());
                    } else if (JobStatus.SUCCESSFUL.equals(status) || JobStatus.FAILED.equals(status)) {
                        Map<String, Object> result = iocDeploymentResult.get(ConversionUtil.getIocInventoryName(deployment.getIocVersion()));
                        if (result != null && Boolean.parseBoolean(result.get("successful").toString())) {
                            deployment.setStartTime(started);
                            deployment.setFinishedAt(finished);
                            deployment.setDeploymentState(DeploymentStatus.SUCCESSFUL.name());

                            List<Long> versionIdsForIoc = iocVersionService.findVersionIdsForIoc(deployment.getIocVersion().getIoc().getId());
                            try {
                                DeploymentEntity activeDeployment = deploymentRepository.findActiveDeploymentForIocVersions(versionIdsForIoc);
                                activeDeployment.setActive(false);
                                deploymentRepository.updateDeployment(activeDeployment);
                            } catch (EmptyResultDataAccessException e) {
                                logger.warn("No active deployment found for IOC (ID: " + deployment.getIocVersion().getIoc().getId() +
                                        "). Ignore this warning if this the very first deployment for the IOC.", e);
                            }

                            if (deployment.getHostMapping().getHost() != null) {
                                deployment.setActive(true);
                            }
                            deployment.setIntended(false);
                        } else {
                            deployment.setStartTime(started);
                            deployment.setFinishedAt(finished);
                            deployment.setDeploymentState(DeploymentStatus.FAILED.name());
                            deployment.setIntended(false);
                        }
                    } else if (JobStatus.CANCELED.equals(status)) {
                        deployment.setStartTime(started);
                        deployment.setFinishedAt(finished);
                        deployment.setDeploymentState(DeploymentStatus.FAILED.name());
                        deployment.setIntended(false);
                    }
                    deploymentRepository.updateDeployment(deployment);
                }

                if (JobStatus.SUCCESSFUL.equals(status)
                        || JobStatus.FAILED.equals(status)
                        || JobStatus.CANCELED.equals(status)) {
                        executeNextQueuedDeployment();
                }
            }
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("AWX job", awxJobId);
        }
    }

    @Transactional
    public void setDeploymentsFailed(List<DeploymentEntity> deployments){
        for (DeploymentEntity deploymentEntity : deployments) {
            deploymentEntity.setIntended(false);
            deploymentEntity.setDeploymentState(DeploymentStatus.FAILED.name());

            deploymentRepository.updateDeployment(deploymentEntity);
        }
        executeNextQueuedDeployment();
    }

    public Deployment findDeploymentForIocVersions(List<Long> iocVersionIds) {
        DeploymentEntity activeDeployment = null;
        try {
            activeDeployment = deploymentRepository.findActiveDeploymentForIocVersions(iocVersionIds);
        } catch (EmptyResultDataAccessException e) {
        }

        AwxJobEntity job = activeDeployment == null ? new AwxJobEntity() : findJobForDeployment(activeDeployment.getId());

        if (job == null) {
            job = new AwxJobEntity();
        }

        return ConversionUtil.convertToDeployment(activeDeployment, getHostStatus(activeDeployment), job);
    }

    public List<HostEntity> findDeploymentHostsForIocLatestVersions(List<IocVersionEntity> iocLatestVersions) {
        List<HostEntity> activeDeploymentHosts = Collections.emptyList();
        List<HostEntity> intendedDeploymentHosts = Collections.emptyList();
        try {
            activeDeploymentHosts = iocLatestVersions.stream().map(
                    iocVersion -> deploymentRepository.findActiveDeploymentForIocVersions(ImmutableList.of(iocVersion.getId())).getHostMapping().getHost())
                    .collect(Collectors.toList());
        } catch (EmptyResultDataAccessException e) {

        }

        try {
            List<HostMappingEntity> intendedDeploymentHostMappings = iocLatestVersions.stream().map(
                    iocVersion -> hostRepository.findLatestMappingByIocId(iocVersion.getIoc().getId()))
                    .collect(Collectors.toList());
            intendedDeploymentHosts = intendedDeploymentHostMappings.stream().
                    map(HostMappingEntity::getHost).filter(Objects::nonNull).collect(Collectors.toList());
        } catch (EmptyResultDataAccessException e) {

        }

        List<HostEntity> allHosts = new ArrayList<>();
        allHosts.addAll(activeDeploymentHosts);
        allHosts.addAll(intendedDeploymentHosts);

        return allHosts.stream().filter(Util.distinctByKey(HostEntity::getId))
                .collect(Collectors.toList());
    }

    public Deployment findDeploymentById(long id) {
        DeploymentEntity deployment = deploymentRepository.findDeploymentById(id);
        return extractDeployment(deployment);
    }

    public AwxJobEntity findJobForDeployment(long deploymentId) {
        AwxJobEntity result = null;
        try {
            result = deploymentRepository.findJobForDeployment(deploymentId);
        } catch (EmptyResultDataAccessException e) {
        }

        return result;
    }

    public PagedDeploymentResponse findAll(DeploymentStatus status, Long iocId, Long hostCSEntryId, String user, String searchText, DeploymentOrder orderBy, Boolean isAsc, Integer page, Integer limit) {

        List<Deployment> listResult = new ArrayList<>();

        PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit);

        String statusString = status == null ? null : status.name();
        String userString = StringUtils.isEmpty(user) ? null : user;
        String searchTextString = StringUtils.isEmpty(searchText) ? null : searchText;

        List<DeploymentEntity> deployments = deploymentRepository.findAllDeployments(statusString, iocId, hostCSEntryId,
                userString, searchTextString, orderBy, isAsc,
                pagingLimitDto.getPage(), pagingLimitDto.getLimit());

        if (deployments != null) {

            for (DeploymentEntity deployment : deployments) {
                listResult.add(extractDeployment(deployment));
            }
        }

        long totalCount = deploymentRepository.countForPaging(statusString, iocId, hostCSEntryId, userString, searchText);
        return new PagedDeploymentResponse(totalCount, listResult.size(), pagingLimitDto.getPage(),
                pagingLimitDto.getLimit(), listResult);
    }

    private Deployment extractDeployment(DeploymentEntity deploymentEntity) {
        AwxJobEntity job = findJobForDeployment(deploymentEntity.getId());

        return ConversionUtil.convertToDeployment(deploymentEntity, getHostStatus(deploymentEntity), job);
    }

    public HostStatus getHostStatus(DeploymentEntity deployment) {
        HostStatus hostStatus = null;

        if ((deployment != null) && (deployment.getHostMapping() != null)) {
            hostStatus = hostService.getHostStatus(deployment.getHostMapping().getHost() != null
                    ? deployment.getHostMapping().getHost().getHost() : null);
        }

        return hostStatus;
    }

    public Map<String, InventoryHost> getInventory() {
        InventoryCalculation inventoryCalculation = new InventoryCalculation(findAllActiveOrIntended());
        inventoryCalculation.createInventory();
        return inventoryCalculation.getInventory();
    }

    public List<IocEntity> findDeployedIocsByCSEntryHostId(long csEntryHostId) {
        return deploymentRepository.findAllActiveByCSEntryHostId(csEntryHostId).stream()
                .map(d -> d.getIocVersion().getIoc()).collect(Collectors.toList());
    }

    public CommandResponse startIoc(Ioc ioc, UserDetails userDetails) throws AwxException {
        String args = StringUtils.join(ImmutableList.of(getIocNameParam(ioc), "state=started"), AWX_ARG_SEPARATOR);
        CommandResponse result =  executeCommand(ioc, args, userDetails);

        logger.debug("IOC [{}] has been STARTED on host [{}] by [{}]", ioc.getActiveDeployment().getIocName(),
                ioc.getActiveDeployment().getHost().getHost(), userDetails.getUserName());

        return result;
    }

    public CommandResponse stopIoc(Ioc ioc, UserDetails userDetails) throws AwxException {
        String args = StringUtils.join(ImmutableList.of(getIocNameParam(ioc), "state=stopped"), AWX_ARG_SEPARATOR);
        CommandResponse result =  executeCommand(ioc, args, userDetails);

        logger.debug("IOC [{}] has been STOPPED on host [{}] by [{}]", ioc.getActiveDeployment().getIocName(),
                ioc.getActiveDeployment().getHost().getHost(), userDetails.getUserName());

        return result;
    }

    public CommandResponse restartIoc(Ioc ioc, UserDetails userDetails) throws AwxException {
        String args = StringUtils.join(ImmutableList.of(getIocNameParam(ioc), "state=restarted"), AWX_ARG_SEPARATOR);
        CommandResponse result =  executeCommand(ioc, args, userDetails);

        logger.debug("IOC [{}] has been RESTARTED on host [{}] by [{}]", ioc.getActiveDeployment().getIocName(),
                ioc.getActiveDeployment().getHost().getHost(), userDetails.getUserName());

        return result;
    }

    private String getIocNameParam(Ioc ioc) {
        IocVersionEntity iocVersionEntity = iocVersionService.findVersionEntityByIds(ioc.getId(), ioc.getActiveDeployment().getVersion().getVersion());
        return "name=ioc@" + ConversionUtil.getIocInventoryName(iocVersionEntity);
    }

    private CommandResponse executeCommand(Ioc ioc, String args, UserDetails userDetails) throws AwxException {
        String sourceUrl = ioc.getActiveDeployment().getVersion().getSourceUrl();
        Host host = ioc.getActiveDeployment().getHost();
        authorizationService.authorizeIoc(sourceUrl, userDetails);
        Inventory inventory = awxService.getInventory("ccce-inventory-script");
        Credential credential = awxService.getCredential("Demo Credential");
        String module = "service";
        String extra = "";
        Command command = awxService.launchAdHocCommand(inventory, host.getHost(), credential, module, args, extra);
        if (command != null) {
            return new CommandResponse(command.getId(), command.getUrl());
        }
        throw new ServiceException("Error during starting IOC");
    }
}
