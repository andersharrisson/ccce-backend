package eu.ess.ics.ccce.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.ess.ics.ccce.exceptions.ParseException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Service
@Qualifier("baseHttpService")
public class HttpClientService {
    public static class ServiceResponse<T> {
        private final T entity;
        private final int statusCode;
        private final String errorMessage;

        public ServiceResponse(T entity, int statusCode) {
            this.entity = entity;
            this.statusCode = statusCode;
            this.errorMessage = null;
        }

        public ServiceResponse(int statusCode, String errorMessage) {
            this.entity = null;
            this.statusCode = statusCode;
            this.errorMessage = errorMessage;
        }

        public T getEntity() {
            return entity;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public String getErrorMessage() {
            return errorMessage;
        }
    }

    public static final MediaType JSON_MEDIA_TYPE
            = MediaType.get("application/json; charset=utf-8");

    public static final MediaType XML_MEDIA_TYPE
            = MediaType.get("application/xml; charset=utf-8");

    public <T> ServiceResponse<T> executeGetRequest(OkHttpClient client, Headers headers, String url, Class<T> responseClass) throws RemoteServiceException {
        return executeGetRequest(client, headers, url, responseClass, null);
    }

    public <T> ServiceResponse<T> executeGetRequest(@NonNull OkHttpClient client, Headers headers, String url, Class<T> responseClass, String formatDate) throws RemoteServiceException {
        Request request;
        request = new Request.Builder()
                .headers(headers)
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                Gson gson = new Gson();
                if (formatDate != null) {
                    gson = new GsonBuilder().setDateFormat(formatDate).create();
                }
                return new ServiceResponse<>(gson.fromJson(response.body().string(), responseClass), response.code());
            }
            return new ServiceResponse<>(null, response.code());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service", e);
        }
    }

    public <T> ServiceResponse<T> executeGetRequest(@NonNull OkHttpClient client, Headers headers, String url, Type type, String formatDate) throws RemoteServiceException {
        Request request;
        request = new Request.Builder()
                .headers(headers)
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(new GsonBuilder().setDateFormat(formatDate).create().fromJson(response.body().string(), type), response.code());
            }
            return new ServiceResponse<>(null, response.code());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service", e);
        }
    }

    public <T> ServiceResponse<T> executeGetRequest(@NonNull OkHttpClient client, Headers headers, String url, Type type) throws RemoteServiceException {
        Request request;
        request = new Request.Builder()
                .headers(headers)
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(new GsonBuilder().create().fromJson(response.body().string(), type), response.code());
            }
            return new ServiceResponse<>(null, response.code());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service", e);
        }
    }

    public <T> ServiceResponse<T> executeGetRequest(@NonNull OkHttpClient client, Headers headers, String url,
                                                    MediaType mediaType, Class<T> responseClass)
            throws RemoteServiceException, ParseException {

        if (JSON_MEDIA_TYPE.equals(mediaType)) {
            return executeGetRequest(client, headers, url, responseClass);
        }

        Request request;

        request = new Request.Builder()
                .headers(headers)
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(new XmlMapper().readValue(response.body().string(), responseClass),
                        response.code());
            }
            return new ServiceResponse<>(null, response.code());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service with GET method", e);
        }
    }

    public ServiceResponse<String> executePlainGetRequest(@NonNull OkHttpClient client, Headers headers, String url) throws RemoteServiceException {
        Request request;
        request = new Request.Builder()
                .headers(headers)
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(response.body().string(), response.code());
            }
            return new ServiceResponse<>(null, response.code());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service", e);
        }
    }

    public <T> ServiceResponse<T> executePostRequest(@NonNull OkHttpClient client, Headers headers, String url, Object requestBody, Class<T> responseClass) throws RemoteServiceException {
        RequestBody body = RequestBody.create(new Gson().toJson(requestBody), JSON_MEDIA_TYPE);
        Request request;
        request = new Request.Builder()
                .headers(headers)
                .url(url)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(new Gson().fromJson(response.body().string(), responseClass), response.code());
            }
            return new ServiceResponse<>(response.code(), response.body() != null ? response.body().string() : null);
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service", e);
        }
    }

    public ServiceResponse<String> executePlainPostRequest(@NonNull OkHttpClient client, Headers headers, String url, Object requestBody) throws RemoteServiceException {
        RequestBody body = RequestBody.create(new Gson().toJson(requestBody), JSON_MEDIA_TYPE);
        Request request;
        request = new Request.Builder()
                .headers(headers)
                .url(url)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(response.body().string(), response.code());
            }
            return new ServiceResponse<>(null, response.code());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service", e);
        }
    }

    public <T> ServiceResponse<T> executePostRequest(@NonNull OkHttpClient client, Headers headers, String url,
                                                     Object requestBody, MediaType mediaType, Class<T> responseClass)
            throws RemoteServiceException, ParseException {
        if(JSON_MEDIA_TYPE.equals(mediaType)) {
            return executePostRequest(client, headers, url, requestBody, responseClass);
        }

        Request request;

        try {
            RequestBody body = RequestBody.create(new XmlMapper().writeValueAsString(requestBody), XML_MEDIA_TYPE);

            request = new Request.Builder()
                    .headers(headers)
                    .url(url)
                    .post(body)
                    .build();
        } catch (JsonProcessingException e) {
            throw new ParseException("Unable to parse object");
        }

        try (Response response = client.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(new XmlMapper().readValue(response.body().string(), responseClass),
                        response.code());
            }
            return new ServiceResponse<>(null, response.code());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service with POST method", e);
        }
    }

    public Integer executeDeleteRequest(@NonNull OkHttpClient client, Headers headers, String url)
            throws RemoteServiceException, ParseException {

        Request request;

        request = new Request.Builder()
                .headers(headers)
                .url(url)
                .delete()
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.code();
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service with POST method", e);
        }
    }

    public static boolean isSuccessHttpStatusCode(int code) {
        return code >= 200 && code < 300;
    }
}
