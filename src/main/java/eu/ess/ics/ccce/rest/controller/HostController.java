package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.CSEntryException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.rest.api.v1.IHost;
import eu.ess.ics.ccce.rest.model.AssociatedIocs;
import eu.ess.ics.ccce.rest.model.CSEntryHostWithStatus;
import eu.ess.ics.ccce.rest.model.response.PagedCSEntryHostResponse;
import eu.ess.ics.ccce.service.HostService;
import eu.ess.ics.ccce.service.IocService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RestController
public class HostController implements IHost {
    Logger logger = LoggerFactory.getLogger(HostController.class);

    private final HostService hostService;
    private final IocService iocService;

    @Autowired
    public HostController(HostService hostService, IocService iocService) {
        this.hostService = hostService;
        this.iocService = iocService;
    }

    @Override
    public PagedCSEntryHostResponse listHosts(UserDetails user, String query, Integer page, Integer limit) {
        try {
            return hostService.findAllFromCSEntry(query, page, limit);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public CSEntryHostWithStatus findHostById(UserDetails user, long hostCSEntryId) throws EntityNotFoundException {
        try {
            return hostService.findHostByCSEntryId(hostCSEntryId);
        } catch (CSEntryException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public AssociatedIocs findAssociatedIocsByHosId(UserDetails user, long hostCSEntryId) {
        try {
            AssociatedIocs associatedIocs = new AssociatedIocs();
            associatedIocs.setDeployedIocs(Collections.emptyList());
            associatedIocs.setConfiguredIocs(Collections.emptyList());

            HostEntity host = hostService.hostIdForCSEntryHostIfAlreadyExists(hostCSEntryId);
            if (host != null) {
                associatedIocs.setDeployedIocs(iocService.getDeployedIocsForHost(hostCSEntryId));
                associatedIocs.setConfiguredIocs(iocService.getConfiguredIocsForHost(hostCSEntryId));
                return associatedIocs;
            }
            return associatedIocs;
        } catch (EntityNotFoundException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
        catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
    }
}
