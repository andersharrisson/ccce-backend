package eu.ess.ics.ccce.rest.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public enum DeploymentStatus {
    QUEUED,
    PENDING,
    RUNNING,
    SUCCESSFUL,
    FAILED;

    public static DeploymentStatus getByName(String name){
        for(DeploymentStatus e : DeploymentStatus.values()){
            if(name.equals(e.name())) {
                return e;
            }
        }
        return null;
    }
}
