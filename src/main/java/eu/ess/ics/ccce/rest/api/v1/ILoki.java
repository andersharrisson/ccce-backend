/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.rest.model.response.LokiResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/api/v1/loki")
@SecurityRequirement(name = "bearerAuth")
public interface ILoki {
    @Operation(summary = "Fetches syslog lines for a specific host")
    @Tag(name = "Loki")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Log lines",
                    content = @Content(schema = @Schema(implementation = LokiResponse.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/syslog/host/{hostName}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    LokiResponse fetchSyslogLines(@Parameter(in = ParameterIn.PATH,
            description = "Fully qualified host name (including network)", required = true)
                               @PathVariable("hostName") String hostName,
                                  @Parameter(description = "Time range (in minutes)")
                               @RequestParam(required = false) Integer timeRange);


    @Operation(summary = "Fetches procServ Log lines for a specific host")
    @Tag(name = "Loki")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Log lines",
                    content = @Content(schema = @Schema(implementation = LokiResponse.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception",
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/procserv/host/{hostName}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    LokiResponse fetchProcServLogLines(@Parameter(in = ParameterIn.PATH,
            description = "Host name (without network part)", required = true)
                                  @PathVariable("hostName") String hostName,
                                  @Parameter(description = "Name of the IOC to get procServ logs")
                                  @RequestParam(required = true) String iocName,
                                  @Parameter(description = "Time range (in minutes)")
                                  @RequestParam(required = false) Integer timeRange);
}
