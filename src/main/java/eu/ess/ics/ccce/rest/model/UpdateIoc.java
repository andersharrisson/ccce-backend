package eu.ess.ics.ccce.rest.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class UpdateIoc extends BaseIoc {
    private String sourceUrl;
    private String sourceVersion;
    private Long hostCSEntryId;

    public UpdateIoc(String description, String owner, String sourceUrl, String sourceVersion, Long hostCSEntryId) {
        super(description, owner);
        this.sourceUrl = sourceUrl;
        this.sourceVersion = sourceVersion;
        this.hostCSEntryId = hostCSEntryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getSourceVersion() {
        return sourceVersion;
    }

    public void setSourceVersion(String sourceVersion) {
        this.sourceVersion = sourceVersion;
    }

    public Long getHostCSEntryId() {
        return hostCSEntryId;
    }

    public void setHostCSEntryId(Long hostCSEntryId) {
        this.hostCSEntryId = hostCSEntryId;
    }

    @Override
    public String toString() {
        return "{" +
                ", description='" + description + '\'' +
                ", owner='" + owner + '\'' +
                ", sourceUrl='" + sourceUrl + '\'' +
                ", sourceVersion='" + sourceVersion + '\'' +
                ", hostCSEntryId=" + hostCSEntryId +
                '}';
    }
}

