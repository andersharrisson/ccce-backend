package eu.ess.ics.ccce.rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class BaseController {
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;

    @Autowired
    public BaseController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    protected void checkToken() {
        String token = request.getHeader("CCCE-TOKEN");
        if (!StringUtils.equals("ccce", token)) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED);
        }
    }
}
