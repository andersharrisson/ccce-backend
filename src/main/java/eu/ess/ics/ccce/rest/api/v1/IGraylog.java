package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.graylog.model.GraylogResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RequestMapping("/api/v1/graylog")
@SecurityRequirement(name = "bearerAuth")
public interface IGraylog {

    @Operation(summary = "Log lines")
    @Tag(name = "Graylog")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Log lines",
                    content = @Content(schema = @Schema(implementation = GraylogResponse.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/host/{hostName}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    GraylogResponse fetchLogLines(@Parameter(in = ParameterIn.PATH, description = "Host name", required = true) @PathVariable("hostName") String hostName,
                                  @Parameter(description = "Time range (in seconds)") @RequestParam(required = false) Integer timeRange);
}
