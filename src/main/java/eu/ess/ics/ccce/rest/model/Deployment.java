package eu.ess.ics.ccce.rest.model;

import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class Deployment {
    private Long id;
    private String createdBy;
    private Date startDate;
    private Date endDate;
    private String comment;
    private String iocName;
    private IocVersion version;
    private Host host;
    private DeploymentStatus status;
    private Long awxJobId;
    private String awxJobUrl;

    public Deployment(Long id, String createdBy, Date startDate, Date endDate, String comment, String iocName,
                      IocVersion version, Host host, DeploymentStatus status, Long awxJobId, String awxJobUrl) {
        this.id = id;
        this.createdBy = createdBy;
        this.startDate = startDate;
        this.endDate = endDate;
        this.comment = comment;
        this.iocName = iocName;
        this.version = version;
        this.host = host;
        this.status = status;
        this.awxJobId = awxJobId;
        this.awxJobUrl = awxJobUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getIocName() {
        return iocName;
    }

    public void setIocName(String iocName) {
        this.iocName = iocName;
    }

    public IocVersion getVersion() {
        return version;
    }

    public void setVersion(IocVersion version) {
        this.version = version;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public DeploymentStatus getStatus() {
        return status;
    }

    public void setStatus(DeploymentStatus status) {
        this.status = status;
    }

    public Long getAwxJobId() {
        return awxJobId;
    }

    public void setAwxJobId(Long awxJobId) {
        this.awxJobId = awxJobId;
    }

    public String getAwxJobUrl() {
        return awxJobUrl;
    }

    public void setAwxJobUrl(String awxJobUrl) {
        this.awxJobUrl = awxJobUrl;
    }
}
