/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.rest.model.Ioc;
import eu.ess.ics.ccce.rest.model.response.PagedIocResponse;
import eu.ess.ics.ccce.rest.model.response.UserInfoResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping("/api/v1/gitHelper")
@SecurityRequirement(name = "bearerAuth")
public interface IGitIntegration {
    @Operation(summary = "List Tags")
    @Tag(name = "Git")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "Git exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "List of Tags for a specific GitLab repo",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = String.class))))

    })
    @RequestMapping(
            value = "/tags",
            produces = {"application/json"},
            method = RequestMethod.GET)
    List<String> listTags(@AuthenticationPrincipal UserDetails user,
                              @Parameter(description = "Git repo URL") @RequestParam(required = true) String gitRepo);

    @Operation(summary = "List Branches")
    @Tag(name = "Git")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "Git exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "List of Branches for a specific GitLab repo",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = String.class))))

    })
    @RequestMapping(
            value = "/branches",
            produces = {"application/json"},
            method = RequestMethod.GET)
    List<String> listBranches(@AuthenticationPrincipal UserDetails user,
                              @Parameter(description = "Git repo URL") @RequestParam(required = true) String gitRepo);

    @Operation(summary = "Current-user information")
    @Tag(name = "Git")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "Git exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "Information about the current user",
                    content = @Content(schema = @Schema(implementation = UserInfoResponse.class)))

    })
    @RequestMapping(
            value = "/userInfo",
            produces = {"application/json"},
            method = RequestMethod.GET)
    UserInfoResponse userInfo(@AuthenticationPrincipal UserDetails user);

    @Operation(summary = "User information")
    @Tag(name = "Git")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "Git exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "Information about the current user",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = UserInfoResponse.class))))

    })
    @RequestMapping(
            value = "/userInfo/{userName}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    List<UserInfoResponse> infoFromUserName(@AuthenticationPrincipal UserDetails user,
                                      @Parameter(in = ParameterIn.PATH, description = "The username to retrieve info from", required=true) @PathVariable("userName") String userName);
}
