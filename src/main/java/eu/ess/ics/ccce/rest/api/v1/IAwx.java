package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.rest.model.AwxJobMeta;
import eu.ess.ics.ccce.rest.model.InventoryHost;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RequestMapping("/api/v1/awx")
@SecurityRequirement(name = "apiKey")
public interface IAwx {

    @Operation(summary = "Inventory items")
    @Tag(name = "AWX")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A map of host names and IOC array",
                    content = @Content(mediaType = APPLICATION_JSON_VALUE,
                            schema = @Schema(ref = "#/components/schemas/InventoryMap"))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/inventory",
            produces = {"application/json"},
            method = RequestMethod.GET)
    Map<String, InventoryHost> getInventory();

    @Operation(summary = "Update an AWX job's status")
    @Tag(name = "AWX")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Job updated", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "Job not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/jobs",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    void updateJob(
            @Parameter(in = ParameterIn.DEFAULT, description = "Job to update",
                    required = true, schema = @Schema(implementation = AwxJobMeta.class)) @Valid @RequestBody AwxJobMeta body);
}
