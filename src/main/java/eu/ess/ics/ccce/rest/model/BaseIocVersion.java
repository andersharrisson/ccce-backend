package eu.ess.ics.ccce.rest.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class BaseIocVersion {
    private String sourceUrl;
    private String sourceVersion;

    public BaseIocVersion(String sourceUrl, String sourceVersion) {
        this.sourceUrl = sourceUrl;
        this.sourceVersion = sourceVersion;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getSourceVersion() {
        return sourceVersion;
    }

    public void setSourceVersion(String sourceVersion) {
        this.sourceVersion = sourceVersion;
    }
}
