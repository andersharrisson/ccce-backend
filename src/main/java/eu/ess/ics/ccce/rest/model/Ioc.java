package eu.ess.ics.ccce.rest.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class Ioc extends BaseIoc {
    private long id;
    private IocStatus status;
    private String createdBy;
    private IocVersion latestVersion;
    private Deployment activeDeployment;
    private Host configuredToHost;
    private Boolean isActive;
    private Boolean isDirty;
    private Boolean hasLocalCommits;

    public Ioc(String description, String owner, long id, IocStatus status, String createdBy,
               IocVersion latestVersion, Deployment activeDeployment, Host configuredToHost, Boolean isActive,
               Boolean isDirty, Boolean hasLocalCommits) {
        super(description, owner);
        this.id = id;
        this.status = status;
        this.createdBy = createdBy;
        this.latestVersion = latestVersion;
        this.activeDeployment = activeDeployment;
        this.configuredToHost = configuredToHost;
        this.isActive = isActive;
        this.isDirty = isDirty;
        this.hasLocalCommits = hasLocalCommits;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public IocStatus getStatus() {
        return status;
    }

    public void setStatus(IocStatus status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public IocVersion getLatestVersion() {
        return latestVersion;
    }

    public void setLatestVersion(IocVersion latestVersion) {
        this.latestVersion = latestVersion;
    }

    public Deployment getActiveDeployment() {
        return activeDeployment;
    }

    public void setActiveDeployment(Deployment activeDeployment) {
        this.activeDeployment = activeDeployment;
    }

    public Host getConfiguredToHost() {
        return configuredToHost;
    }

    public void setConfiguredToHost(Host configuredToHost) {
        this.configuredToHost = configuredToHost;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getDirty() {
        return isDirty;
    }

    public Boolean getHasLocalCommits() {
        return hasLocalCommits;
    }
}
