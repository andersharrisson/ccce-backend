/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.GitException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.rest.model.request.Login;
import eu.ess.ics.ccce.rest.model.response.LoginResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@RequestMapping("/api/v1/authentication")
@Tag(name = "Authentication")
public interface IAuthentication {

    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Ok",
                    content = @Content(schema = @Schema(implementation = LoginResponse.class))),
            @ApiResponse(responseCode = "403", description = "Bad username, and/or password",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "Login server unavailable",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception",
                    content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @Operation(summary = "Login user and acquire JWT token",
            description = "Login user with username, and password in order to acquire JWT token")
    @PostMapping(value="/login", produces = {"application/json"})
    ResponseEntity<LoginResponse> login(@RequestBody Login loginInfo) throws GitException;

    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Ok",
                    content = @Content(schema = @Schema(implementation = LoginResponse.class))),
            @ApiResponse(responseCode = "503", description = "Login server unavailable",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception",
                    content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @Operation(summary = "Renewing JWT token",
    description = "Renewing valid, non-expired JWT token",
            security = {@SecurityRequirement(name = "bearerAuth")})
    @PostMapping(value="/renew", produces = {"application/json"})
    ResponseEntity<LoginResponse> tokenRenew(@AuthenticationPrincipal UserDetails user);

    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Ok"),
            @ApiResponse(responseCode = "403", description = "Logout error",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception",
                    content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @Operation(summary = "Logout",
            description = "Logging out the user", security = {@SecurityRequirement(name = "bearerAuth")})
    @DeleteMapping(value="/logout", produces = {"application/json"})
    ResponseEntity<Object> logout(@AuthenticationPrincipal UserDetails user);
}
