package eu.ess.ics.ccce.rest.model;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class InventoryHost {
    private List<InventoryIoc> iocs;

    public InventoryHost(List<InventoryIoc> iocs) {
        this.iocs = iocs;
    }

    public List<InventoryIoc> getIocs() {
        return iocs;
    }

    public void setIocs(List<InventoryIoc> iocs) {
        this.iocs = iocs;
    }
}
