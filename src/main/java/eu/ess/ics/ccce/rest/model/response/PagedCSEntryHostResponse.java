package eu.ess.ics.ccce.rest.model.response;

import eu.ess.ics.ccce.rest.model.CSEntryHostWithStatus;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class PagedCSEntryHostResponse extends BasePagedResponse {
    private List<CSEntryHostWithStatus> csEntryHosts;

    public PagedCSEntryHostResponse(List<CSEntryHostWithStatus> csEntryHosts) {
        this.csEntryHosts = csEntryHosts;
    }

    public PagedCSEntryHostResponse(long totalCount, int listSize, int pageNumber, int limit, List<CSEntryHostWithStatus> csEntryHosts) {
        super(totalCount, listSize, pageNumber, limit);
        this.csEntryHosts = csEntryHosts;
    }

    public List<CSEntryHostWithStatus> getCsEntryHosts() {
        return csEntryHosts;
    }
}
