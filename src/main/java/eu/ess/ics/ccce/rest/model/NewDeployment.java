package eu.ess.ics.ccce.rest.model;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class NewDeployment {
    private List<Long> iocIds;
    private String comment;

    public NewDeployment(List<Long> iocIds, String comment) {
        this.iocIds = iocIds;
        this.comment = comment;
    }

    public List<Long> getIocIds() {
        return iocIds;
    }

    public void setIocIds(List<Long> iocIds) {
        this.iocIds = iocIds;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
