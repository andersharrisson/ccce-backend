package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.awx.model.CommandDetails;
import eu.ess.ics.ccce.awx.model.JobDetails;
import eu.ess.ics.ccce.awx.service.AwxService;
import eu.ess.ics.ccce.exceptions.AwxException;
import eu.ess.ics.ccce.exceptions.AwxServiceException;
import eu.ess.ics.ccce.exceptions.ConcurrentDeploymentFoundException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.IocNotDeployedException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.exceptions.UnauthorizedException;
import eu.ess.ics.ccce.rest.api.v1.IDeployment;
import eu.ess.ics.ccce.rest.model.CommandResponse;
import eu.ess.ics.ccce.rest.model.Deployment;
import eu.ess.ics.ccce.rest.model.DeploymentStatus;
import eu.ess.ics.ccce.rest.model.Ioc;
import eu.ess.ics.ccce.rest.model.NewDeployment;
import eu.ess.ics.ccce.rest.model.request.DeploymentOrder;
import eu.ess.ics.ccce.rest.model.response.PagedDeploymentResponse;
import eu.ess.ics.ccce.service.DeploymentService;
import eu.ess.ics.ccce.service.IocService;
import eu.ess.ics.ccce.service.deployment.DeploymentTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RestController
public class DeploymentController implements IDeployment {

    Logger logger = LoggerFactory.getLogger(DeploymentController.class);

    private final AwxService awxService;
    private final DeploymentService deploymentService;
    private final IocService iocService;

    @Autowired
    public DeploymentController(@Qualifier("awxService") AwxService awxService, DeploymentService deploymentService, IocService iocService) {
        this.awxService = awxService;
        this.deploymentService = deploymentService;
        this.iocService = iocService;
    }

    @Override
    public PagedDeploymentResponse listDeployments(UserDetails userDetails, Long iocId, Long hostCSEntryId, DeploymentStatus status, String user, String query,
                                                   DeploymentOrder orderBy, Boolean isAsc, Integer page, Integer limit) {

        try {
            return deploymentService.findAll(status, iocId, hostCSEntryId, user, query, orderBy, isAsc, page, limit);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Deployment> createDeployment(UserDetails user, @Valid NewDeployment body) {

        try {
            return deploymentService.createDeployment(DeploymentTask.createDeploymentTask(body.getIocIds(), body.getComment(), user));
        }
        catch (EntityNotFoundException | AwxServiceException | ConcurrentDeploymentFoundException | UnauthorizedException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Deployment> createUndeployment(UserDetails user, NewDeployment body) {
        try {
            return deploymentService.createUndeployment(DeploymentTask.createUndeploymentTask(body.getIocIds(), body.getComment(), user));
        }
        catch (EntityNotFoundException | AwxServiceException | ConcurrentDeploymentFoundException | UnauthorizedException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Deployment fetchDeployment(UserDetails user, long deploymentId) throws EntityNotFoundException {

        try {
            return deploymentService.findDeploymentById(deploymentId);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("Deployment", deploymentId);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public JobDetails fetchDeploymentJobDetails(UserDetails user, long awxJobId) {
        try {
            return deploymentService.fetchDeploymentJobDetails(awxJobId);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CommandResponse startIoc(UserDetails user, long iocId) {
        try {
            Ioc ioc = iocService.searchIcById(iocId);
            Deployment activeDeployment = ioc.getActiveDeployment();
            if (activeDeployment != null) {
                return deploymentService.startIoc(ioc, user);
            } else {
                throw new IocNotDeployedException("IOC (" + ioc.getActiveDeployment().getIocName() + ") has no active deployment");
            }
        } catch (AwxException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CommandResponse stopIoc(UserDetails user, long iocId) {
        try {
            Ioc ioc = iocService.searchIcById(iocId);
            if (ioc.getActiveDeployment() != null) {
                return deploymentService.stopIoc(ioc, user);
            } else {
                throw new IocNotDeployedException("IOC (" + ioc.getActiveDeployment().getIocName() + ") has no active deployment");
            }
        } catch (AwxException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CommandResponse restartIoc(UserDetails user, long iocId) {
        try {
            Ioc ioc = iocService.searchIcById(iocId);
            if (ioc.getActiveDeployment() != null) {
                return deploymentService.restartIoc(ioc, user);
            } else {
                throw new IocNotDeployedException("IOC (" + ioc.getActiveDeployment().getIocName() + ") has no active deployment");
            }
        } catch (AwxException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CommandDetails fetchCommandDetails(UserDetails user, long awxCommandId) {
        try {
            return awxService.getCommandDetails(awxCommandId);
        } catch (AwxException e) {
            throw new ServiceException(e);
        }
    }
}
