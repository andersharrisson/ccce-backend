package eu.ess.ics.ccce.rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.ess.ics.ccce.csentry.model.CSEntryHost;
import eu.ess.ics.ccce.csentry.service.CSEntryService;
import eu.ess.ics.ccce.exceptions.*;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.rest.api.v1.IIoc;
import eu.ess.ics.ccce.rest.model.CreateIoc;
import eu.ess.ics.ccce.rest.model.Ioc;
import eu.ess.ics.ccce.rest.model.IocVersion;
import eu.ess.ics.ccce.rest.model.UpdateIoc;
import eu.ess.ics.ccce.rest.model.request.IocOrder;
import eu.ess.ics.ccce.rest.model.response.PagedIocResponse;
import eu.ess.ics.ccce.rest.model.response.PagedIocVersionResponse;
import eu.ess.ics.ccce.service.HostService;
import eu.ess.ics.ccce.service.IocService;
import eu.ess.ics.ccce.service.IocVersionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RestController
public class IocController implements IIoc {

    Logger logger = LoggerFactory.getLogger(IocController.class);

    private final IocService iocService;
    private final IocVersionService iocVersionService;
    private final HostService hostService;
    private final CSEntryService csEntryService;

    @Autowired
    public IocController(IocService iocService, IocVersionService iocVersionService, HostService hostService,
                         @Qualifier("csentryService") CSEntryService csEntryService) {
        this.iocService = iocService;
        this.iocVersionService = iocVersionService;
        this.hostService = hostService;
        this.csEntryService = csEntryService;
    }

    @Override
    public PagedIocResponse listIocs(UserDetails user, String owner, String query, IocOrder orderBy, Boolean isAsc, Integer page, Integer limit) {

        try {
            logger.debug("Trying to list all Ioc's");
            return iocService.findAll(owner, query, orderBy, isAsc, page, limit);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException("Error while trying to list all Ioc's");
        }
    }

    @Override
    public Ioc getIoc(UserDetails user, long iocId) {

        try {
            logger.debug("Trying to get ioc with ID: {}", iocId);
            return iocService.searchIcById(iocId);
        } catch (EntityNotFoundException e) {
            logger.error("Entity not found to get details with ID: {}",  iocId, e);
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Ioc createIoc(UserDetails user, @Valid CreateIoc body) {
        logger.debug("Trying to create new Ioc");
        checkIocCreateBody(body);

        try {
            logger.debug("Trying to create new Ioc");
            HostEntity host = null;
            if (body.getHostCSEntryId() != null) {
                host = getHostId(body.getHostCSEntryId());
            }
            return iocService.createNewIoc(body, host, user);
        } catch (MetaDataFileException | JsonProcessingException e) {
            logger.error("Error while trying to process metadata file: " + e.getMessage() , e);
            throw new MetaDataFileException(e.getMessage());
        } catch (EntityNotFoundException e) {
            logger.error(e.getMessage(), e);
            throw new UnprocessableEntityException(e.getMessage());
        } catch (DataIntegrityViolationException e) {
            logger.error(e.getMessage(), e);
            throw new EntityAlreadyCreatedException("Constraint violation exception, Ioc was already created!");
        } catch (CSEntryServiceException | UnauthorizedException | IncompleteRequestException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException("Error while trying to create new IOC", e);
        }
    }

    @Override
    public Ioc updateIoc(UserDetails user, long iocId, @Valid UpdateIoc body) {
        logger.debug("Trying to update Ioc with ID: {}",  iocId);
        checkIocUpdateBody(body);

        try {
            logger.debug("Trying to update Ioc with ID: {}", iocId);
            HostEntity host = null;
            if (body.getHostCSEntryId() != null) {
                host = getHostId(body.getHostCSEntryId());
            }
            return iocService.updateIoc(iocId, body, host, user);
        } catch (MetaDataFileException | JsonProcessingException e) {
            logger.error("Error while trying to process metadata file content " + e.getMessage(), e);
            throw new MetaDataFileException(e.getMessage());
        } catch (EntityNotFoundException e) {
            logger.error(e.getMessage(), e);
            throw new UnprocessableEntityException(e.getMessage());
        } catch (EmptyResultDataAccessException e) {
            logger.error("Entity not found for update with ID: {}",  iocId, e);
            throw new EntityNotFoundException("IOC", iocId);
        } catch (CSEntryServiceException | UnauthorizedException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while trying to update IOC with ID: {}", iocId, e);
            throw new ServiceException("Error while trying to update IOC with ID: " + iocId, e);
        }
    }

    @Override
    public void deleteIoc(UserDetails user, long iocId) {
        throw new ServiceException("Not implemented");
    }

    @Override
    public PagedIocVersionResponse getIocVersions(UserDetails user, long iocId, Integer page, Integer limit) {

        try {
            return iocVersionService.findAllVersions(iocId, page, limit);
        } catch (EmptyResultDataAccessException e) {
            logger.error("Ioc version can not be found for Ioc ID: {}", iocId, e);
            throw new EntityNotFoundException("IOC", iocId);
        } catch (Exception e) {
            logger.error("Error while trying to get iocVersions for Ioc ID: {}", iocId, e);
            throw new ServiceException("Error while trying to get iocVersions for Ioc ID: " + iocId);
        }
    }

    @Override
    public IocVersion getIocVersion(UserDetails user, long iocId, long iocVersionId) {

        try {
            return iocVersionService.findVersionByIds(iocId, iocVersionId);

        } catch (EmptyResultDataAccessException e) {
            logger.error("Ioc-, or Ioc version ID can not be found ID: {}, IocVerId: {}", iocId, iocVersionId, e);
            throw new EntityNotFoundException("Ioc-, or Ioc version ID can not be found ID: "+ iocId +
                    ", IocVerId: " + iocVersionId);
        } catch (Exception e) {
            logger.error("Error while trying to get iocVersions for Ioc ID: {}", iocId);
            throw new ServiceException(e.getMessage());
        }

    }

    private HostEntity getHostId(long hostCSEntryId) throws CSEntryServiceException, EntityNotFoundException, CSEntryException {
        HostEntity hostEntity = hostService.hostIdForCSEntryHostIfAlreadyExists(hostCSEntryId);
        if (hostEntity == null) {
            CSEntryHost csEntryHost = csEntryService.findHostById(hostCSEntryId);
            return hostService.hostIdForCSEntryHostIfNotExists(csEntryHost);
        }
        return hostEntity;
    }

    private void checkIocCreateBody(CreateIoc ioc) {

        if (ioc.getExternalNameId() == null) {
            throw new IncompleteRequestException("Valid naming-name has to be selected!");
        }
        if (StringUtils.isNotEmpty(ioc.getSourceUrl()) && StringUtils.isEmpty(ioc.getSourceVersion())
                || StringUtils.isEmpty(ioc.getSourceUrl()) && StringUtils.isNotEmpty(ioc.getSourceVersion())) {
            throw new IncompleteRequestException("Source URL and version are both required!");
        }
    }

    /**
     * Checks if all mandatory fields are present in request when updating an IOC.
     * Repository URL, and tag/branch-name are optional, but if one is present -> other should be also present.
     *
     * @param ioc the request body to update an IOC
     * @throws IncompleteRequestException when mandatory/necessary fields are missing from the request
     */
    private void checkIocUpdateBody(UpdateIoc ioc) {
        if (StringUtils.isNotEmpty(ioc.getSourceUrl()) && StringUtils.isEmpty(ioc.getSourceVersion())
                || StringUtils.isEmpty(ioc.getSourceUrl()) && StringUtils.isNotEmpty(ioc.getSourceVersion())) {
            throw new IncompleteRequestException("Source URL and version are both required!");
        }
    }
}
