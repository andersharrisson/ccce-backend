package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.GraylogException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.graylog.model.GraylogResponse;
import eu.ess.ics.ccce.graylog.service.GraylogService;
import eu.ess.ics.ccce.rest.api.v1.IGraylog;
import eu.ess.ics.ccce.util.CsvParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RestController
public class GraylogController implements IGraylog {
    Logger logger = LoggerFactory.getLogger(GraylogController.class);

    private final GraylogService graylogService;

    @Autowired
    public GraylogController(GraylogService graylogService) {
        this.graylogService = graylogService;
    }

    @Override
    public GraylogResponse fetchLogLines(String hostName, Integer timeRange) {
        try {
            String response = graylogService.getLogLines(hostName, timeRange);
            if (StringUtils.isNotEmpty(response)) {
                List<List<String>> lines = CsvParser.parseCsv(response);
                if (lines.size() > 1) {
                    return new GraylogResponse(lines.get(0), lines.subList(1, lines.size()));
                }
            }
            return new GraylogResponse(Collections.emptyList(), Collections.emptyList());
        } catch (GraylogException e) {
            logger.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
    }
}
