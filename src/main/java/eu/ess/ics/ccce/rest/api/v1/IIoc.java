package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.rest.model.CreateIoc;
import eu.ess.ics.ccce.rest.model.Ioc;
import eu.ess.ics.ccce.rest.model.IocVersion;
import eu.ess.ics.ccce.rest.model.UpdateIoc;
import eu.ess.ics.ccce.rest.model.request.IocOrder;
import eu.ess.ics.ccce.rest.model.response.PagedIocResponse;
import eu.ess.ics.ccce.rest.model.response.PagedIocVersionResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RequestMapping("/api/v1/iocs")
@SecurityRequirement(name = "bearerAuth")
public interface IIoc {
    @Operation(summary = "List IOCs")
    @Tag(name = "IOCs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A paged array of IOCs",
                    content = @Content(schema = @Schema(implementation = PagedIocResponse.class)))

    })
    @RequestMapping(
            produces = {"application/json"},
            method = RequestMethod.GET)
    PagedIocResponse listIocs(@AuthenticationPrincipal UserDetails user,
            @Parameter(description = "User name") @RequestParam(required = false) String owner,
            @Parameter(description = "Search text (Search for Custom name, Description, Created by)") @RequestParam(required = false) String query,
            @Parameter(description = "Order by") @RequestParam(required = false) IocOrder orderBy,
            @Parameter(description = "Order Ascending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
            @Parameter(description = "Page size") @RequestParam(required = false) Integer limit);

    @Operation(summary = "Find IOC by ID")
    @Tag(name = "IOCs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "Ioc not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "Found IOC", content = @Content(schema = @Schema(implementation = Ioc.class)))
    })
    @RequestMapping(
            value = "/{iocId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    Ioc getIoc(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "Unique ID of IOC", required = true) @PathVariable("iocId") long iocId) throws EntityNotFoundException;

    @Operation(summary = "Create a new IOC")
    @Tag(name = "IOCs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "IOC created", content = @Content(schema = @Schema(implementation = Ioc.class))),
            @ApiResponse(responseCode = "400", description = "Incomplete request", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden: User doesn't have the necessary permissions in CSEntry Service and/or in Gitlab",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "409", description = "IOC already created", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "422", description = "CSEntry host not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "424", description = "Metadata file not found, or not processable", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "CSEntry service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))})
    @RequestMapping(
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    Ioc createIoc(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.DEFAULT, description = "IOC to create",
                    required = true, schema = @Schema(implementation = CreateIoc.class)) @Valid @RequestBody CreateIoc body);

    @Operation(summary = "Update an existing IOC")
    @Tag(name = "IOCs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "IOC updated", content = @Content(schema = @Schema(implementation = Ioc.class))),
            @ApiResponse(responseCode = "400", description = "Incomplete request", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden: User doesn't have the necessary permissions in CSEntry Service and/or in Gitlab",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "IOC not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "422", description = "CSEntry host not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "424", description = "Metadata file not found, or not processable", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "CSEntry service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/{iocId}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.PUT)
    Ioc updateIoc(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "The id of the IOC to update", required=true) @PathVariable("iocId") long iocId,
            @Parameter(in = ParameterIn.DEFAULT, description = "IOC to update",
                    required = true, schema = @Schema(implementation = UpdateIoc.class)) @Valid @RequestBody UpdateIoc body);

    @Operation(summary = "Deletes a single IOC based on the ID supplied")
    @Tag(name = "IOCs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "IOC deleted", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "IOC not found", content = @Content(schema = @Schema(implementation = GeneralException.class)))})
    @RequestMapping(value = "/{iocId}",
            produces = { "application/json" },
            method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteIoc(@AuthenticationPrincipal UserDetails user, @Parameter(in = ParameterIn.PATH, description = "Id of IOC to delete", required=true) @PathVariable("iocId") long iocId);

    @Operation(summary = "List of all versions of an IOC")
    @Tag(name = "IOC Versions")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "IOC not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A paged array of versions",
                    content = @Content(schema = @Schema(implementation = PagedIocVersionResponse.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/{iocId}/versions",
            produces = {"application/json"},
            method = RequestMethod.GET)
    PagedIocVersionResponse getIocVersions(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "The ID of the IOC to retrieve", required=true) @PathVariable("iocId") long iocId,
            @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
            @Parameter(description = "Page size") @RequestParam(required = false) Integer limit);


    @Operation(summary = "Get a version of an IOC")
    @Tag(name = "IOC Versions")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A paged array of versions",
                    content = @Content(schema = @Schema(implementation = IocVersion.class))),
            @ApiResponse(responseCode = "404", description = "IOC version not found", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/{iocId}/versions/{versionNumber}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    IocVersion getIocVersion(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "The ID of the IOC to retrieve", required=true) @PathVariable("iocId") long iocId,
            @Parameter(in = ParameterIn.PATH, description = "The ID of the version to retrieve", required=true) @PathVariable("versionNumber") long versionNumber);
}
