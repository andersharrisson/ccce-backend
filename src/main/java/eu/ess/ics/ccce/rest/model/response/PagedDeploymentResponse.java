package eu.ess.ics.ccce.rest.model.response;

import eu.ess.ics.ccce.rest.model.Deployment;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class PagedDeploymentResponse extends BasePagedResponse {
    private List<Deployment> deployments;

    public PagedDeploymentResponse(List<Deployment> deployments) {
        this.deployments = deployments;
    }

    public PagedDeploymentResponse(long totalCount, int listSize, int pageNumber, int limit, List<Deployment> deployments) {
        super(totalCount, listSize, pageNumber, limit);
        this.deployments = deployments;
    }

    public List<Deployment> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<Deployment> deployments) {
        this.deployments = deployments;
    }
}
