/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.GitException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.model.RBACToken;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.rest.api.v1.IGitIntegration;
import eu.ess.ics.ccce.rest.model.response.UserInfoResponse;
import eu.ess.ics.ccce.service.GitLabService;
import eu.ess.ics.ccce.service.RBACService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@RestController
public class GitController implements IGitIntegration {

    Logger logger = LoggerFactory.getLogger(GitController.class);

    private final GitLabService gitLabService;
    private final RBACService rbacService;

    @Autowired
    public GitController(GitLabService gitLabService, RBACService rbacService) {
        this.gitLabService = gitLabService;
        this.rbacService = rbacService;
    }

    @Override
    public List<String> listTags(UserDetails user, String gitRepo) {
        try {
            return gitLabService.tagList(gitRepo);
        } catch (GitException e) {
            logger.error("GitException while trying to retrieve git tags", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while trying to retrieve git tags", e);
            throw new ServiceException(e.getMessage());
        }

    }

    @Override
    public List<String> listBranches(UserDetails user, String gitRepo) {
        try {
            return gitLabService.branchList(gitRepo);
        } catch (GitException e) {
            logger.error("GitException while trying to retrieve git branches", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while trying to retrieve git branches", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public UserInfoResponse userInfo(UserDetails user) {
        try {

            UserInfoResponse response = gitLabService.currentUserInfo(user.getUserName());

            if ((StringUtils.isEmpty(response.getFullName())) &&
                    (StringUtils.isEmpty(response.getLoginName()))) {
                RBACToken userInfoFromToken = rbacService.getUserInfoFromToken(user.getToken());
                response.setFullName(userInfoFromToken.getFirstName() + " " + userInfoFromToken.getLastName());
                response.setLoginName(userInfoFromToken.getUserName());
            }

            return response;
        } catch (GitException e) {
            logger.error("GitException while trying to retrieve user info from git", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while trying to retrieve user info from git", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<UserInfoResponse> infoFromUserName(UserDetails user, String userName) {

        try {
            return gitLabService.infoFromUserName(userName);
        } catch (GitException e) {
            logger.error("Error while trying to retrieve info from username from GitLab", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while trying to retrieve info from username from GitLab", e);
            throw new ServiceException("Error while trying to retrieve info from username from GitLab");
        }
    }
}
