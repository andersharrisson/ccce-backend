package eu.ess.ics.ccce.rest.model;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class AssociatedIocs {
    private List<Ioc> deployedIocs;
    private List<Ioc> configuredIocs;

    public List<Ioc> getDeployedIocs() {
        return deployedIocs;
    }

    public void setDeployedIocs(List<Ioc> deployedIocs) {
        this.deployedIocs = deployedIocs;
    }

    public List<Ioc> getConfiguredIocs() {
        return configuredIocs;
    }

    public void setConfiguredIocs(List<Ioc> configuredIocs) {
        this.configuredIocs = configuredIocs;
    }
}
