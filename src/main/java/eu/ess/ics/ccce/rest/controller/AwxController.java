package eu.ess.ics.ccce.rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.rest.api.v1.IAwx;
import eu.ess.ics.ccce.rest.model.AwxJobMeta;
import eu.ess.ics.ccce.rest.model.InventoryHost;
import eu.ess.ics.ccce.service.DeploymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RestController
public class AwxController extends BaseController implements IAwx {
    Logger logger = LoggerFactory.getLogger(AwxController.class);

    @Autowired
    private DeploymentService deploymentService;

    public AwxController(ObjectMapper objectMapper, HttpServletRequest request) {
        super(objectMapper, request);
    }

    @Override
    public Map<String, InventoryHost> getInventory() {
        checkToken();
        try {
            return deploymentService.getInventory();
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateJob(@Valid AwxJobMeta body) {
        checkToken();
        try {
            deploymentService.updateAwxJob(body);
        } catch (EntityNotFoundException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }
}
