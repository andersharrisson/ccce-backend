/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.ccdb.service.CCDBService;
import eu.ess.ics.ccce.exceptions.CCDBException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.rest.api.v1.INaming;
import eu.ess.ics.ccce.rest.model.response.NameResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@RestController
public class NamingController implements INaming {

    private final Logger logger = LoggerFactory.getLogger(NamingController.class);

    private final CCDBService ccdbService;

    @Autowired
    public NamingController(CCDBService ccdbService) {
        this.ccdbService = ccdbService;
    }

    @Override
    public List<NameResponse> fetchIOCByName(String iocName) {
        try {
            return ccdbService.filterByIocName(iocName);

        } catch (ServiceException | CCDBException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while trying to fetch IOC names by name", e);
            throw new ServiceException("Error while trying to fetch IOC names by name");
        }
    }
}
