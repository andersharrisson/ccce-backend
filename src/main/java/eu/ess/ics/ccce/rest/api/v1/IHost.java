package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.rest.model.AssociatedIocs;
import eu.ess.ics.ccce.rest.model.CSEntryHostWithStatus;
import eu.ess.ics.ccce.rest.model.response.PagedCSEntryHostResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RequestMapping("/api/v1")
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Hosts")
public interface IHost {
    @Operation(summary = "List CSEntry hosts")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A paged array of CSEntry hosts",
                    content = @Content(schema = @Schema(implementation = PagedCSEntryHostResponse.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping( value = "/hosts",
            produces = {"application/json"},
            method = RequestMethod.GET)
    PagedCSEntryHostResponse listHosts(@AuthenticationPrincipal UserDetails user,
            @Parameter(description = "Search text, this query string is passed directly to Elasticsearch. " +
                    "E.g.: To search all hosts where the string \"archiver\" appears in any field, use \"archiver\"." +
                    "To restrict the search to only the name field, use \"name:archiver\"." +
                    "Note that if you pass \"name:arch\", this will not match archiver as elasticsearch uses keywords for string matching. " +
                    "In this case you’d need to use a wildcard: \"name:arch*\".")
            @RequestParam(required = false) String query,
            @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
            @Parameter(description = "Page size", schema=@Schema(maximum="100"))
            @RequestParam(required = false) Integer limit);

    @Operation(summary = "Find host by CSEntry ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "Found Host", content = @Content(schema = @Schema(implementation = CSEntryHostWithStatus.class)))
    })
    @RequestMapping(
            value = "/hosts/{hostCSEntryId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    CSEntryHostWithStatus findHostById(@AuthenticationPrincipal UserDetails user, @Parameter(in = ParameterIn.PATH, description = "Unique CSEntry ID of the host", required = true) @PathVariable("hostCSEntryId") long hostCSEntryId) throws EntityNotFoundException;

    @Operation(summary = "Find associated IOCs for the host")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "Found Host", content = @Content(schema = @Schema(implementation = AssociatedIocs.class)))
    })
    @RequestMapping(
            value = "/hosts/{hostCSEntryId}/iocs",
            produces = {"application/json"},
            method = RequestMethod.GET)
    AssociatedIocs findAssociatedIocsByHosId(@AuthenticationPrincipal UserDetails user, @Parameter(in = ParameterIn.PATH, description = "Unique CSEntry ID of the host", required = true) @PathVariable("hostCSEntryId") long hostCSEntryId);
}
