package eu.ess.ics.ccce.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import eu.ess.ics.ccce.model.DeploymentDto;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class InventoryIoc {
    private String name;
    private String git;
    private String version;
    @JsonProperty("epics_version")
    @SerializedName("epics_version")
    private String epicsVersion;
    @JsonProperty("require_version")
    @SerializedName("require_version")
    private String requireVersion;

    public InventoryIoc(String name, String git, String version, String epicsVersion, String requireVersion) {
        this.name = name;
        this.git = git;
        this.version = version;
        this.epicsVersion = epicsVersion;
        this.requireVersion = requireVersion;
    }

    public InventoryIoc(DeploymentDto deploymentDto) {
        this.name = deploymentDto.getIocInventoryName();
        this.git = deploymentDto.getIocVersionSourceUrl();
        this.version = deploymentDto.getIocVersionSourceVersion();
        this.epicsVersion = deploymentDto.getIocVersionEpicsVersion();
        this.requireVersion = deploymentDto.getIocVersionRequireVersion();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGit() {
        return git;
    }

    public void setGit(String git) {
        this.git = git;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getEpicsVersion() {
        return epicsVersion;
    }

    public void setEpicsVersion(String epicsVersion) {
        this.epicsVersion = epicsVersion;
    }

    public String getRequireVersion() {
        return requireVersion;
    }

    public void setRequireVersion(String requireVersion) {
        this.requireVersion = requireVersion;
    }
}
