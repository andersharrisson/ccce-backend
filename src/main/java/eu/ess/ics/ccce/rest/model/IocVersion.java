package eu.ess.ics.ccce.rest.model;

import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class IocVersion extends BaseIocVersion {
    private Long id;
    private Long version;
    private String createdBy;
    private Date createdAt;
    private String namingName;
    private String epicsVersion;
    private String requireVersion;

    public IocVersion(String sourceUrl, String sourceVersion, Long id, Long version, String createdBy, Date createdAt,
                      String namingName, String epicsVersion, String requireVersion) {
        super(sourceUrl, sourceVersion);
        this.id = id;
        this.version = version;
        this.createdBy = createdBy;
        this.createdAt = createdAt;
        this.namingName = namingName;
        this.epicsVersion = epicsVersion;
        this.requireVersion = requireVersion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getNamingName() {
        return namingName;
    }

    public void setNamingName(String namingName) {
        this.namingName = namingName;
    }

    public String getEpicsVersion() {
        return epicsVersion;
    }

    public void setEpicsVersion(String epicsVersion) {
        this.epicsVersion = epicsVersion;
    }

    public String getRequireVersion() {
        return requireVersion;
    }

    public void setRequireVersion(String requireVersion) {
        this.requireVersion = requireVersion;
    }
}
