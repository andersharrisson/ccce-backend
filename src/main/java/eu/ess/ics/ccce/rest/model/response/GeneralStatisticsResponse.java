/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.model.response;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
public class GeneralStatisticsResponse {
    private long numberOfActiveHosts;
    private long numberOfCreatedIocs;
    private long numberOfActiveIocs;
    private long numberOfDeployments;
    private long numberOfActiveDeployments;

    public long getNumberOfActiveHosts() {
        return numberOfActiveHosts;
    }

    public void setNumberOfActiveHosts(long numberOfActiveHosts) {
        this.numberOfActiveHosts = numberOfActiveHosts;
    }

    public long getNumberOfCreatedIocs() {
        return numberOfCreatedIocs;
    }

    public void setNumberOfCreatedIocs(long numberOfCreatedIocs) {
        this.numberOfCreatedIocs = numberOfCreatedIocs;
    }

    public long getNumberOfActiveIocs() {
        return numberOfActiveIocs;
    }

    public void setNumberOfActiveIocs(long numberOfActiveIocs) {
        this.numberOfActiveIocs = numberOfActiveIocs;
    }

    public long getNumberOfDeployments() {
        return numberOfDeployments;
    }

    public void setNumberOfDeployments(long numberOfDeployments) {
        this.numberOfDeployments = numberOfDeployments;
    }

    public long getNumberOfActiveDeployments() {
        return numberOfActiveDeployments;
    }

    public void setNumberOfActiveDeployments(long numberOfActiveDeployments) {
        this.numberOfActiveDeployments = numberOfActiveDeployments;
    }
}
