package eu.ess.ics.ccce.rest.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public enum IocStatus {
    UNDEPLOYED,
    DEPLOYED
}
