package eu.ess.ics.ccce.rest.api.v1;

import eu.ess.ics.ccce.awx.model.CommandDetails;
import eu.ess.ics.ccce.awx.model.JobDetails;
import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.rest.model.CommandResponse;
import eu.ess.ics.ccce.rest.model.Deployment;
import eu.ess.ics.ccce.rest.model.DeploymentStatus;
import eu.ess.ics.ccce.rest.model.NewDeployment;
import eu.ess.ics.ccce.rest.model.request.DeploymentOrder;
import eu.ess.ics.ccce.rest.model.response.PagedDeploymentResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@RequestMapping("/api/v1/deployments")
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Deployments")
public interface IDeployment {
    @Operation(summary = "List deployments")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A paged array of deployments",
                    content = @Content(schema = @Schema(implementation = PagedDeploymentResponse.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            produces = {"application/json"},
            method = RequestMethod.GET)
    PagedDeploymentResponse listDeployments(@AuthenticationPrincipal UserDetails userDetails,
            @Parameter(description = "IOC ID") @RequestParam(required = false) Long iocId,
            @Parameter(description = "Host CSEntry ID") @RequestParam(required = false) Long hostCSEntryId,
            @Parameter(description = "Status") @RequestParam(required = false) DeploymentStatus status,
            @Parameter(description = "User name") @RequestParam(required = false) String user,
            @Parameter(description = "Search text (Search for IOC name, IOC version, Host name, Description, Created by)") @RequestParam(required = false) String query,
            @Parameter(description = "Order by") @RequestParam(required = false) DeploymentOrder orderBy,
            @Parameter(description = "Order Ascending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(description = "Page offset") @RequestParam(required = false) Integer page,
            @Parameter(description = "Page size") @RequestParam(required = false) Integer limit);

    @Operation(summary = "Start a new deployment", description = "", tags= "Deployments")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Deployment created", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Deployment.class)))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden: User doesn't have the necessary permissions in CSEntry Service and/or in Gitlab",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "409", description = "Concurrent deployment(s) or undeployment(s) for IOC(s) are ongoing", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "AWX service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    List<Deployment> createDeployment(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.DEFAULT, description = "Deployment to start",
                    required=true, schema=@Schema(implementation = NewDeployment.class))
            @Valid @RequestBody NewDeployment body);

    @Operation(summary = "Start undeployment", description = "", tags= "Deployments")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Deployment created", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Deployment.class)))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden: user doesn't have the necessary permissions in CSEntry Service", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "409", description = "Concurrent deployment(s) or undeployment(s) for IOC(s) are ongoing", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "AWX service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/undeploy",
            produces = { "application/json" },
            consumes = { "application/json" },
            method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    List<Deployment> createUndeployment(@AuthenticationPrincipal UserDetails user,
                                      @Parameter(in = ParameterIn.DEFAULT, description = "Deployment to start",
                                              required=true, schema=@Schema(implementation = NewDeployment.class))
                                      @Valid @RequestBody NewDeployment body);

    @Operation(summary = "Get deployment")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "Deployment not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A deployment with details",
                    content = @Content(schema = @Schema(implementation = Deployment.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/{deploymentId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    Deployment fetchDeployment(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "Unique ID of deployment", required = true) @PathVariable("deploymentId") long deploymentId) throws EntityNotFoundException;

    @Operation(summary = "Get deployment job details")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "Deployment not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "AWX service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A job with details", content = @Content(schema = @Schema(implementation = JobDetails.class)))
    })
    @RequestMapping(
            value = "/jobs/{awxJobId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    JobDetails fetchDeploymentJobDetails(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "Unique ID of AWX job", required = true) @PathVariable("awxJobId") long awxJobId);

    @Operation(summary = "Start an existing IOC")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "IOC start initiated", content = @Content(schema = @Schema(implementation = CommandResponse.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden: User doesn't have the necessary permissions in CSEntry Service and/or in Gitlab",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "IOC not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "422", description = "IOC has no active deployment", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "AWX service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
            })
    @RequestMapping(
            value = "/{iocId}/start",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    CommandResponse startIoc(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "The id of the IOC to start", required=true) @PathVariable("iocId") long iocId);

    @Operation(summary = "Stop an existing IOC")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "IOC stop initiated", content = @Content(schema = @Schema(implementation = CommandResponse.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden: User doesn't have the necessary permissions in CSEntry Service and/or in Gitlab",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "IOC not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "422", description = "IOC has no active deployment", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "AWX service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
            })
    @RequestMapping(
            value = "/{iocId}/stop",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    CommandResponse stopIoc(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "The id of the IOC to stop", required=true) @PathVariable("iocId") long iocId);

    @Operation(summary = "Restart an existing IOC")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "IOC restart initiated", content = @Content(schema = @Schema(implementation = CommandResponse.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "403", description = "Forbidden: User doesn't have the necessary permissions in CSEntry Service and/or in Gitlab",
                    content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "IOC not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "422", description = "IOC has no active deployment", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "AWX service exception", content = @Content(schema = @Schema(implementation = GeneralException.class)))
    })
    @RequestMapping(
            value = "/{iocId}/restart",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    CommandResponse restartIoc(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "The id of the IOC to restart", required=true) @PathVariable("iocId") long iocId);

    @Operation(summary = "Get ad hoc command details")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "404", description = "AWX command not found", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "500", description = "Service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "503", description = "AWX service exception", content = @Content(schema = @Schema(implementation = GeneralException.class))),
            @ApiResponse(responseCode = "200", description = "A command with details", content = @Content(schema = @Schema(implementation = CommandDetails.class)))
    })
    @RequestMapping(
            value = "/commands/{awxCommandId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    CommandDetails fetchCommandDetails(@AuthenticationPrincipal UserDetails user,
            @Parameter(in = ParameterIn.PATH, description = "Unique ID of AWX command", required = true) @PathVariable("awxCommandId") long awxCommandId);
}
