package eu.ess.ics.ccce.rest.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class CommandResponse {
    private final Long awxCommandId;
    private final String awxCommandUrl;

    public CommandResponse(Long awxCommandId, String awxCommandUrl) {
        this.awxCommandId = awxCommandId;
        this.awxCommandUrl = awxCommandUrl;
    }

    public Long getAwxCommandId() {
        return awxCommandId;
    }

    public String getAwxCommandUrl() {
        return awxCommandUrl;
    }
}
