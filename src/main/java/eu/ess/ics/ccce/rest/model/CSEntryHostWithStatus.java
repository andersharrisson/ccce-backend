package eu.ess.ics.ccce.rest.model;

import eu.ess.ics.ccce.csentry.model.CSEntryHost;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class CSEntryHostWithStatus {
    private CSEntryHost csEntryHost;
    private Boolean assigned;
    private HostStatus status;

    public CSEntryHostWithStatus(CSEntryHost csEntryHost, Boolean assigned, HostStatus status) {
        this.csEntryHost = csEntryHost;
        this.assigned = assigned;
        this.status = status;
    }

    public CSEntryHost getCsEntryHost() {
        return csEntryHost;
    }

    public Boolean getAssigned() {
        return assigned;
    }

    public HostStatus getStatus() {
        return status;
    }
}
