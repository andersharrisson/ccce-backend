package eu.ess.ics.ccce.rest.model.request;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public enum DeploymentOrder {
    ID ("id"),
    CREATED_BY("createdBy"),
    CREATED_AT("createdAt"),
    START_TIME("startTime"),
    FINISHED_AT("finishedAt");

    public final String columnName;

    private DeploymentOrder(String columnName) {
        this.columnName = columnName;
    }
}
