package eu.ess.ics.ccce.rest.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class Host {
    private long csEntryId;
    private String host;
    private String network;
    private HostStatus status;

    public Host(long csEntryId, String host, String network, HostStatus status) {
        this.network = network;
        this.csEntryId = csEntryId;
        this.status = status;
        this.host = host;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public long getCsEntryId() {
        return csEntryId;
    }

    public void setCsEntryId(long csEntryId) {
        this.csEntryId = csEntryId;
    }

    public HostStatus getStatus() {
        return status;
    }

    public void setStatus(HostStatus status) {
        this.status = status;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
