/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.rest.controller;

import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.loki.service.LokiService;
import eu.ess.ics.ccce.rest.api.v1.ILoki;
import eu.ess.ics.ccce.rest.model.response.LokiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@RestController
public class LokiController implements ILoki {

    private final Logger logger = LoggerFactory.getLogger(LokiController.class);

    private final LokiService lokiService;

    @Autowired
    public LokiController(LokiService lokiService) {
        this.lokiService = lokiService;
    }

    @Override
    public LokiResponse fetchSyslogLines(String hostName, Integer timeRange) {
        try {
            return lokiService.getLogLines(hostName, null, timeRange);
        } catch (Exception e) {
            logger.error("Error while trying to get syslogs from Loki", e);
            throw new ServiceException("Error while trying to get syslogs");
        }
    }

    @Override
    public LokiResponse fetchProcServLogLines(String hostName, String iocName, Integer timeRange) {
        try {
            return lokiService.getLogLines(hostName, iocName, timeRange);
        } catch (Exception e) {
            logger.error("Error while trying to get procServ logs from Loki", e);
            throw new ServiceException("Error while trying to get procServ logs");
        }
    }
}
