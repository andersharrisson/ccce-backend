/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.configuration;

import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.util.Constants;
import eu.ess.ics.ccce.util.EncryptUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    Logger logger = LoggerFactory.getLogger(JwtRequestFilter.class);

    private final JwtUserDetailsService jwtUserDetailsService;
    private final JwtTokenUtil jwtTokenUtil;
    private final EncryptUtil encryptUtil;


    @Autowired
    public JwtRequestFilter(JwtUserDetailsService jwtUserDetailsService, JwtTokenUtil jwtTokenUtil,
                            EncryptUtil encryptUtil) {
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.encryptUtil = encryptUtil;
    }

    private class TokenInfo {
        private String userName;
        private String rbacToken;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getRbacToken() {
            return rbacToken;
        }

        public void setRbacToken(String rbacToken) {
            this.rbacToken = rbacToken;
        }
    }

    private TokenInfo extractTokenInfo(String jwtToken) {
        TokenInfo result = new TokenInfo();
        try {
            result.setUserName(jwtTokenUtil.getUsernameFromToken(jwtToken));
            result.setRbacToken(encryptUtil.decrypt(jwtTokenUtil.getClaim(jwtToken, JwtTokenUtil.TOKEN, String.class)));
        } catch (IllegalArgumentException e) {
            logger.error("Unable to get JWT Token", e);
            return null;
        } catch (ExpiredJwtException e) {
            logger.error("JWT Token has expired", e);
            return null;
        }

        return result;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        final String requestTokenHeader = httpServletRequest.getHeader("Authorization");

        String jwtToken = null;
        TokenInfo tokenInfo = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            tokenInfo = extractTokenInfo(jwtToken);
        } else {
            Cookie cookie = WebUtils.getCookie(httpServletRequest, Constants.COOKIE_AUTH_HEADER);

            if (cookie != null) {
                String cookieValue = cookie.getValue();
                jwtToken = cookieValue;

                tokenInfo = extractTokenInfo(cookieValue);
            }
        }

        // Once we get the token validate it.
        if ((tokenInfo != null) && (jwtToken != null) && (tokenInfo.getRbacToken() != null)
                && (SecurityContextHolder.getContext().getAuthentication() == null)) {

            UserDetails userDetails = this.jwtUserDetailsService.loadUserByToken(tokenInfo.getRbacToken());

            // if token is valid configure Spring Security to manually set
            // authentication
            if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
            // After setting the Authentication in the context, we specify
            // that the current user is authenticated. So it passes the
            // Spring Security Configurations successfully.
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}