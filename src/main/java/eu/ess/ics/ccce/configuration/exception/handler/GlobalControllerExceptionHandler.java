/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.configuration.exception.handler;

import eu.ess.ics.ccce.configuration.exception.GeneralException;
import eu.ess.ics.ccce.exceptions.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@RestControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String CONTENT_TYPE = "Content-type";
    private static final String APP_JSON = "Application/JSON";
    private static final String APP_XML = "Application/XML";

    @ExceptionHandler
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        HttpHeaders header = new HttpHeaders();
        header.add(CONTENT_TYPE, APP_JSON);

        GeneralException json = new GeneralException();
        json.setDescription(ex.getMessage());

        HttpStatus resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        //your exception comes here
        if (ex instanceof AuthenticationException | ex instanceof UnauthorizedException) {
            resultStatus = HttpStatus.FORBIDDEN;
        }

        if (ex instanceof GitException || ex instanceof AwxServiceException || ex instanceof CSEntryServiceException) {
            resultStatus = HttpStatus.SERVICE_UNAVAILABLE;
        }

        if (ex instanceof EntityNotFoundException) {
            resultStatus = HttpStatus.NOT_FOUND;
        }

        if (ex instanceof EntityAlreadyCreatedException | ex instanceof ConcurrentDeploymentFoundException) {
            resultStatus = HttpStatus.CONFLICT;
        }

        if (ex instanceof IocNotDeployedException) {
            resultStatus = HttpStatus.UNPROCESSABLE_ENTITY;
        }

        if (ex instanceof IncompleteRequestException) {
            resultStatus = HttpStatus.BAD_REQUEST;
        }

        if (ex instanceof UnprocessableEntityException) {
            resultStatus = HttpStatus.UNPROCESSABLE_ENTITY;
        }

        if (ex instanceof MetaDataFileException) {
            resultStatus = HttpStatus.FAILED_DEPENDENCY;
        }

        if (ex instanceof CCDBException) {
            resultStatus = HttpStatus.SERVICE_UNAVAILABLE;
        }

        return new ResponseEntity<>(json, resultStatus);
    }
}
