package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class ConcurrentDeploymentFoundException extends RuntimeException {
    public ConcurrentDeploymentFoundException(String message) {
        super(message);
    }
}
