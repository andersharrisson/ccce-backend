package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class AwxException extends Exception {
    public AwxException(String message) {
        super(message);
    }
}
