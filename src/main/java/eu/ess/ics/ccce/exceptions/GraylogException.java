package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class GraylogException extends Exception {
    public GraylogException(String message) {
        super(message);
    }
}
