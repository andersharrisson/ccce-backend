package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class CSEntryServiceException extends RuntimeException {
    public CSEntryServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
