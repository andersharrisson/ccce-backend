package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class PrometheusException extends Exception {
    public PrometheusException(String message) {
        super(message);
    }
}
