package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class AwxServiceException extends RuntimeException {
    public AwxServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
