package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class IocNotDeployedException extends RuntimeException {
    public IocNotDeployedException(String message) {
        super(message);
    }
}
