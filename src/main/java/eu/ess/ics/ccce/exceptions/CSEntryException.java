package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class CSEntryException extends Exception {
    public CSEntryException(String message) {
        super(message);
    }
}
