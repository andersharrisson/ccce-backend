package eu.ess.ics.ccce.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class GraylogServiceException extends RuntimeException {
    public GraylogServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
