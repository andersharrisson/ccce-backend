package eu.ess.ics.ccce.csentry.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class CSEntryHostById extends BaseCSEntryHost {
    private final List<String> interfaces;

    public CSEntryHostById(long id, String fqdn, String name, String scope, boolean ioc, String deviceType,
                           String description, String user, Date createdAt,
                           Map<String, Object> ansibleVars, List<String> ansibleGroups, List<String> interfaces) {
        super(id, fqdn, name, scope, ioc, deviceType, description, user, createdAt, ansibleVars, ansibleGroups);
        this.interfaces = interfaces;
    }

    public List<String> getInterfaces() {
        return interfaces;
    }
}
