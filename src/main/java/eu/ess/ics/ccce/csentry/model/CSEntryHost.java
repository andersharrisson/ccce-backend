package eu.ess.ics.ccce.csentry.model;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class CSEntryHost extends BaseCSEntryHost {
    private final List<CSEntryInterface> interfaces;

    public List<CSEntryInterface> getInterfaces() {
        return interfaces;
    }

    public CSEntryHost(CSEntryHostById csEntryHostById, List<CSEntryInterface> interfaces) {
        super(csEntryHostById.getId(), csEntryHostById.getFqdn(), csEntryHostById.getName(), csEntryHostById.getScope(),
                csEntryHostById.isIoc(), csEntryHostById.getDeviceType(), csEntryHostById.getDescription(), csEntryHostById.getUser(),
                csEntryHostById.getCreatedAt(), csEntryHostById.getAnsibleVars(), csEntryHostById.getAnsibleGroups());
        this.interfaces = interfaces;
    }
}
