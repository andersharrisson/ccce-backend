package eu.ess.ics.ccce.csentry.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class BaseCSEntryHost {
    private long id;
    private String fqdn;
    private String name;
    private String scope;
    @SerializedName("is_ioc")
    @JsonProperty("is_ioc")
    private boolean ioc;
    @SerializedName("device_type")
    @JsonProperty("device_type")
    private String deviceType;
    private String description;
    private String user;
    @SerializedName("created_at")
    @JsonProperty("created_at")
    private Date createdAt;
    @SerializedName("ansible_vars")
    @JsonProperty("ansible_vars")
    private Map<String, Object> ansibleVars;
    @SerializedName("ansible_groups")
    @JsonProperty("ansible_groups")
    private List<String> ansibleGroups;

    public BaseCSEntryHost(long id, String fqdn, String name, String scope, boolean ioc, String deviceType,
                           String description, String user, Date createdAt,
                           Map<String, Object> ansibleVars, List<String> ansibleGroups) {
        this.id = id;
        this.fqdn = fqdn;
        this.name = name;
        this.scope = scope;
        this.ioc = ioc;
        this.deviceType = deviceType;
        this.description = description;
        this.user = user;
        this.createdAt = createdAt;
        this.ansibleVars = ansibleVars;
        this.ansibleGroups = ansibleGroups;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFqdn() {
        return fqdn;
    }

    public void setFqdn(String fqdn) {
        this.fqdn = fqdn;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public boolean isIoc() {
        return ioc;
    }

    public void setIoc(boolean ioc) {
        this.ioc = ioc;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Map<String, Object> getAnsibleVars() {
        return ansibleVars;
    }

    public void setAnsibleVars(Map<String, Object> ansibleVars) {
        this.ansibleVars = ansibleVars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAnsibleGroups() {
        return ansibleGroups;
    }

    public void setAnsibleGroups(List<String> ansibleGroups) {
        this.ansibleGroups = ansibleGroups;
    }
}
