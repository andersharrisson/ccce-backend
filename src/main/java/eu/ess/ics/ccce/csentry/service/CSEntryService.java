package eu.ess.ics.ccce.csentry.service;

import com.google.gson.reflect.TypeToken;
import eu.ess.ics.ccce.csentry.model.AnsibleGroup;
import eu.ess.ics.ccce.csentry.model.CSEntryHost;
import eu.ess.ics.ccce.csentry.model.CSEntryHostById;
import eu.ess.ics.ccce.csentry.model.CSEntryInterface;
import eu.ess.ics.ccce.exceptions.CSEntryException;
import eu.ess.ics.ccce.exceptions.CSEntryServiceException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.service.HttpClientService;
import eu.ess.ics.ccce.util.Utils;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Service
@Qualifier("csentryService")
public class CSEntryService extends HttpClientService {
    public static final String CSENTRY_URL = "csentry.server.address";
    private static final String CSENTRY_TOKEN = "csentry.server.ccce.token";
    private static final String HOST_SEARCH_URL = "api/v1/network/hosts/search";
    private static final String HOSTS_URL = "api/v1/network/hosts";
    private static final String GROUPS_URL = "api/v1/network/groups";
    private static final String INTERFACE_URL = "api/v1/network/interfaces";
    private static final String CSENTRY_DATE_FORMAT = "yyyy-MM-dd HH:mm";
    public static final int CSENTRY_PAGE_LIMIT = 100;
    private final Environment env;
    private final String baseUrl;
    private final OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

    @Autowired
    public CSEntryService(Environment env) {
        this.env = env;
        this.baseUrl = env.getProperty(CSENTRY_URL);
    }

    public List<CSEntryHost> searchHost(String queryText, Integer page, Integer limit) throws CSEntryServiceException, CSEntryException {
        try {
            Type listType = new TypeToken<ArrayList<CSEntryHost>>(){}.getType();
            UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(getEndpointUrl(HOST_SEARCH_URL));
            if (!StringUtils.isEmpty(queryText)) {
                ub.queryParam("q", "is_ioc:true AND (" + queryText + ")");
            } else {
                ub.queryParam("q", "is_ioc:true");
            }

            if (page != null && limit != null) {
                ub.queryParam("page", page + 1);
                ub.queryParam("per_page", limit);
            }
            ServiceResponse<List<CSEntryHost>> response = this.executeGetRequest(client, createAuthHeader(), ub.build().toUri().toString(),
                    listType, CSENTRY_DATE_FORMAT);
            if (isSuccessHttpStatusCode(response.getStatusCode())) {
                return response.getEntity();
            } else {
                throw new CSEntryException("searchHost response code: " + response.getStatusCode());
            }
        } catch (MalformedURLException | RemoteServiceException e) {
            throw new CSEntryServiceException("Unable to call CSEntry service", e);
        }
    }

    public CSEntryHost findHostById(long hostCSEntryId) throws CSEntryServiceException, EntityNotFoundException, CSEntryException {
        try {
            UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(getEndpointUrl(HOSTS_URL));
            ub.queryParam("id", hostCSEntryId);
            Type hostListType = new TypeToken<ArrayList<CSEntryHostById>>(){}.getType();
            ServiceResponse<List<CSEntryHostById>> response = this.executeGetRequest(client, createAuthHeader(), ub.build().toUri().toString(),
                    hostListType, CSENTRY_DATE_FORMAT);
            if (isSuccessHttpStatusCode(response.getStatusCode())) {
                CSEntryHostById csEntryHostById = response.getEntity().stream().findFirst().orElse(null);
                if (csEntryHostById != null) {
                    List<CSEntryInterface> interfaces = new ArrayList<>();
                    for (String interfaceName : csEntryHostById.getInterfaces()) {
                        UriComponentsBuilder ubIf = UriComponentsBuilder.fromUriString(getEndpointUrl(INTERFACE_URL));
                        ubIf.queryParam("name", interfaceName);
                        Type interfaceListType = new TypeToken<ArrayList<CSEntryInterface>>() {
                        }.getType();
                        ServiceResponse<List<CSEntryInterface>> interfaceResponse = this.executeGetRequest(client, createAuthHeader(), ubIf.build().toUri().toString(),
                                interfaceListType, CSENTRY_DATE_FORMAT);
                        if (isSuccessHttpStatusCode(interfaceResponse.getStatusCode())) {
                            List<CSEntryInterface> csEntryInterfaces = interfaceResponse.getEntity();
                            interfaces.addAll(csEntryInterfaces.stream().filter(csEntryInterface -> csEntryInterface.getHost().equals(csEntryHostById.getName()))
                                    .collect(Collectors.toList()));
                        } else {
                            throw new CSEntryException("findHostById (" + hostCSEntryId + ") find interface (" + interfaceName + ") response code: " + response.getStatusCode());
                        }
                    }
                    return new CSEntryHost(csEntryHostById, interfaces);
                }
                throw new EntityNotFoundException("CSEntry host", hostCSEntryId);
            } else {
                throw new CSEntryException("findHostById (" + hostCSEntryId + ") response code: " + response.getStatusCode());
            }
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new CSEntryServiceException("Unable to call CSEntry service", e);
        }
    }

    public List<AnsibleGroup> fetchAnsibleGroupByName(String name) throws CSEntryException {
        try {
            Type listType = new TypeToken<ArrayList<AnsibleGroup>>(){}.getType();
            UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(getEndpointUrl(GROUPS_URL));
            ub.queryParam("name", name);
            ServiceResponse<List<AnsibleGroup>> response = this.executeGetRequest(client, createAuthHeader(), ub.build().toUri().toString(),
                    listType, CSENTRY_DATE_FORMAT);
            if (isSuccessHttpStatusCode(response.getStatusCode())) {
                List<AnsibleGroup> groups = response.getEntity();
                if (groups.size() == 0) {
                    throw new EntityNotFoundException("Ansible group by name: " + name);
                }
                return response.getEntity();
            } else {
                throw new CSEntryException("fetchAnsibleGroupByName response code: " + response.getStatusCode());
            }
        } catch (MalformedURLException | RemoteServiceException e) {
            throw new CSEntryServiceException("Unable to call CSEntry service", e);
        }
    }

    private String getEndpointUrl(String endpointPostfix) throws MalformedURLException {
        return new URL(new URL(baseUrl), endpointPostfix).toString();
    }

    private Headers createAuthHeader(){
        return Headers.of("Authorization", "Bearer " + env.getProperty(CSENTRY_TOKEN));
    }
}
