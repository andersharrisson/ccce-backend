package eu.ess.ics.ccce.awx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public enum JobStatus {
    @SerializedName("pending")
    @JsonProperty("pending")
    PENDING,
    @SerializedName("running")
    @JsonProperty("running")
    RUNNING,
    @SerializedName("failed")
    @JsonProperty("failed")
    FAILED,
    @SerializedName("canceled")
    @JsonProperty("canceled")
    CANCELED,
    @SerializedName("successful")
    @JsonProperty("successful")
    SUCCESSFUL;
}
