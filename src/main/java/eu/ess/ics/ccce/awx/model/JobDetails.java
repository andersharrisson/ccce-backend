package eu.ess.ics.ccce.awx.model;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class JobDetails {
    private Job job;
    private List<JobEvent> events;
    private List<JobHostSummary> hostSummaries;
    private Map<String, Map<String, Object>> iocDeploymentResults;
    private String stdout;

    public JobDetails(Job job, List<JobEvent> events, List<JobHostSummary> hostSummaries,
                      Map<String, Map<String, Object>> iocDeploymentResults, String stdout) {
        this.job = job;
        this.events = events;
        this.hostSummaries = hostSummaries;
        this.iocDeploymentResults = iocDeploymentResults;
        this.stdout = stdout;
    }

    public Job getJob() {
        return job;
    }

    public List<JobEvent> getEvents() {
        return events;
    }

    public List<JobHostSummary> getHostSummaries() {
        return hostSummaries;
    }

    public Map<String, Map<String, Object>> getIocDeploymentResults() {
        return iocDeploymentResults;
    }

    public String getStdout() {
        return stdout;
    }
}
