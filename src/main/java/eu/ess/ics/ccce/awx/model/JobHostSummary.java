package eu.ess.ics.ccce.awx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class JobHostSummary {
    public static class Host {
        private int id;
        private String name;
        private String description;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public static class Job {
        private long id;
        private String name;
        private String description;
        private String status;
        private boolean failed;
        private double elapsed;
        private String type;
        @SerializedName("job_template_id")
        @JsonProperty("job_template_id")
        private int jobTemplateId;
        @SerializedName("job_template_name")
        @JsonProperty("job_template_name")
        private String jobTemplateName;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public boolean isFailed() {
            return failed;
        }

        public void setFailed(boolean failed) {
            this.failed = failed;
        }

        public double getElapsed() {
            return elapsed;
        }

        public void setElapsed(double elapsed) {
            this.elapsed = elapsed;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getJobTemplateId() {
            return jobTemplateId;
        }

        public void setJobTemplateId(int jobTemplateId) {
            this.jobTemplateId = jobTemplateId;
        }

        public String getJobTemplateName() {
            return jobTemplateName;
        }

        public void setJobTemplateName(String jobTemplateName) {
            this.jobTemplateName = jobTemplateName;
        }
    }

    public static class SummaryFields {
        private Host host;
        private Job job;

        public Host getHost() {
            return host;
        }

        public void setHost(Host host) {
            this.host = host;
        }

        public Job getJob() {
            return job;
        }

        public void setJob(Job job) {
            this.job = job;
        }
    }

    private long id;
    private String type;
    @SerializedName("summary_fields")
    @JsonProperty("summary_fields")
    private SummaryFields summaryFields;
    private Date created;
    private Date modified;
    private long job;
    private Long host;
    @SerializedName("host_name")
    @JsonProperty("host_name")
    private String hostName;
    private int changed;
    private int dark;
    private int failures;
    private int ok;
    private int processed;
    private int skipped;
    private boolean failed;
    private int ignored;
    private int rescued;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public SummaryFields getSummaryFields() {
        return summaryFields;
    }

    public void setSummaryFields(SummaryFields summaryFields) {
        this.summaryFields = summaryFields;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public long getJob() {
        return job;
    }

    public void setJob(long job) {
        this.job = job;
    }

    public Long getHost() {
        return host;
    }

    public void setHost(Long host) {
        this.host = host;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getChanged() {
        return changed;
    }

    public void setChanged(int changed) {
        this.changed = changed;
    }

    public int getDark() {
        return dark;
    }

    public void setDark(int dark) {
        this.dark = dark;
    }

    public int getFailures() {
        return failures;
    }

    public void setFailures(int failures) {
        this.failures = failures;
    }

    public int getOk() {
        return ok;
    }

    public void setOk(int ok) {
        this.ok = ok;
    }

    public int getProcessed() {
        return processed;
    }

    public void setProcessed(int processed) {
        this.processed = processed;
    }

    public int getSkipped() {
        return skipped;
    }

    public void setSkipped(int skipped) {
        this.skipped = skipped;
    }

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public int getIgnored() {
        return ignored;
    }

    public void setIgnored(int ignored) {
        this.ignored = ignored;
    }

    public int getRescued() {
        return rescued;
    }

    public void setRescued(int rescued) {
        this.rescued = rescued;
    }
}
