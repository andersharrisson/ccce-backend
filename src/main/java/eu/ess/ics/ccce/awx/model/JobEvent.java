package eu.ess.ics.ccce.awx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class JobEvent {
    private long id;
    private String type;
    private Date created;
    private Date modified;
    @JsonProperty("event_display")
    @SerializedName("event_display")
    private String eventDisplay;
    private String stdout;
    @JsonProperty("host_name")
    @SerializedName("host_name")
    private String hostName;
    private String task;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getEventDisplay() {
        return eventDisplay;
    }

    public void setEventDisplay(String eventDisplay) {
        this.eventDisplay = eventDisplay;
    }

    public String getStdout() {
        return stdout;
    }

    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }
}
