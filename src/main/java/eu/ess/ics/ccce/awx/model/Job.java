package eu.ess.ics.ccce.awx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class Job {
    public static class Related {
        private String stdout;
        @JsonProperty("job_events")
        @SerializedName("job_events")
        private String jobEvents;
        @JsonProperty("job_host_summaries")
        @SerializedName("job_host_summaries")
        private String jobHostSummaries;
        private String cancel;
        private String relaunch;

        public String getCancel() {
            return cancel;
        }

        public void setCancel(String cancel) {
            this.cancel = cancel;
        }

        public String getRelaunch() {
            return relaunch;
        }

        public void setRelaunch(String relaunch) {
            this.relaunch = relaunch;
        }

        public String getStdout() {
            return stdout;
        }

        public void setStdout(String stdout) {
            this.stdout = stdout;
        }

        public String getJobEvents() {
            return jobEvents;
        }

        public void setJobEvents(String jobEvents) {
            this.jobEvents = jobEvents;
        }

        public String getJobHostSummaries() {
            return jobHostSummaries;
        }

        public void setJobHostSummaries(String jobHostSummaries) {
            this.jobHostSummaries = jobHostSummaries;
        }
    }

    private long id;
    private long job;
    private String type;
    private long inventory;
    private Related related;
    @SerializedName("job_type")
    private String jobType;
    private String url;
    private JobStatus status;
    private Date started;
    private Date finished;
    private double elapsed;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getJob() {
        return job;
    }

    public void setJob(long job) {
        this.job = job;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getInventory() {
        return inventory;
    }

    public void setInventory(long inventory) {
        this.inventory = inventory;
    }

    public Related getRelated() {
        return related;
    }

    public void setRelated(Related related) {
        this.related = related;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    public Date getFinished() {
        return finished;
    }

    public void setFinished(Date finished) {
        this.finished = finished;
    }

    public double getElapsed() {
        return elapsed;
    }
}
