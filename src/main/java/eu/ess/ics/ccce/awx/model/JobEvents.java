package eu.ess.ics.ccce.awx.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class JobEvents extends BasePagedAwxResponse<JobEvent> { }
