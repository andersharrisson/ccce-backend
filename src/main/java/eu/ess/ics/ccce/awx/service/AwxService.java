package eu.ess.ics.ccce.awx.service;

import com.google.common.collect.ImmutableMap;
import com.google.gson.reflect.TypeToken;
import eu.ess.ics.ccce.awx.model.Command;
import eu.ess.ics.ccce.awx.model.BasePagedAwxResponse;
import eu.ess.ics.ccce.awx.model.CommandDetails;
import eu.ess.ics.ccce.awx.model.CommandEvent;
import eu.ess.ics.ccce.awx.model.CommandEvents;
import eu.ess.ics.ccce.awx.model.Credential;
import eu.ess.ics.ccce.awx.model.Credentials;
import eu.ess.ics.ccce.awx.model.Host;
import eu.ess.ics.ccce.awx.model.Hosts;
import eu.ess.ics.ccce.awx.model.Inventories;
import eu.ess.ics.ccce.awx.model.Inventory;
import eu.ess.ics.ccce.awx.model.JobEvent;
import eu.ess.ics.ccce.awx.model.JobHostSummary;
import eu.ess.ics.ccce.awx.model.JobTemplate;
import eu.ess.ics.ccce.awx.model.NewCommandBody;
import eu.ess.ics.ccce.awx.model.NewJobBody;
import eu.ess.ics.ccce.awx.model.Job;
import eu.ess.ics.ccce.awx.model.JobDetails;
import eu.ess.ics.ccce.awx.model.JobEvents;
import eu.ess.ics.ccce.awx.model.JobHostSummaries;
import eu.ess.ics.ccce.awx.model.JobTemplates;
import eu.ess.ics.ccce.exceptions.AwxException;
import eu.ess.ics.ccce.exceptions.AwxServiceException;
import eu.ess.ics.ccce.exceptions.EntityNotFoundException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.service.HttpClientService;
import eu.ess.ics.ccce.util.Utils;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Service
@Qualifier("awxService")
public class AwxService extends HttpClientService {
    public static final String AWX_URL = "awx.server.address";
    public static final String AWX_TOKEN = "awx.server.ccce.token";
    private static final String AWX_JOB_TEMPLATE_NAME = "awx.job.template.name";
    private static final String JOB_TEMPLATES_URL = "api/v2/job_templates";
    private static final String JOBS_URL = "api/v2/jobs/";
    private static final String AD_HOC_COMMAND_URL = "api/v2/ad_hoc_commands/";
    private static final String INVENTORIES_URL = "/api/v2/inventories";
    private static final String HOSTS_URL = "/api/v2/hosts";
    private static final String CREDENTIALS_URL = "/api/v2/credentials";
    private static final String KEY_IOC_DEPLOYMENT_RESULT = "ioc_deployment_result";
    private static final String KEY_AWX = "awx";
    private static final String KEY_AWX_JOB_ID = "awx_job_id";
    private static final String KEY_IOCS = "iocs";
    private final Environment env;
    private final String baseUrl;
    private final String token;
    private final OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

    @Autowired
    public AwxService(Environment env) {
        this.env = env;
        this.baseUrl = env.getProperty(AWX_URL);
        this.token = env.getProperty(AWX_TOKEN);
    }

    public JobTemplate getJobTemplate(String name) throws AwxException {
        try {
            List<JobTemplate> templates = getAllPaged(JOB_TEMPLATES_URL, ImmutableMap.of("name", name), JobTemplates.class);
            if (templates.size() == 1) {
                return templates.stream().findFirst().get();
            }
            throw new EntityNotFoundException("AWX job template (name: " + name + ")");
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    public Job launchJob(final String launchUrl, final String limit) throws AwxException {
        return launchJob(launchUrl, createNewJob(limit));
    }

    public Job launchJob(final String launchUrl, final String limit, final String iocsToDeploy) throws AwxException {
        return launchJob(launchUrl, createNewJob(limit, iocsToDeploy));
    }

    private Job launchJob(final String launchUrl, final NewJobBody body) throws AwxException {
        try {
            ServiceResponse<Job> response = executePostRequest(client, createAuthHeader(), getEndpointUrl(launchUrl), body, Job.class);
            if (isSuccessHttpStatusCode(response.getStatusCode())) {
                return response.getEntity();
            }
            throw new AwxException("launchJob (launch URL: " + launchUrl + ") response code: "
                    + response.getStatusCode()
                    + ", error message: " + (response.getErrorMessage() != null ? response.getErrorMessage() : "-"));
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    public JobDetails getJobDetails(long jobId) throws AwxException, EntityNotFoundException {
        try {
            ServiceResponse<Job> jobResponse = executeGetRequest(client, createAuthHeader(), getEndpointUrl(JOBS_URL + jobId), Job.class);
            if (isSuccessHttpStatusCode(jobResponse.getStatusCode())) {

                Job job = jobResponse.getEntity();
                // Events query is removed temporary: not used by front-end
                List<JobEvent> events = Collections.emptyList(); //getAllPaged(job.getRelated().getJobEvents(), JobEvents.class);
                List<JobHostSummary> hostSummaries = getAllPaged(job.getRelated().getJobHostSummaries(), JobHostSummaries.class);
                Map<String, Map<String, Object>> iocDeploymentResults = new HashMap<>();
                List<Host> hosts = new ArrayList<>();
                for (JobHostSummary jhs : hostSummaries) {
                    if (jhs.getSummaryFields().getHost() != null) {
                        hosts.add(getHost(jhs.getSummaryFields().getHost().getId()));
                    }
                }
                for (Host host : hosts) {
                    Type mapType = new TypeToken<Map<String, Object>>(){}.getType();
                    ServiceResponse<Map<String, Object>> response = executeGetRequest(client, createAuthHeader(), getEndpointUrl(host.getRelated().getAnsibleFacts()), mapType);
                    Map<String, Object> ansibleFacts = response.getEntity();
                    if (ansibleFacts != null && ansibleFacts.containsKey(KEY_IOC_DEPLOYMENT_RESULT)) {
                        Map<String, Object> rawDeploymentResults = (Map<String, Object>) ansibleFacts.get(KEY_IOC_DEPLOYMENT_RESULT);
                        if (rawDeploymentResults.containsKey(KEY_AWX)
                                && ((Map<String, Object>)rawDeploymentResults.get(KEY_AWX)).containsKey(KEY_AWX_JOB_ID)
                                && jobId ==
                                (((Double) ((Map<String, Object>)rawDeploymentResults.get(KEY_AWX)).get(KEY_AWX_JOB_ID)).longValue())) {
                            if (rawDeploymentResults.containsKey(KEY_IOCS)) {
                                Map<String, Map<String, Object>> deploymentResults = (Map<String, Map<String, Object>>) rawDeploymentResults.get("iocs");
                                iocDeploymentResults.putAll(deploymentResults);
                            }
                        }
                    }
                }

                ServiceResponse<String> stdoutResponse = executePlainGetRequest(client, createAuthHeader(),
                        getEndpointUrl(job.getRelated().getStdout() + "?format=html"));
                if (isSuccessHttpStatusCode(stdoutResponse.getStatusCode())) {
                    String stdout = stdoutResponse.getEntity();
                    return new JobDetails(job, events, hostSummaries, iocDeploymentResults, stdout);
                }
                throw new AwxException("get stdout for job (ID: " + jobId + ") response code: " + stdoutResponse.getStatusCode());
            } else if (jobResponse.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
                throw new EntityNotFoundException("AWX job", jobId);
            }
            throw new AwxException("getJobDetails for job (" + jobId + ") response code: " + jobResponse.getStatusCode());
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    public Inventory getInventory(String name) throws AwxException {
        try {
            List<Inventory> inventories = getAllPaged(INVENTORIES_URL, ImmutableMap.of("name", name), Inventories.class);
            if (inventories.size() == 1) {
                return inventories.stream().findFirst().get();
            }
            throw new EntityNotFoundException("AWX inventory (name: " + name + ")");
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    public Inventory getInventory(long id) throws AwxException {
        try {
            List<Inventory> inventories = getAllPaged(INVENTORIES_URL, ImmutableMap.of("id", Long.toString(id)), Inventories.class);
            if (inventories.size() == 1) {
                return inventories.stream().findFirst().get();
            }
            throw new EntityNotFoundException("AWX inventory (id: " + id + ")");
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    public Host getHost(long id) throws AwxException {
        try {
            List<Host> hosts = getAllPaged(HOSTS_URL, ImmutableMap.of("id", Long.toString(id)), Hosts.class);
            if (hosts.size() == 1) {
                return hosts.stream().findFirst().get();
            }
            throw new EntityNotFoundException("AWX inventory (id: " + id + ")");
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    public Credential getCredential(String name) throws AwxException {
        try {
            List<Credential> credentials = getAllPaged(CREDENTIALS_URL, ImmutableMap.of("name", name), Credentials.class);
            if (credentials.size() == 1) {
                return credentials.stream().findFirst().get();
            }
            throw new EntityNotFoundException("AWX credential (name: " + name + ")");
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    public Command launchAdHocCommand(Inventory inventory, String limit, Credential credential, String moduleName, String moduleArgs, String extraVars) throws AwxException {
        try {
            ServiceResponse<Command> response = executePostRequest(client, createAuthHeader(),
                    getEndpointUrl(AD_HOC_COMMAND_URL), createNewCommand(inventory.getId(), limit, credential.getId(), moduleName, moduleArgs, extraVars), Command.class);
            if (isSuccessHttpStatusCode(response.getStatusCode())) {
                return response.getEntity();
            }
            throw new AwxException("launchAdHocCommand response code: " + response.getStatusCode());
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    public CommandDetails getCommandDetails(long commandId) throws AwxException, EntityNotFoundException {
        try {
            ServiceResponse<Command> commandResponse = executeGetRequest(client, createAuthHeader(),
                    getEndpointUrl(AD_HOC_COMMAND_URL + commandId), Command.class);
            if (isSuccessHttpStatusCode(commandResponse.getStatusCode())) {
                Command command = commandResponse.getEntity();
                List<CommandEvent> events = getAllPaged(command.getRelated().getEvents(), CommandEvents.class);

                ServiceResponse<String> stdoutResponse = executePlainGetRequest(client, createAuthHeader(), getEndpointUrl(command.getRelated().getStdout() + "?format=html"));
                if (isSuccessHttpStatusCode(stdoutResponse.getStatusCode())) {
                    String stdout = stdoutResponse.getEntity();
                    return new CommandDetails(command, events, stdout);
                }
                throw new AwxException("get stdout for command (ID: " + commandId + ") response code: " + stdoutResponse.getStatusCode());
            } else if (commandResponse.getStatusCode() == HttpStatus.NOT_FOUND.value()) {
                throw new EntityNotFoundException("AWX command", commandId);
            }
            throw new AwxException("getCommandDetails for command (" + commandId + ") response code: " + commandResponse.getStatusCode());
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new AwxServiceException("Unable to call AWX service", e);
        }
    }

    private <T, A extends BasePagedAwxResponse<T>> List<T> getAllPaged(String urlPostfix, Class<A> responseClass) throws AwxException, RemoteServiceException, MalformedURLException {
        return getAllPaged(urlPostfix, null, responseClass);
    }

    private <T, A extends BasePagedAwxResponse<T>> List<T> getAllPaged(String urlPostfix, Map<String, String> params, Class<A> responseClass) throws MalformedURLException, RemoteServiceException, AwxException {
        List<T> all = new ArrayList<>();
        while (urlPostfix != null) {
            String url = getEndpointUrl(urlPostfix);
            if (params != null) {
                UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(url);
                for ( Map.Entry<String, String> param : params.entrySet()) {
                    ub.queryParam(param.getKey(), param.getValue());
                }
                url = ub.build().toUri().toString();
            }
            ServiceResponse<A> serviceResponse = executeGetRequest(client, createAuthHeader(),
                    url, responseClass);
            if (isSuccessHttpStatusCode(serviceResponse.getStatusCode())) {
                BasePagedAwxResponse<T> response = serviceResponse.getEntity();
                all.addAll(response.getResults());
                urlPostfix = response.getNext();
            } else {
                throw new AwxException("Error during getting AWX paged response (URL: " + url + "), status code: " + serviceResponse.getStatusCode());
            }
        }
        return all;
    }

    public String getJobTemplateName() {
        return env.getProperty(AWX_JOB_TEMPLATE_NAME);
    }

    private String getEndpointUrl(String endpointPostfix) throws MalformedURLException {
        return new URL(new URL(baseUrl), endpointPostfix).toString();
    }

    private Headers createAuthHeader(){
        return Headers.of("Authorization", "Bearer " + token);
    }

    private NewJobBody createNewJob(final String limit) {
        return new NewJobBody(limit);
    }

    private NewJobBody createNewJob(final String limit, final String iocs) {
        return new NewJobBody(limit, new NewJobBody.ExtraVars(iocs));
    }

    private NewCommandBody createNewCommand(long inventoryId, String limit, long credentialId, String moduleName, String moduleArgs, String extraVars) {
        return new NewCommandBody("run", inventoryId, limit, credentialId, moduleName, moduleArgs,
                0, 3, extraVars, true, false);
    }
}
