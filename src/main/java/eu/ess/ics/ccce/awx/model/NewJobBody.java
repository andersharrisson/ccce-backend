package eu.ess.ics.ccce.awx.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class NewJobBody {
    public static class ExtraVars {
        @SerializedName("iocs_to_deploy")
        private final String iocsToDeploy;

        public ExtraVars(String iocsToDeploy) {
            this.iocsToDeploy = iocsToDeploy;
        }

        public String getIocsToDeploy() {
            return iocsToDeploy;
        }
    }

    private String limit;
    @SerializedName("extra_vars")
    private ExtraVars extraVars;

    public NewJobBody(String limit, ExtraVars extraVars) {
        this.limit = limit;
        this.extraVars = extraVars;
    }

    public NewJobBody(String limit) {
        this.limit = limit;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public ExtraVars getExtraVars() {
        return extraVars;
    }

    public void setExtraVars(ExtraVars extraVars) {
        this.extraVars = extraVars;
    }
}
