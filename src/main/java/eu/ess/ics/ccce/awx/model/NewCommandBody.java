package eu.ess.ics.ccce.awx.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class NewCommandBody {
    @SerializedName("job_type")
    private String jobType;
    private long inventory;
    private String limit;
    private long credential;
    @SerializedName("module_name")
    private String moduleName;
    @SerializedName("module_args")
    private String moduleArgs;
    private int forks;
    private int verbosity;
    @SerializedName("extra_vars")
    private String extraVars;
    @SerializedName("become_enabled")
    private boolean becomeEnabled;
    @SerializedName("diff_mode")
    private boolean diffMode;

    public NewCommandBody(String jobType, long inventory, String limit, long credential, String moduleName, String moduleArgs, int forks, int verbosity, String extraVars, boolean becomeEnabled, boolean diffMode) {
        this.jobType = jobType;
        this.inventory = inventory;
        this.limit = limit;
        this.credential = credential;
        this.moduleName = moduleName;
        this.moduleArgs = moduleArgs;
        this.forks = forks;
        this.verbosity = verbosity;
        this.extraVars = extraVars;
        this.becomeEnabled = becomeEnabled;
        this.diffMode = diffMode;
    }

    public String getJobType() {
        return jobType;
    }

    public long getInventory() {
        return inventory;
    }

    public String getLimit() {
        return limit;
    }

    public long getCredential() {
        return credential;
    }

    public String getModuleName() {
        return moduleName;
    }

    public String getModuleArgs() {
        return moduleArgs;
    }

    public int getForks() {
        return forks;
    }

    public int getVerbosity() {
        return verbosity;
    }

    public String getExtraVars() {
        return extraVars;
    }

    public boolean isBecomeEnabled() {
        return becomeEnabled;
    }

    public boolean isDiffMode() {
        return diffMode;
    }
}
