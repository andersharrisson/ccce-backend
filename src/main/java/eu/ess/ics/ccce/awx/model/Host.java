package eu.ess.ics.ccce.awx.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class Host {
    public static class Related {
        @JsonProperty("ansible_facts")
        @SerializedName("ansible_facts")
        private String ansibleFacts;

        public String getAnsibleFacts() {
            return ansibleFacts;
        }

        public void setAnsibleFacts(String ansibleFacts) {
            this.ansibleFacts = ansibleFacts;
        }
    }

    private long id;
    private String type;
    private String name;
    private long inventory;
    private Related related;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getInventory() {
        return inventory;
    }

    public void setInventory(long inventory) {
        this.inventory = inventory;
    }

    public Related getRelated() {
        return related;
    }

    public void setRelated(Related related) {
        this.related = related;
    }
}
