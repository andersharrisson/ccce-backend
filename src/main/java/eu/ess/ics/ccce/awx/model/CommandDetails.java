package eu.ess.ics.ccce.awx.model;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class CommandDetails {
    private Command command;
    private List<CommandEvent> events;
    private String stdout;

    public CommandDetails(Command command, List<CommandEvent> events, String stdout) {
        this.command = command;
        this.events = events;
        this.stdout = stdout;
    }

    public Command getCommand() {
        return command;
    }

    public List<CommandEvent> getEvents() {
        return events;
    }

    public String getStdout() {
        return stdout;
    }
}
