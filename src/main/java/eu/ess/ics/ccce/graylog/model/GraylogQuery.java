package eu.ess.ics.ccce.graylog.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class GraylogQuery {
    private static final String ELASTIC_SEARCH = "elasticsearch";
    private static final String RELATIVE_TIME_RANGE = "relative";

    private static class QueryString {
        private String type;
        @SerializedName("query_string")
        private String queryString;

        public QueryString(String queryString) {
            this.type = ELASTIC_SEARCH;
            this.queryString = queryString;
        }
    }

    private static class TimeRange {
        private String type;
        private Integer range;

        public TimeRange(Integer range) {
            this.type = RELATIVE_TIME_RANGE;
            this.range = range;
        }
    }

    @SerializedName("query_string")
    private QueryString queryString;
    @SerializedName("timerange")
    private TimeRange timeRange;
    @SerializedName("fields_in_order")
    private List<String> fieldsInOrder;

    public GraylogQuery(String queryString, List<String> fields, Integer range) {
        this.queryString = new QueryString(queryString);
        this.timeRange = new TimeRange(range);
        this.fieldsInOrder = fields;
    }
}
