package eu.ess.ics.ccce.graylog.model;

import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class GraylogResponse {
    private List<String> fields;
    private List<List<String>> lines;

    public GraylogResponse(List<String> fields, List<List<String>> lines) {
        this.fields = fields;
        this.lines = lines;
    }

    public List<String> getFields() {
        return fields;
    }

    public List<List<String>> getLines() {
        return lines;
    }
}
