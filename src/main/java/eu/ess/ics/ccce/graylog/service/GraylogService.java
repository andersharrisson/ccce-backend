package eu.ess.ics.ccce.graylog.service;

import com.google.common.collect.ImmutableList;
import eu.ess.ics.ccce.exceptions.GraylogException;
import eu.ess.ics.ccce.exceptions.GraylogServiceException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.graylog.model.GraylogQuery;
import eu.ess.ics.ccce.service.HttpClientService;
import eu.ess.ics.ccce.util.Utils;
import okhttp3.Credentials;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Service
@Qualifier("graylogService")
public class GraylogService extends HttpClientService {
    private static final String GRAYLOG_URL = "graylog.server.address";
    private static final String GRAYLOG_TOKEN = "graylog.server.ccce.token";
    private static final String MESSAGES_URL = "api/views/search/messages";
    private static final String FIELD_HOSTNAME = "HOSTNAME";
    private static final List<String> FIELDS = ImmutableList.of("timestamp", "source", "program", "message");
    private static final int DEFAULT_TIME_RANGE = 120;
    private final String baseUrl;
    private final String token;
    private final OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

    @Autowired
    public GraylogService(Environment env) {
        this.baseUrl = env.getProperty(GRAYLOG_URL);
        this.token = env.getProperty(GRAYLOG_TOKEN);
    }

    public String getLogLines(String hostName, Integer timeRange) throws GraylogException {
        try {
            ServiceResponse<String> logLinesResponse = executePlainPostRequest(client, createAuthHeader(),
                    getEndpointUrl(MESSAGES_URL), new GraylogQuery(FIELD_HOSTNAME + ":" + hostName, FIELDS,
                            timeRange != null ? timeRange : DEFAULT_TIME_RANGE));
            if (isSuccessHttpStatusCode(logLinesResponse.getStatusCode())) {
                return logLinesResponse.getEntity();
            }
            throw new GraylogException("get log lines (query: " + hostName + ") response code: " + logLinesResponse.getStatusCode());
        } catch (RemoteServiceException | MalformedURLException e) {
            throw new GraylogServiceException("Unable to call Graylog service", e);
        }
    }

    private String getEndpointUrl(String endpointPostfix) throws MalformedURLException {
        return new URL(new URL(baseUrl), endpointPostfix).toString();
    }

    private Headers createAuthHeader(){
        return Headers.of("Authorization", Credentials.basic(token, "token"), "X-Requested-By", "CCCE-BE");
    }
}
