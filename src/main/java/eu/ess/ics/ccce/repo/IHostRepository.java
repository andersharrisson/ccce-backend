/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo;

import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.HostMappingEntity;
import eu.ess.ics.ccce.rest.model.request.HostOrder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
public interface IHostRepository {
    List<HostEntity> findAll(String searchText, HostOrder orderBy, Boolean isAsc, Integer page, Integer limit);
    Long countForPaging(String searchText);
    HostEntity findHostById(long hostId);
    HostEntity findHostByCSEntryId(long hostCSEntryId);
    @Transactional
    void createHost(HostEntity host);
    @Transactional
    void updateHost(HostEntity host);
    void createHostMapping(HostMappingEntity hostMapping);
    HostMappingEntity findLatestMappingByIocId(long iocId);
    List<HostMappingEntity> findLatestMappingsByCSEntryId(long hostCSEntryId);
}
