/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo.model;

import javax.persistence.*;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Entity
@Table(name = "deployment_job_mapping")
public class DeploymentJobMappingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "job")
    private AwxJobEntity awxJob;

    @ManyToOne
    @JoinColumn(name = "deployment")
    private DeploymentEntity deployment;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public AwxJobEntity getAwxJob() {
        return awxJob;
    }

    public void setAwxJob(AwxJobEntity awxJob) {
        this.awxJob = awxJob;
    }

    public DeploymentEntity getDeployment() {
        return deployment;
    }

    public void setDeployment(DeploymentEntity deployment) {
        this.deployment = deployment;
    }
}
