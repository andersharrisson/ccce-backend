package eu.ess.ics.ccce.repo.impl;

import eu.ess.ics.ccce.repo.IDeploymentRepository;
import eu.ess.ics.ccce.repo.model.AwxJobEntity;
import eu.ess.ics.ccce.repo.model.DeploymentEntity;
import eu.ess.ics.ccce.repo.model.DeploymentJobMappingEntity;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.HostMappingEntity;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import eu.ess.ics.ccce.rest.model.DeploymentStatus;
import eu.ess.ics.ccce.rest.model.request.DeploymentOrder;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Repository
public class DeploymentRepository implements IDeploymentRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public void createDeployment(DeploymentEntity deployment) {
        em.persist(deployment);
    }

    @Override
    public void updateDeployment(DeploymentEntity deployment) {
        em.merge(deployment);
    }

    @Override
    public DeploymentEntity findActiveDeploymentForIocVersions(List<Long> iocVersionIds) {
        return em.createQuery("SELECT d FROM DeploymentEntity d WHERE d.iocVersion.id IN (:pIocVersionIds) AND " +
                " d.isActive = true", DeploymentEntity.class)
                .setParameter("pIocVersionIds", iocVersionIds)
                .getSingleResult();
    }

    @Override
    public List<DeploymentEntity> findIntendedOrQueuedDeploymentForIocVersions(List<IocVersionEntity> iocVersions) {
        List<Long> iocVersionIds = iocVersions.stream().map(IocVersionEntity::getId).collect(Collectors.toList());
        return em.createQuery("SELECT d FROM DeploymentEntity d WHERE d.iocVersion.id IN (:pIocVersionIds) AND " +
                " ((d.isIntended = true AND d.isActive = false) OR d.deploymentState = :pQueuedStatus)", DeploymentEntity.class)
                .setParameter("pQueuedStatus", DeploymentStatus.QUEUED.name())
                .setParameter("pIocVersionIds", iocVersionIds).getResultList();
    }

    @Override
    public List<DeploymentEntity> findConcurrentIntendedDeploymentsForDeployments(List<DeploymentEntity> deployments) {
        List<Long> iocVersionIds = deployments.stream().map(de -> de.getIocVersion().getId()).collect(Collectors.toList());
        return em.createQuery("SELECT d FROM DeploymentEntity d WHERE d.iocVersion.id IN (:pIocVersionIds) AND " +
                " d.isIntended = true AND d.isActive = false", DeploymentEntity.class)
                .setParameter("pIocVersionIds", iocVersionIds).getResultList();
    }

    @Override
    public List<DeploymentEntity> findAllIntendedOrQueuedDeployments() {
        return em.createQuery("SELECT d FROM DeploymentEntity d WHERE " +
                " ((d.isIntended = true AND d.isActive = false) OR d.deploymentState = :pQueuedStatus)", DeploymentEntity.class)
                .setParameter("pQueuedStatus", DeploymentStatus.QUEUED.name())
                .getResultList();
    }

    @Override
    public List<DeploymentEntity> findAllIntendedDeployments() {
        return em.createQuery("SELECT d FROM DeploymentEntity d WHERE " +
                " d.isIntended = true AND d.isActive = false", DeploymentEntity.class)
                .getResultList();
    }

    @Override
    public DeploymentEntity findFirstQueued() {
        return em.createQuery("SELECT d FROM DeploymentEntity d WHERE d.deploymentState = :pQueuedStatus ORDER BY d.createdAt ASC", DeploymentEntity.class)
                .setParameter("pQueuedStatus", DeploymentStatus.QUEUED.name()).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public List<DeploymentEntity> findQueuedForTaskId(UUID taskId) {
        return em.createQuery("SELECT d FROM DeploymentEntity d WHERE " +
                " d.taskId = :pTaskId AND d.deploymentState = :pQueuedStatus", DeploymentEntity.class)
                .setParameter("pTaskId", taskId)
                .setParameter("pQueuedStatus", DeploymentStatus.QUEUED.name())
                .getResultList();
    }

    @Override
    public DeploymentEntity findDeploymentById(long id) {
        return em.createQuery("SELECT d FROM DeploymentEntity d WHERE d.id = :pId", DeploymentEntity.class)
                .setParameter("pId", id)
                .getSingleResult();
    }

    @Override
    public List<DeploymentEntity> findAllDeployments(String status, Long iocId, Long hostCSEntryId, String user, String searchText, DeploymentOrder orderBy, Boolean isAsc, int page, int limit) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<DeploymentEntity> cq = cb.createQuery(DeploymentEntity.class);
        Root<DeploymentEntity> from = cq.from(DeploymentEntity.class);

        CriteriaQuery<DeploymentEntity> select = cq.select(from);

        if (orderBy != null) {
            if(BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(from.get(orderBy.columnName)));
            } else {
                cq.orderBy(cb.desc(from.get(orderBy.columnName)));
            }
        } else {
            if(BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(from.get("createdAt")));
            } else {
                cq.orderBy(cb.desc(from.get("createdAt")));
            }
        }

        TypedQuery<DeploymentEntity> q = pagingQuery(cb, select, from, searchText, iocId, hostCSEntryId, status, user, cq);
        q.setFirstResult(page * limit);
        q.setMaxResults(limit);

        return q.getResultList();
    }

    @Override
    public long countForPaging(String status, Long iocId, Long hostCSEntryId, String user, String searchText) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<DeploymentEntity> from = cq.from(DeploymentEntity.class);
        Expression<Long> count1 = cb.count(from);
        CriteriaQuery<Long> select = cq.select(count1);

        return pagingQuery(cb, select, from, searchText, iocId, hostCSEntryId, status, user, cq)
                .getSingleResult();
    }

    private <T, R> TypedQuery<T> pagingQuery(CriteriaBuilder cb, CriteriaQuery<T> select, Root<R> from,
                                             String searchText, Long iocId, Long hostCSEntryId, String status, String user, CriteriaQuery<T> cq) {
        List<Predicate> predicates = new ArrayList<>();
        Join<DeploymentEntity, IocVersionEntity> iocVersion = from.join("iocVersion");
        Join<IocVersionEntity, IocEntity> ioc = iocVersion.join("ioc");
        Join<DeploymentEntity, HostMappingEntity> hostMapping = from.join("hostMapping");
        Join<HostMappingEntity, HostEntity> host = hostMapping.join("host");

        if(searchText != null) {
            predicates.add(
                    cb.or(
                            cb.like(
                                    cb.lower(
                                            ioc.get("customName")
                                    ), "%" + searchText.toLowerCase() + "%"
                            ),
                            cb.like(
                                    cb.lower(
                                            iocVersion.get("sourceVersion")
                                    ), "%" + searchText.toLowerCase() + "%"
                            ),
                            cb.like(
                                    cb.lower(
                                            host.get("host")
                                    ), "%" + searchText.toLowerCase() + "%"
                            ),
                            cb.like(
                                    cb.lower(
                                            from.get("description")
                                    ), "%" + searchText.toLowerCase() + "%"
                            ),
                            cb.like(
                                    cb.lower(
                                            from.get("createdBy")
                                    ), "%" + searchText.toLowerCase() + "%"
                            )
                    )
            );
        }

        if (iocId != null) {
            predicates.add(cb.equal(iocVersion.get("ioc"), iocId));
        }

        if (hostCSEntryId != null) {
            predicates.add(cb.equal(host.get("csEntryId"), hostCSEntryId));
        }

        if (user != null) {
            Predicate userPredicate = cb.like(
                    cb.lower(
                            from.get("createdBy")),
                    "%" + user.toLowerCase() + "%");
            predicates.add(userPredicate);
        }

        if (status != null) {
            Predicate statusPredicate = cb.equal(from.get("deploymentState"), status);
            predicates.add(statusPredicate);
        }

        select.where(cb.and(predicates.toArray(new Predicate[0])));
        return em.createQuery(cq);

    }

    @Override
    public void createAwxJob(AwxJobEntity awxJob) {
        em.persist(awxJob);
    }

    @Override
    public void createDeploymentJobMapping(DeploymentJobMappingEntity deploymentJobMapping) {
        em.persist(deploymentJobMapping);
    }

    @Override
    public List<DeploymentEntity> findAllActiveOrIntended() {
        return em.createQuery("SELECT d from DeploymentEntity d " +
                " WHERE d.isActive = true OR d.isIntended = true ORDER BY d.createdAt DESC", DeploymentEntity.class)
                .getResultList();
    }

    @Override
    public List<DeploymentEntity> findAllActiveByCSEntryHostId(long csEntryHostId) {
        return em.createQuery("SELECT d from DeploymentEntity d " +
                " WHERE d.isActive = true AND d.hostMapping.host.csEntryId = :pCSEntryHostId", DeploymentEntity.class)
                .setParameter("pCSEntryHostId", csEntryHostId)
                .getResultList();
    }

    @Override
    public AwxJobEntity findJobForDeployment(long deploymentId) {

        return em.createQuery("SELECT j.awxJob from DeploymentJobMappingEntity j " +
                " WHERE j.deployment.id = :pDeploymentId", AwxJobEntity.class)
                .setParameter("pDeploymentId", deploymentId)
                .getSingleResult();
    }

    @Override
    public void updateAwxJob(AwxJobEntity awxJob) {
        em.merge(awxJob);
    }

    @Override
    public AwxJobEntity findByAwxJobId(long awxId) {
        return em.createQuery("SELECT j from AwxJobEntity j " +
                " WHERE j.awxJobId = :pAwJobId", AwxJobEntity.class)
                .setParameter("pAwJobId", awxId)
                .getSingleResult();
    }

    @Override
    public List<DeploymentJobMappingEntity> findDeploymentMappingForAwxJobId(long awxJobId) {
        return em.createQuery("SELECT m from DeploymentJobMappingEntity m " +
                " WHERE m.awxJob.id = :pAwJobId", DeploymentJobMappingEntity.class)
                .setParameter("pAwJobId", awxJobId)
                .getResultList();
    }

    @Override
    public long countActiveDeployments() {
        Query query = em.createQuery(
                "SELECT COUNT(*) FROM DeploymentEntity d WHERE d.isActive=:pIsActive");
        query.setParameter("pIsActive", true);
        return (Long) query.getSingleResult();
    }

    @Override
    public long countAllDeployments() {
        Query query = em.createQuery(
                "SELECT COUNT(*) FROM DeploymentEntity");
        return (Long) query.getSingleResult();
    }
}
