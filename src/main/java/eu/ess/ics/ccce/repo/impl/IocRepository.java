/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo.impl;

import eu.ess.ics.ccce.repo.IIocRepository;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.rest.model.request.IocOrder;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Repository
public class IocRepository implements IIocRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public IocEntity findById(Long id) {
        return em.createQuery("SELECT i FROM IocEntity i WHERE i.id =:pId", IocEntity.class)
                .setParameter("pId", id)
                .getSingleResult();
    }

    @Override
    public List<IocEntity> findAll(String owner, String searchText, IocOrder orderBy, Boolean isAsc, int page, int limit) {

        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<IocEntity> cq = cb.createQuery(IocEntity.class);
        Root<IocEntity> from = cq.from(IocEntity.class);

        CriteriaQuery<IocEntity> select = cq.select(from);

        if (orderBy != null) {
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(from.get(orderBy.columnName)));
            } else {
                cq.orderBy(cb.desc(from.get(orderBy.columnName)));
            }
        } else {
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(from.get("createdAt")));
            } else {
                cq.orderBy(cb.desc(from.get("createdAt")));
            }
        }

        TypedQuery<IocEntity> q = pagingQuery(cb, select, from, searchText, owner, cq);
        q.setFirstResult(page * limit);
        q.setMaxResults(limit);

        return q.getResultList();
    }

    @Override
    public Long countAll() {
        return em.createQuery("SELECT COUNT(*) FROM IocEntity", Long.class)
                .getSingleResult();
    }

    private <T, R> TypedQuery<T> pagingQuery(CriteriaBuilder cb, CriteriaQuery<T> select, Root<R> from,
                                             String searchText, String owner, CriteriaQuery<T> cq) {

        List<Predicate> predicates = new ArrayList<>();
        if (owner != null) {
            //TODO owner??
            Predicate ownerPredicate = cb.like(
                    cb.lower(
                            from.get("createdBy")),
                    "%" + owner.toLowerCase() + "%");
            predicates.add(ownerPredicate);
        }

        if (searchText != null) {
            predicates.add(
                    cb.or(
                            cb.like(
                                    cb.lower(
                                            from.get("customName")
                                    ), "%" + searchText.toLowerCase() + "%"
                            ),
                            cb.like(
                                    cb.lower(
                                            from.get("description")
                                    ), "%" + searchText.toLowerCase() + "%"
                            ),
                            cb.like(
                                    cb.lower(
                                            from.get("createdBy")
                                    ), "%" + searchText.toLowerCase() + "%"
                            )
                    )
            );
        }

        select.where(cb.and(predicates.toArray(new Predicate[0])));
        return em.createQuery(cq);
    }

    @Override
    public long countForPaging(String owner, String searchText) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<IocEntity> from = cq.from(IocEntity.class);
        Expression<Long> count1 = cb.count(from);
        CriteriaQuery<Long> select = cq.select(count1);

        return pagingQuery(cb, select, from, searchText, owner, cq)
                .getSingleResult();
    }

    @Override
    public void createIoc(IocEntity ioc) {
        em.persist(ioc);
    }

    @Override
    public void updateIoc(IocEntity iocEnt) {
        em.merge(iocEnt);
    }
}
