/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Entity
@Table(name = "ioc_version")
public class IocVersionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "source_url")
    private String sourceUrl;

    @Column(name = "source_version")
    private String sourceVersion;

    @ManyToOne
    @JoinColumn(name = "ioc")
    private IocEntity ioc;

    @Column(name = "naming_name")
    private String namingName;

    @Column(name = "epics_version")
    private String epicsVersion;

    @Column(name = "require_version")
    private String requireVersion;

    @ManyToOne
    @JoinColumn(name = "meta_data")
    private IocMetaDataEntity iocMetaData;

    @Column(name = "naming_uuid")
    private String namingUuid;

    private Long version;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getSourceVersion() {
        return sourceVersion;
    }

    public void setSourceVersion(String sourceVersion) {
        this.sourceVersion = sourceVersion;
    }

    public IocEntity getIoc() {
        return ioc;
    }

    public void setIoc(IocEntity ioc) {
        this.ioc = ioc;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public IocMetaDataEntity getIocMetaData() {
        return iocMetaData;
    }

    public void setIocMetaData(IocMetaDataEntity iocMetaData) {
        this.iocMetaData = iocMetaData;
    }

    public String getNamingName() {
        return namingName;
    }

    public void setNamingName(String namingName) {
        this.namingName = namingName;
    }

    public String getEpicsVersion() {
        return epicsVersion;
    }

    public void setEpicsVersion(String epicsVersion) {
        this.epicsVersion = epicsVersion;
    }

    public String getRequireVersion() {
        return requireVersion;
    }

    public void setRequireVersion(String requireVersion) {
        this.requireVersion = requireVersion;
    }

    public String getNamingUuid() {
        return namingUuid;
    }

    public void setNamingUuid(String namingUuid) {
        this.namingUuid = namingUuid;
    }
}
