/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo.impl;

import eu.ess.ics.ccce.repo.IIocVersionRepository;
import eu.ess.ics.ccce.repo.model.IocMetaDataEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/

@Repository
public class IocVersionRepository implements IIocVersionRepository {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public void createIocVersion(IocVersionEntity iocVersion) {
        em.persist(iocVersion);
    }

    @Override
    public List<IocVersionEntity> findAllByIocId(long iocId, int page, int limit) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<IocVersionEntity> cq = cb.createQuery(IocVersionEntity.class);
        Root<IocVersionEntity> from = cq.from(IocVersionEntity.class);

        CriteriaQuery<IocVersionEntity> select = cq.select(from);

        cq.orderBy(cb.asc(from.get("createdAt")));

        TypedQuery<IocVersionEntity> q = pagingQuery(cb, select, from, iocId, cq);
        q.setFirstResult(page * limit);
        q.setMaxResults(limit);

        return q.getResultList();
    }

    private <T, R> TypedQuery<T> pagingQuery(CriteriaBuilder cb, CriteriaQuery<T> select, Root<R> from,
                                             long iocId, CriteriaQuery<T> cq) {

        Predicate iocPredicate = cb.equal(from.get("ioc"), iocId);
        select.where(iocPredicate);

        return em.createQuery(cq);
    }

    @Override
    public long countForPaging(long iocId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<IocVersionEntity> from = cq.from(IocVersionEntity.class);
        Expression<Long> count1 = cb.count(from);
        CriteriaQuery<Long> select = cq.select(count1);

        return pagingQuery(cb, select, from, iocId, cq)
                .getSingleResult();
    }

    @Override
    public IocVersionEntity findLatest(long iocId) {
        return em.createQuery("SELECT v FROM IocVersionEntity v WHERE v.ioc.id = :pIocId ORDER BY v.createdAt DESC",
                IocVersionEntity.class)
                .setParameter("pIocId", iocId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public List<Long> findVersionIdsForIoc(long iocId) {
        return em.createQuery("SELECT i.id FROM IocVersionEntity i WHERE i.ioc.id = :pIocId", Long.class)
                .setParameter("pIocId", iocId)
                .getResultList();
    }

    @Override
    public void incrementVersion(long iocVersionId, String sourceUrl, String sourceVersion,
                                 IocMetaDataEntity metadata,
                                 String namingName, String epicsVersion, String requireVersion, String createdBy) {
        IocVersionEntity iocVerEnt = em.createQuery("SELECT v FROM IocVersionEntity v WHERE v.id = :pVerId ORDER BY v.createdAt DESC",
                IocVersionEntity.class)
                .setParameter("pVerId", iocVersionId)
                .setLockMode(LockModeType.PESSIMISTIC_WRITE)
                .setMaxResults(1)
                .getSingleResult();

        IocVersionEntity newVer = new IocVersionEntity();
        newVer.setSourceVersion(sourceVersion);
        newVer.setSourceUrl(sourceUrl);
        newVer.setCreatedBy(createdBy);
        newVer.setIoc(iocVerEnt.getIoc());
        newVer.setIocMetaData(metadata);
        newVer.setNamingName(namingName);
        newVer.setEpicsVersion(epicsVersion);
        newVer.setRequireVersion(requireVersion);
        newVer.setVersion(iocVerEnt.getVersion() +1);

        em.persist(newVer);
    }

    @Override
    public IocVersionEntity findByVersionId(long iocId, long versionNumber) {
        return em.createQuery("SELECT v FROM IocVersionEntity v WHERE v.ioc.id = :pIocId AND v.version = :pVersion",
                IocVersionEntity.class)
                .setParameter("pIocId", iocId)
                .setParameter("pVersion", versionNumber)
                .getSingleResult();
    }
}
