/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo.impl;

import eu.ess.ics.ccce.repo.IHostRepository;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.HostMappingEntity;
import eu.ess.ics.ccce.rest.model.request.HostOrder;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Repository
public class HostRepository implements IHostRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<HostEntity> findAll(String searchText, HostOrder orderBy, Boolean isAsc, Integer page, Integer limit) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<HostEntity> cq = cb.createQuery(HostEntity.class);
        Root<HostEntity> from = cq.from(HostEntity.class);

        CriteriaQuery<HostEntity> select = cq.select(from);

        TypedQuery<HostEntity> q = pagingQuery(cb, select, from, searchText, cq);
        q.setFirstResult(page * limit);
        q.setMaxResults(limit);

        return q.getResultList();
    }

    private <T, R> TypedQuery<T> pagingQuery(CriteriaBuilder cb, CriteriaQuery<T> select, Root<R> from,
                                             String searchText, CriteriaQuery<T> cq) {

        if(searchText != null) {
            select.where(
                    cb.or(
                            cb.like(
                                    cb.lower(
                                            from.get("host")
                                    ), "%" + searchText.toLowerCase() + "%"
                            ),
                            cb.like(
                                    cb.lower(
                                            from.get("network")
                                    ), "%" + searchText.toLowerCase() + "%"
                            )
                    )
            );
        }

        return em.createQuery(cq);
    }

    @Override
    public Long countForPaging(String searchText) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<HostEntity> from = cq.from(HostEntity.class);
        Expression<Long> count1 = cb.count(from);
        CriteriaQuery<Long> select = cq.select(count1);

        return pagingQuery(cb, select, from, searchText, cq).getSingleResult();
    }

    @Override
    public HostEntity findHostById(long hostId) {
        return em.createQuery("SELECT h FROM HostEntity h WHERE h.id=:pHostId", HostEntity.class)
                .setParameter("pHostId", hostId)
                .getSingleResult();
    }

    @Override
    public HostEntity findHostByCSEntryId(long hostCSEntryId) {
        return em.createQuery("SELECT h FROM HostEntity h WHERE h.csEntryId=:pHostCSEntryId", HostEntity.class)
                .setParameter("pHostCSEntryId", hostCSEntryId)
                .getSingleResult();
    }

    @Override
    public void createHost(HostEntity host) {
        em.persist(host);
    }

    @Override
    public void updateHost(HostEntity host) {
        em.merge(host);
    }

    @Override
    public void createHostMapping(HostMappingEntity hostMapping) {
        em.persist(hostMapping);
    }

    @Override
    public HostMappingEntity findLatestMappingByIocId(long iocId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<HostMappingEntity> cq = cb.createQuery(HostMappingEntity.class);
        Root<HostMappingEntity> from = cq.from(HostMappingEntity.class);

        CriteriaQuery<HostMappingEntity> select = cq.select(from);
        Predicate iocPredicate = cb.equal(from.get("ioc"), iocId);
        select.where(iocPredicate);

        cq.orderBy(cb.desc(from.get("createdAt")));

        TypedQuery<HostMappingEntity> q = em.createQuery(cq);
        q.setMaxResults(1);
        return q.getSingleResult();
    }

    @Override
    public List<HostMappingEntity> findLatestMappingsByCSEntryId(long hostCSEntryId) {
        return em.createQuery("SELECT m FROM HostMappingEntity m " +
                "WHERE m.host.csEntryId=:pHostCSEntryId AND m.createdAt IN " +
                "(SELECT MAX(m2.createdAt) FROM HostMappingEntity m2 GROUP BY m2.ioc)", HostMappingEntity.class)
                .setParameter("pHostCSEntryId", hostCSEntryId)
                .getResultList();
    }
}
