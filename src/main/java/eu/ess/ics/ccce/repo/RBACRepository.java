/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo;

import eu.ess.ics.ccce.exceptions.AuthenticationException;
import eu.ess.ics.ccce.exceptions.ParseException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.model.RBACToken;
import eu.ess.ics.ccce.service.HttpClientService;
import eu.ess.ics.ccce.util.Utils;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Base64;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Component
public class RBACRepository {
    private final Environment env;
    private final HttpClientService httpClientService;

    private static final String RBAC_SERVER_BASEURL = "rbac.server.address";
    private static final String TOKEN_PATH = "auth/token";

    @Autowired
    public RBACRepository(Environment env, HttpClientService httpClientService) {
        this.env = env;
        this.httpClientService = httpClientService;
    }

    public RBACToken loginUser(String userName, String password) throws ServiceException, AuthenticationException {
        OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

        String secretHeader = Base64.getEncoder().encodeToString((userName+":"+password).getBytes());

        Headers headers = new Headers.Builder()
                .add("Authorization", "BASIC " + secretHeader)
                .build();

        //trying to log in
        try {
            HttpClientService.ServiceResponse<RBACToken> rbacTokenServiceResponse =
                    httpClientService.executePostRequest(client, headers, env.getProperty(RBAC_SERVER_BASEURL)
                                    + TOKEN_PATH,null, HttpClientService.XML_MEDIA_TYPE, RBACToken.class);

            //successful login
            if (HttpClientService.isSuccessHttpStatusCode(rbacTokenServiceResponse.getStatusCode())) {
                return rbacTokenServiceResponse.getEntity();
            }
        } catch (ParseException | RemoteServiceException e) {
            throw new ServiceException("Error while trying to log in to RBAC", e);
        }

        throw new AuthenticationException("Bad username/password");
    }

    public RBACToken userInfoFromToken(String token) throws AuthenticationException, ServiceException {
        OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

        Headers headers = new Headers.Builder().build();

        try {
            HttpClientService.ServiceResponse<RBACToken> rbacTokenServiceResponse =
                    httpClientService.executeGetRequest(client, headers, env.getProperty(RBAC_SERVER_BASEURL)
                    + TOKEN_PATH + "/" + token, HttpClientService.XML_MEDIA_TYPE, RBACToken.class);

            //token check was successful
            if (HttpClientService.isSuccessHttpStatusCode(rbacTokenServiceResponse.getStatusCode())) {
                return rbacTokenServiceResponse.getEntity();
            }
        } catch (RemoteServiceException | ParseException e) {
            throw new ServiceException("Error while trying to getting token info from RBAC", e);
        }

        throw new AuthenticationException("Token error");
    }

    public boolean checkTokenIsValid(String token) {
        OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

        Headers headers = new Headers.Builder().build();

        try {
           HttpClientService.ServiceResponse<String> response =
                    httpClientService.executePlainGetRequest(client, headers, env.getProperty(RBAC_SERVER_BASEURL)
                            + TOKEN_PATH + "/" + token + "/isvalid");

            //check answer
            return HttpClientService.isSuccessHttpStatusCode(response.getStatusCode());

        } catch (RemoteServiceException e) {
            throw new ServiceException("Error while trying to check token in RBAC", e);
        }
    }

    public RBACToken renewToken(String token) throws AuthenticationException, ServiceException {
        OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

        Headers headers = new Headers.Builder().build();

        try {
            HttpClientService.ServiceResponse<RBACToken> rbacTokenServiceResponse =
                    httpClientService.executePostRequest(client, headers, env.getProperty(RBAC_SERVER_BASEURL)
                            + TOKEN_PATH + "/" + token + "/renew", null, HttpClientService.XML_MEDIA_TYPE, RBACToken.class);

            //token renewal was successful
            if (HttpClientService.isSuccessHttpStatusCode(rbacTokenServiceResponse.getStatusCode())) {
                return rbacTokenServiceResponse.getEntity();
            }
        } catch (RemoteServiceException | ParseException e) {
            throw new ServiceException("Error while trying to renew token in RBAC", e);
        }

        throw new AuthenticationException("Token renewal error");
    }

    public void deleteToken(String token) throws AuthenticationException, ServiceException {
        OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

        Headers headers = new Headers.Builder().build();

        try {
            Integer respCode = httpClientService.executeDeleteRequest(client, headers, env.getProperty(RBAC_SERVER_BASEURL)
                    + TOKEN_PATH + "/" + token);

            //deleting token was successful
            if (HttpClientService.isSuccessHttpStatusCode(respCode)) {
                return ;
            }
        } catch (RemoteServiceException | ParseException e) {
            throw new ServiceException("Error while trying to delete token in RBAC", e);
        }

        throw new AuthenticationException("Error while trying to log out");
    }
}
