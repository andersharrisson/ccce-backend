/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.repo.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Entity
@Table(name = "deployment")
public class DeploymentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ioc_version")
    private IocVersionEntity iocVersion;

    @ManyToOne
    @JoinColumn(name = "host_mapping")
    private HostMappingEntity hostMapping;

    @Column(name = "deployment_state")
    private String deploymentState;

    @Column(name = "description")
    private String description;

    @Column(name = "created_by")
    private String createdBy;

    @CreationTimestamp
    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_intended")
    private Boolean isIntended;

    @Column( name = "finished_at")
    private Date finishedAt;

    @Column( name = "task_id")
    private UUID taskId;

    @Column(name = "is_undeploy")
    private Boolean isUndeploy;

    @Column(name = "`limit`")
    private String limit;

    @Column(name = "iocs_to_deploy")
    private String iocsToDeploy;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public IocVersionEntity getIocVersion() {
        return iocVersion;
    }

    public void setIocVersion(IocVersionEntity iocVersion) {
        this.iocVersion = iocVersion;
    }

    public HostMappingEntity getHostMapping() {
        return hostMapping;
    }

    public void setHostMapping(HostMappingEntity hostMapping) {
        this.hostMapping = hostMapping;
    }

    public String getDeploymentState() {
        return deploymentState;
    }

    public void setDeploymentState(String deploymentState) {
        this.deploymentState = deploymentState;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean isIntended() {
        return isIntended;
    }

    public void setIntended(Boolean intended) {
        isIntended = intended;
    }

    public Date getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Date finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getTaskId() {
        return taskId;
    }

    public void setTaskId(UUID taskId) {
        this.taskId = taskId;
    }

    public Boolean isUndeploy() {
        return isUndeploy;
    }

    public void setUndeploy(Boolean undeploy) {
        isUndeploy = undeploy;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getIocsToDeploy() {
        return iocsToDeploy;
    }

    public void setIocsToDeploy(String iocsToDeploy) {
        this.iocsToDeploy = iocsToDeploy;
    }

    @Override
    public String toString() {
        return "Deployment record -"
                + " IOC:"  + iocVersion.getIoc().getCustomName()
                + ", VERSION: " + iocVersion.getVersion()
                + ", HOST: " + (hostMapping.getHost() != null ? hostMapping.getHost().getHost() : "null")
                + ", ACTIVE: " + isActive
                + ", INTENDED: " + isIntended;
    }
}
