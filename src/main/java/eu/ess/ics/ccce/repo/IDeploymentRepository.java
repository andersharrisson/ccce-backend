package eu.ess.ics.ccce.repo;

import eu.ess.ics.ccce.repo.model.AwxJobEntity;
import eu.ess.ics.ccce.repo.model.DeploymentEntity;
import eu.ess.ics.ccce.repo.model.DeploymentJobMappingEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import eu.ess.ics.ccce.rest.model.request.DeploymentOrder;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.UUID;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public interface IDeploymentRepository {

    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void createDeployment(DeploymentEntity deployment);

    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void updateDeployment(DeploymentEntity deployment);

    DeploymentEntity findActiveDeploymentForIocVersions(List<Long> iocVersionIds);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<DeploymentEntity> findIntendedOrQueuedDeploymentForIocVersions(List<IocVersionEntity> iocVersions);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<DeploymentEntity> findConcurrentIntendedDeploymentsForDeployments(List<DeploymentEntity> deployments);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<DeploymentEntity> findAllIntendedOrQueuedDeployments();

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<DeploymentEntity> findAllIntendedDeployments();

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    DeploymentEntity findFirstQueued();

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<DeploymentEntity> findQueuedForTaskId(UUID taskId);

    DeploymentEntity findDeploymentById(long id);

    List<DeploymentEntity> findAllDeployments(String status, Long iocId, Long hostCSEntryId, String user, String searchText, DeploymentOrder orderBy, Boolean isAsc, int page, int limit);
    long countForPaging(String status, Long iocId, Long hostCSEntryId, String user, String searchText);

    @Transactional
    void createAwxJob(AwxJobEntity awxJob);

    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    void createDeploymentJobMapping(DeploymentJobMappingEntity deploymentJobMapping);

    List<DeploymentEntity> findAllActiveOrIntended();

    List<DeploymentEntity> findAllActiveByCSEntryHostId(long csEntryHostId);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    AwxJobEntity findJobForDeployment(long deploymentId);

    @Transactional
    void updateAwxJob(AwxJobEntity awxJob);

    AwxJobEntity findByAwxJobId(long awxId);

    List<DeploymentJobMappingEntity> findDeploymentMappingForAwxJobId(long awxJobId);

    long countActiveDeployments();
    long countAllDeployments();
}
