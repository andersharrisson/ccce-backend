package eu.ess.ics.ccce;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import eu.ess.ics.ccce.rest.model.InventoryHost;
import eu.ess.ics.ccce.rest.model.InventoryIoc;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Map;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class CcceApplication {
	private static final String OPENAPI_DEV_SERVER = "openapi.dev.server";
	private static final String OPENAPI_TEST_SERVER = "openapi.test.server";
	private final Environment env;

	@Autowired
	public CcceApplication(Environment env) {
		this.env = env;
	}

	public static void main(String[] args) {
		SpringApplication.run(CcceApplication.class, args);
	}

	@Bean
	public OpenAPI customOpenAPI(@Value("${api.version}") String appVersion) {
		return new OpenAPI()
				.components(new Components()
						.addSchemas("InventoryMap", new Schema<Map<String, InventoryHost>>()
								.example(ImmutableMap.of("abc.cslab.esss.lu.se", new InventoryHost(ImmutableList.of(
										new InventoryIoc("my-ioc-1", "git.com/my-repo.git", "master", "7.0.5", "3.4.0"),
										new InventoryIoc("my-ioc-2", "git.com/my-repo-2.git", "master","7.0.5", "3.4.0")))
										)
								)
						)
				)
				.info(new Info().title("CCCE API").version(appVersion).description("CCCE backend"))
				.servers(ImmutableList.of(
						new Server().url(env.getProperty(OPENAPI_DEV_SERVER)).description("Development server (local)"),
						new Server().url(env.getProperty(OPENAPI_TEST_SERVER)).description("Test server")));
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST","PUT", "DELETE");
			}
		};
	}
}
