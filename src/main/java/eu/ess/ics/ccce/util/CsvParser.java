package eu.ess.ics.ccce.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class CsvParser {
    private static final String COMMA_DELIMITER = ",";

    public static List<List<String>> parseCsv(String csv) {
        List<List<String>> records = new ArrayList<>();
        try (Scanner scanner = new Scanner(csv)) {
            while (scanner.hasNextLine()) {
                records.add(getRecordFromLine(scanner.nextLine()));
            }
        }
        return records;
    }

    private static List<String> getRecordFromLine(String line) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(COMMA_DELIMITER);
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next().replaceAll("\"", ""));
            }
        }
        return values;
    }
}
