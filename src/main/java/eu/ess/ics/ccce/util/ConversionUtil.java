/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.util;

import eu.ess.ics.ccce.loki.model.Data;
import eu.ess.ics.ccce.loki.model.Result;
import eu.ess.ics.ccce.loki.model.Root;
import eu.ess.ics.ccce.model.DeploymentDto;
import eu.ess.ics.ccce.model.RBACToken;
import eu.ess.ics.ccce.model.UserDetails;
import eu.ess.ics.ccce.repo.model.AwxJobEntity;
import eu.ess.ics.ccce.repo.model.DeploymentEntity;
import eu.ess.ics.ccce.repo.model.HostEntity;
import eu.ess.ics.ccce.repo.model.IocEntity;
import eu.ess.ics.ccce.repo.model.IocVersionEntity;
import eu.ess.ics.ccce.rest.model.*;
import eu.ess.ics.ccce.rest.model.response.LokiMessage;
import eu.ess.ics.ccce.rest.model.response.LokiResponse;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
public class ConversionUtil {

    private static final String IOC_NAME_COLON = ":";
    private static final String IOC_NAME_UNDERSCORE = "_";

    //not intended to be instantiated
    private ConversionUtil() {
    }

    public static Ioc convertToIoc(IocEntity iocEntity, IocVersionEntity iocVersionEntity, Deployment deployment,
                                   Host configuredHost, Boolean isActive,
                                   Boolean isDirtyRepo, Boolean hasLocalCommits) {
        Ioc result = null;

        if (iocEntity != null) {
            //TODO owner == createdBy???
            result = new Ioc(iocEntity.getDescription(), iocEntity.getCreatedBy(),
                    iocEntity.getId(), null, iocEntity.getCreatedBy(),
                    convertToIocVersion(iocVersionEntity), deployment, configuredHost, isActive,
                    isDirtyRepo, hasLocalCommits);
        }

        return result;
    }

    public static IocVersion convertToIocVersion(IocVersionEntity ent) {
        IocVersion result = null;

        if (ent != null) {
            result = new IocVersion(ent.getSourceUrl(),
                    ent.getSourceVersion(), ent.getId(), ent.getVersion(), ent.getCreatedBy(),
                    ent.getCreatedAt(), ent.getNamingName(), ent.getEpicsVersion(), ent.getRequireVersion());
        }

        return result;
    }

    public static Host convertToHost(HostEntity ent, HostStatus hostStatus) {
        Host result = null;

        if (ent != null) {
            result = new Host(ent.getCsEntryId(), ent.getHost(), ent.getNetwork(), hostStatus);
        }

        return result;
    }

    public static Deployment convertToDeployment(DeploymentEntity deploymentEntity, HostStatus hostStatus, AwxJobEntity jobEntity) {
        Deployment result = null;

        if (deploymentEntity != null) {
            result = new Deployment(deploymentEntity.getId(), deploymentEntity.getCreatedBy(),
                    deploymentEntity.getStartTime(), deploymentEntity.getFinishedAt(), deploymentEntity.getDescription(),
                    deploymentEntity.getIocVersion().getNamingName(), ConversionUtil.convertToIocVersion(deploymentEntity.getIocVersion()),
                    ConversionUtil.convertToHost(deploymentEntity.getHostMapping().getHost(), hostStatus), DeploymentStatus.getByName(deploymentEntity.getDeploymentState()),
                    jobEntity != null ? jobEntity.getAwxJobId() : null, jobEntity != null ? jobEntity.getAwxJobUrl() : null);
        }

        return result;
    }

    public static DeploymentDto convert(DeploymentEntity deploymentEntity) {
        return new DeploymentDto(
                getIocInventoryName(deploymentEntity.getIocVersion()),
                deploymentEntity.getIocVersion().getSourceUrl(),
                deploymentEntity.getIocVersion().getSourceVersion(),
                deploymentEntity.getIocVersion().getEpicsVersion(),
                deploymentEntity.getIocVersion().getRequireVersion(),
                deploymentEntity.getHostMapping().getHost() != null
                        ? deploymentEntity.getHostMapping().getHost().getHost() : null,
                deploymentEntity.isActive(),
                deploymentEntity.isIntended());
    }

    public static String getIocInventoryName(IocVersionEntity iocVersionEntity) {
        return iocVersionEntity.getNamingName();
    }

    public static IocStatus convertToIocStatus(DeploymentEntity deployment) {
        IocStatus result = null;
        if (deployment != null) {
            result = deployment.isActive() ? IocStatus.DEPLOYED : IocStatus.UNDEPLOYED;
        }

        return result;
    }

    public static UserDetails convertToUserDetails(RBACToken rbacInfo) {
        UserDetails result = null;

        if (rbacInfo != null) {
            result = new UserDetails();

            result.setUserName(rbacInfo.getUserName());
            result.setToken(rbacInfo.getId());
        }

        return result;
    }

    /**
     * Converts the response of the Loki to List of string response.
     *
     * @param lokiResponse the response from the Loki.
     * @return List of strings containing the dates, and logs.
     * Empty list will be responded if no logs found.
     */

    public static LokiResponse convertToLokiResponse(Root lokiResponse) {
        LokiResponse result = new LokiResponse();
        List<LokiMessage> lokiLogList = new ArrayList<>();

        if (lokiResponse != null) {
            Data data = lokiResponse.getData();
            if (data != null) {
                List<Result> lokiLogResults = data.getResult();
                if (lokiLogResults != null) {
                    List<LokiMessage> logList = new ArrayList<>();

                    result.setLines(logList);

                    for (Result r : lokiLogResults) {

                        List<List<String>> logValues = r.getValues();
                        for (List<String> subVal : logValues) {

                            lokiLogList.add(lokiLogExtraction(subVal));
                        }
                    }
                }
            }
        }

        result.setLines(convertLokiLogsToResponse(lokiLogList));

        //always return an empty list instead of NULL value
        if (result.getLines() == null) {
            result.setLines(Collections.emptyList());
        }

        return result;
    }

    //converting Loki log-list to String-list response object
    //before converting log-lines will be sorted by date
    private static List<LokiMessage> convertLokiLogsToResponse(List<LokiMessage> lokiLogList) {
        List<LokiMessage> result = new ArrayList<>(Collections.emptyList());

        if (lokiLogList != null) {
            //order list by date
            lokiLogList.sort(Comparator.comparing(LokiMessage::getLogDate));
            //add log-lines
            for (LokiMessage lokiLog : lokiLogList) {
                result.add(new LokiMessage(
                        lokiLog.getLogDate(),
                        lokiLog.getLogMessage())
                );
            }
        }

        return result;
    }

    // trying to extract logs from Loki into own "Log" object
    private static LokiMessage lokiLogExtraction(List<String> params) {
        LokiMessage result = null;

        if (params != null) {
            result = new LokiMessage();

            for (String p : params) {
                if (StringUtils.isNumeric(p)) {
                    long millis = TimeUnit.MILLISECONDS.convert(Long.parseLong(p),
                            TimeUnit.NANOSECONDS);

                    result.setLogDate(new Date(millis));
                } else {
                    result.setLogMessage(result.getLogMessage() + p.replace("\n", ""));
                }
            }
        }

        return result;
    }

    /**
     * Transforms iocName from which contains ":" to name that will contain "_" instead.
     *
     * @param iocName the name of the ioc. If empty -> empty string will be responded.
     * @return transformed iocName (":" will with replaced by "_")
     */
    public static String transformIocName(String iocName) {
        if (StringUtils.isNotEmpty(iocName)) {
            return StringUtils.replace(iocName, IOC_NAME_COLON, IOC_NAME_UNDERSCORE);
        }

        return iocName;
    }

    /**
     * Formats date into ISO format string.
     *
     * @param timeToConvert the date that has to be converted.
     * @return the date converted to ISO string format.
     */
    public static String dateToISOFormat(Date timeToConvert) {
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        sdf.setTimeZone(TimeZone.getDefault());

        return sdf.format(timeToConvert);
    }
}
