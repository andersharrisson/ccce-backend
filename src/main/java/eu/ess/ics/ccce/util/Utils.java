package eu.ess.ics.ccce.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
@Component
public class Utils {

    private static final String LIST_LIMIT = "response.list.max.size";

    @Autowired
    private Environment env;

    private final int responseListMaxSize;

    public Utils(Environment env) {
        this.env = env;
        this.responseListMaxSize = Integer.parseInt(env.getProperty(LIST_LIMIT));
    }

    public static OkHttpClient getTestOnlyClient(OkHttpClient client) {
        OkHttpClient.Builder clientBuilder = client.newBuilder().readTimeout(10, TimeUnit.SECONDS);

        boolean allowUntrusted = true;

        if (  allowUntrusted) {
            final X509TrustManager trustManager= new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkServerTrusted(final X509Certificate[] chain,
                                               final String authType) {
                }

                @Override
                public void checkClientTrusted(final X509Certificate[] chain,
                                               final String authType) {
                }
            };

            SSLContext sslContext = null;
            try {
                sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, new TrustManager[]{trustManager}, new java.security.SecureRandom());
            } catch (NoSuchAlgorithmException | KeyManagementException e) {
               return null;
            }

            clientBuilder.sslSocketFactory(sslContext.getSocketFactory(), trustManager);

            HostnameVerifier hostnameVerifier = (hostname, session) -> true;
            clientBuilder.hostnameVerifier( hostnameVerifier);
        }

        return clientBuilder.build();
    }

    public  PagingLimitDto pageLimitConverter(Integer page, Integer limit) {
        return pageLimitConverter(page, limit, null);
    }

    public  PagingLimitDto pageLimitConverter(Integer page, Integer limit, Integer customPageLimit) {

        int maxLimitSize = customPageLimit != null ? customPageLimit
                : responseListMaxSize;

        int pageSize = page == null ? 0 : page;
        int limitSize = limit == null ? maxLimitSize : Math.min(limit, maxLimitSize);

        return new PagingLimitDto(pageSize, limitSize);
    }

    public static String convertYamlToJson(String yaml) throws JsonProcessingException {
        ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
        Object obj = yamlReader.readValue(yaml, Object.class);

        ObjectMapper jsonWriter = new ObjectMapper();
        return jsonWriter.writeValueAsString(obj);
    }
}
