/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.ccdb.service;

import eu.ess.ics.ccce.ccdb.model.CCDBIocResponse;
import eu.ess.ics.ccce.ccdb.model.NameDetail;
import eu.ess.ics.ccce.exceptions.CCDBException;
import eu.ess.ics.ccce.exceptions.ParseException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.exceptions.ServiceException;
import eu.ess.ics.ccce.rest.model.response.NameResponse;
import eu.ess.ics.ccce.service.HttpClientService;
import eu.ess.ics.ccce.util.Utils;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Service
public class CCDBService {

    private static final String FIND_IOC_URL = "rest/slots";
    private static final String DEVICE_TYPE = "deviceType";
    private static final String FIND_BY_ID_URL = "rest/slots/id/";
    private static final String IOC = "IOC";

    @Value("${ccdb.base.url}")
    private String baseUrl;

    private final Logger logger = LoggerFactory.getLogger(CCDBService.class);

    private final HttpClientService httpClientService;

    @Autowired
    public CCDBService(HttpClientService httpClientService) {
        this.httpClientService = httpClientService;
    }

    /**
     * Queries all 'IOC' type slots from CCDB to get names from it.
     * Lookups are cached for a short amount of time.
     *
     * @return all name ID and naming-name from CCDB
     *
     * @throws ServiceException if problem occurs when building the URL,
     *      or building up the connection with CCDB.
     * @throws CCDBException if CCDB responseCode is not 2xx.
     */

    @Cacheable(value = "ccdb_all_ioc")
    public List<NameResponse> getAllIocs() throws ServiceException, CCDBException {
        List<NameResponse> result = Collections.emptyList();

        logger.debug("Getting all IOC list from CCDB");
        final OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

        try {
            String hostUrl = buildQueryAllIOCUrl();

            HttpClientService.ServiceResponse<CCDBIocResponse> ccdbResponse =
                    httpClientService.executeGetRequest(client, Headers.of(), hostUrl,
                            HttpClientService.JSON_MEDIA_TYPE, CCDBIocResponse.class);

            if (HttpClientService.isSuccessHttpStatusCode(ccdbResponse.getStatusCode())) {
                CCDBIocResponse ccdbEntity = ccdbResponse.getEntity();

                if (ccdbEntity.getInstallationSlots() != null) {
                    result = ccdbEntity.getInstallationSlots()
                            .stream()
                            .map(n -> new NameResponse(n.getId(), n.getName()))
                            .collect(Collectors.toList());
                }

                return result;
            }

            logger.error("Can not get IOCs from CCDB, errorCode: [{}]",
                    ccdbResponse.getStatusCode());
            throw new CCDBException("Can not get names from CCDB");
        } catch (MalformedURLException | RemoteServiceException | ParseException e) {
            logger.error("Error while trying to fetch CCDB all IOC list", e);
            throw new ServiceException("Error while trying to fetch CCDB all IOC list");
        }
    }

    /**
     * Looks up an entry in CCDB by ID, and gives back NamingDetails.
     * Lookups are cached for a short amount of time.
     *
     * @param iocId the CCDB ID that has to be looked up.
     *
     * @return the namingDetails from CCDB.
     *
     * @throws ServiceException if problem occurs when building the URL,
     *      or building up the connection with CCDB.
     * @throws CCDBException if CCDB responseCode is not 2xx.
     */

    @Cacheable(value = "ioc_by_id")
    public NameDetail getIocById(Long iocId) throws ServiceException, CCDBException {
        logger.debug("Getting a specific IOC from CCDB, ID:[{}]", iocId);
        final OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());

        try {
            String hostUrl = buildQueryByIdUrl(iocId);

            HttpClientService.ServiceResponse<NameDetail> ccdbResponse =
                    httpClientService.executeGetRequest(client, Headers.of(), hostUrl,
                    HttpClientService.JSON_MEDIA_TYPE, NameDetail.class);

            if (HttpClientService.isSuccessHttpStatusCode(ccdbResponse.getStatusCode())) {
                return ccdbResponse.getEntity();
            }

            logger.error("Can not get IOC by ID from CCDB, errorCode: [{}], ID: [{}]",
                    ccdbResponse.getStatusCode(), iocId);
            throw new CCDBException("Can not get name from CCDB by ID");
        } catch (MalformedURLException | RemoteServiceException | ParseException e) {
            logger.error("Error while trying to fetch IOC from CCDB", e);
            throw new ServiceException("Error while trying to fetch IOC from CCDB");
        }
    }

    /**
     * Searches for iocNames in CCDB system. It fetches all 'IOC' type slots.
     * The name is searched via 'contains', without case-sensitivity.
     * If no result found empty list will be the response.
     * (The IOCs are cached for a short amount of time.)
     * If iocName  param is empty -> full list will go back.
     *
     * @param iocName the part of the iocName that has to be looked up.
     *
     * @return IOC names from CCDB. If iocName param is empty -> full list will go back.
     *       Name is looked up via "contains", without case-sensitivity.
     *      If no IOC found -> empty list will be the result.
     *
     * @throws ServiceException if problem occurs when building the URL,
     *      or building up the connection with CCDB.
     * @throws CCDBException if CCDB responseCode is not 2xx.
     */
    public List<NameResponse> filterByIocName(String iocName)
            throws ServiceException, CCDBException {
        List<NameResponse> allIocs = getAllIocs();

        if (StringUtils.isEmpty(iocName)) {
            return allIocs;
        }

        return allIocs.stream()
                .filter(name -> name.getName().toUpperCase().contains(iocName.toUpperCase()))
                .collect(Collectors.toList());
    }

    private String getEndpointUrl(String endpointPostfix) throws MalformedURLException {
        return new URL(new URL(baseUrl), endpointPostfix).toString();
    }

    private String buildQueryAllIOCUrl()
            throws MalformedURLException {

        UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(
                getEndpointUrl(FIND_IOC_URL));

        //additional query parameter
        ub.queryParam(DEVICE_TYPE, IOC);

        return ub.toUriString();
    }

    private String buildQueryByIdUrl(Long iocId)
            throws MalformedURLException {

        UriComponentsBuilder ub = UriComponentsBuilder.fromUriString(
                getEndpointUrl(FIND_BY_ID_URL + iocId));

        return ub.toUriString();
    }
}
