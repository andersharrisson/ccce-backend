package eu.ess.ics.ccce.model;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 **/
public class DeploymentDto {
    private final String iocInventoryName;
    private final String iocVersionSourceUrl;
    private final String iocVersionSourceVersion;
    private final String iocVersionEpicsVersion;
    private final String iocVersionRequireVersion;
    private final String host;
    private final boolean active;
    private final boolean intended;

    public DeploymentDto(String iocInventoryName, String iocVersionSourceUrl, String iocVersionSourceVersion,
                         String iocVersionEpicsVersion, String iocVersionRequireVersion,
                         String host, boolean active, boolean intended) {
        this.iocInventoryName = iocInventoryName;
        this.iocVersionSourceUrl = iocVersionSourceUrl;
        this.iocVersionSourceVersion = iocVersionSourceVersion;
        this.iocVersionEpicsVersion = iocVersionEpicsVersion;
        this.iocVersionRequireVersion = iocVersionRequireVersion;
        this.host = host;
        this.active = active;
        this.intended = intended;
    }

    public String getIocInventoryName() {
        return iocInventoryName;
    }

    public String getIocVersionSourceUrl() {
        return iocVersionSourceUrl;
    }

    public String getIocVersionSourceVersion() {
        return iocVersionSourceVersion;
    }

    public String getIocVersionEpicsVersion() {
        return iocVersionEpicsVersion;
    }

    public String getIocVersionRequireVersion() {
        return iocVersionRequireVersion;
    }

    public String getHost() {
        return host;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isIntended() {
        return intended;
    }

    @Override
    public String toString() {
        return "Deployment record -"
                + " IOC:"  + iocInventoryName
                + ", VERSION: " + iocVersionSourceUrl + ": " + iocVersionSourceVersion
                + ", HOST: " + host
                + ", ACTIVE: " + active
                + ", INTENDED: " + intended;
    }
}
