/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.prometheus;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Metric {

    @JsonProperty("__name__")
    private String __name__;
    @JsonProperty("csentry_device_type")
    private String csentry_device_type;
    @JsonProperty("csentry_domain")
    private String csentry_domain;
    @JsonProperty("csentry_group_cslab_generallab")
    private String csentry_group_cslab_generallab;
    @JsonProperty("csentry_group_iocs")
    private String csentry_group_iocs;
    @JsonProperty("csentry_group_labnetworks")
    private String csentry_group_labnetworks;
    @JsonProperty("csentry_group_servers")
    private String csentry_group_servers;
    @JsonProperty("csentry_group_virtualmachine")
    private String csentry_group_virtualmachine;
    @JsonProperty("csentry_is_ioc")
    private String csentry_is_ioc;
    @JsonProperty("csentry_vlan")
    private String csentry_vlan;
    @JsonProperty("csentry_vlan_id")
    private String csentry_vlan_id;
    @JsonProperty("instance")
    private String instance;
    @JsonProperty("job")
    private String job;
    @JsonProperty("name")
    private String name;
    @JsonProperty("state")
    private String state;
    @JsonProperty("type")
    private String type;
    @JsonProperty("ioc")
    private String ioc;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("__name__")
    public String get__name__() {
        return __name__;
    }

    @JsonProperty("__name__")
    public void set__name__(String __name__) {
        this.__name__ = __name__;
    }

    @JsonProperty("csentry_device_type")
    public String getCsentry_device_type() {
        return csentry_device_type;
    }

    @JsonProperty("csentry_device_type")
    public void setCsentry_device_type(String csentry_device_type) {
        this.csentry_device_type = csentry_device_type;
    }

    @JsonProperty("csentry_domain")
    public String getCsentry_domain() {
        return csentry_domain;
    }

    @JsonProperty("csentry_domain")
    public void setCsentry_domain(String csentry_domain) {
        this.csentry_domain = csentry_domain;
    }

    @JsonProperty("csentry_group_cslab_generallab")
    public String getCsentry_group_cslab_generallab() {
        return csentry_group_cslab_generallab;
    }

    @JsonProperty("csentry_group_cslab_generallab")
    public void setCsentry_group_cslab_generallab(String csentry_group_cslab_generallab) {
        this.csentry_group_cslab_generallab = csentry_group_cslab_generallab;
    }

    @JsonProperty("csentry_group_iocs")
    public String getCsentry_group_iocs() {
        return csentry_group_iocs;
    }

    @JsonProperty("csentry_group_iocs")
    public void setCsentry_group_iocs(String csentry_group_iocs) {
        this.csentry_group_iocs = csentry_group_iocs;
    }

    @JsonProperty("csentry_group_labnetworks")
    public String getCsentry_group_labnetworks() {
        return csentry_group_labnetworks;
    }

    @JsonProperty("csentry_group_labnetworks")
    public void setCsentry_group_labnetworks(String csentry_group_labnetworks) {
        this.csentry_group_labnetworks = csentry_group_labnetworks;
    }

    @JsonProperty("csentry_group_servers")
    public String getCsentry_group_servers() {
        return csentry_group_servers;
    }

    @JsonProperty("csentry_group_servers")
    public void setCsentry_group_servers(String csentry_group_servers) {
        this.csentry_group_servers = csentry_group_servers;
    }

    @JsonProperty("csentry_group_virtualmachine")
    public String getCsentry_group_virtualmachine() {
        return csentry_group_virtualmachine;
    }

    @JsonProperty("csentry_group_virtualmachine")
    public void setCsentry_group_virtualmachine(String csentry_group_virtualmachine) {
        this.csentry_group_virtualmachine = csentry_group_virtualmachine;
    }

    @JsonProperty("csentry_is_ioc")
    public String getCsentry_is_ioc() {
        return csentry_is_ioc;
    }

    @JsonProperty("csentry_is_ioc")
    public void setCsentry_is_ioc(String csentry_is_ioc) {
        this.csentry_is_ioc = csentry_is_ioc;
    }

    @JsonProperty("csentry_vlan")
    public String getCsentry_vlan() {
        return csentry_vlan;
    }

    @JsonProperty("csentry_vlan")
    public void setCsentry_vlan(String csentry_vlan) {
        this.csentry_vlan = csentry_vlan;
    }

    @JsonProperty("csentry_vlan_id")
    public String getCsentry_vlan_id() {
        return csentry_vlan_id;
    }

    @JsonProperty("csentry_vlan_id")
    public void setCsentry_vlan_id(String csentry_vlan_id) {
        this.csentry_vlan_id = csentry_vlan_id;
    }

    @JsonProperty("instance")
    public String getInstance() {
        return instance;
    }

    @JsonProperty("instance")
    public void setInstance(String instance) {
        this.instance = instance;
    }

    @JsonProperty("job")
    public String getJob() {
        return job;
    }

    @JsonProperty("job")
    public void setJob(String job) {
        this.job = job;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("ioc")
    public String getIoc() {
        return ioc;
    }

    @JsonProperty("ioc")
    public void setIoc(String ioc) {
        this.ioc = ioc;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
