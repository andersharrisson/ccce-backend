/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ccce.prometheus;

import eu.ess.ics.ccce.exceptions.PrometheusException;
import eu.ess.ics.ccce.exceptions.RemoteServiceException;
import eu.ess.ics.ccce.service.HttpClientService;
import eu.ess.ics.ccce.util.ConversionUtil;
import eu.ess.ics.ccce.util.Utils;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 **/
@Component
public class PrometheusRepository {
    private static final String PROMETHEUS_HOST = "prometheus.server.address";
    private static final String PROMETHEUS_URL_POSTFIX = "api/v1/";
    private static final String PROMETHEUS_IOC_QUERY_PREFIX = "query?query=node_systemd_unit_state{name=~\"ioc@";
    private static final String PROMETHEUS_ANY_ACTIVE_IOC_POSTFIX = ".*\", state=~\"active\"}";
    private static final String PROMETHEUS_IOC_QUERY_POSTFIX = ".service\", state=\"active\"";
    private static final String PROMETHEUS_IOC_HOST_FILTER = "instance=\"";
    private static final String PROMETHEUS_REQUEST_POSTFIX = "}";

    private static final String PROMETHEUS_HOST_QUERY_PREFIX = "query?query=up{instance=\"";
    private static final String PROMETHEUS_HOST_QUERY_POSTFIX = "\", job=\"node\"}";
    private static final String PROMETHEUS_HOSTS_QUERY = "query?query=up{job=\"node\",csentry_is_ioc=\"1\"}";

    private static final String PROMETHEUS_E3_DIRTY_REPO_QUERY_PREFIX = "query?query=e3_repository_dirty{ioc=\"";
    private static final String PROMETHEUS_ALL_E3_DIRTY_REPO_QUERY = "query?query=e3_repository_dirty";
    private static final String PROMETHEUS_E3_LOCAL_COMMITS_QUERY_PREFIX = "query?query=e3_repository_local_commits{ioc=\"";
    private static final String PROMETHEUS_ALL_E3_LOCAL_COMMITS_QUERY = "query?query=e3_repository_local_commits";

    Logger logger = LoggerFactory.getLogger(PrometheusRepository.class);

    private final Environment env;
    private final HttpClientService httpClientService;

    @Autowired
    public PrometheusRepository(Environment env, HttpClientService httpClientService) {
        this.env = env;
        this.httpClientService = httpClientService;
    }

    private String buildUrl(String prefix, String name, String postFix) throws MalformedURLException {
        return new URL(new URL(env.getProperty(PROMETHEUS_HOST) + PROMETHEUS_URL_POSTFIX),
                prefix + name + postFix).toString();
    }

    private String buildUrl(String endpointPostfix) throws MalformedURLException {
        return new URL(new URL(env.getProperty(PROMETHEUS_HOST) + PROMETHEUS_URL_POSTFIX),
                endpointPostfix).toString();
    }

    /**
     * Fetches all dirty IOCs from Prometheus.
     * Response will be cached for a short amount of time.
     *
     * @return response from Prometheus for fetching dirty IOCS.
     *         If error occurs during request an empty list will be the response.
     */
    @Cacheable(value = "dirty_iocs")
    public List<Result> allDirtyIoc() {
        logger.debug("Trying to fetch all dirty IOCs from Prometheus");

        try {

            return fetchPrometheusResult(PROMETHEUS_ALL_E3_DIRTY_REPO_QUERY);

        } catch (PrometheusException | RemoteServiceException | MalformedURLException e) {
            logger.error("Can not get Dirty IOC list from Prometheus", e);
        } catch (Exception e) {
            logger.error("Unexpected error while fetching all Dirty IOCs from Prometheus", e);
        }

        return Collections.emptyList();
    }

    /**
     * Fetches all IOCs from Prometheus.
     * Response will be cached for a short period of time.
     *
     * @return Prometheus response for fetching all IOCs.
     *         If error occurs during fetching an empty list will be the response.
     */
    @Cacheable(value = "prometheus_active_ioc_cache")
    public List<Result> listActiveIocs() {
        logger.debug("Trying to fetch all IOCs from Prometheus");

        try {

            return fetchPrometheusResult(PROMETHEUS_IOC_QUERY_PREFIX + PROMETHEUS_ANY_ACTIVE_IOC_POSTFIX);

        } catch (MalformedURLException | PrometheusException | RemoteServiceException e) {
            logger.error("Prometheus service or active IOC status information can't be reached", e);
        } catch (Exception e) {
            logger.error("Unexpected error during active IOCs listing from Prometheus", e);
        }
        return Collections.emptyList();
    }

    /**
     * Lists all active IOC hosts from Prometheus.
     * Response is cached for a short period of time on the backend.
     *
     * @return List of active IOC hosts. If no hosts van be found empty list will be responded.
     */
    @Cacheable(value = "prometheus_active_ioc_host_cache")
    public List<Result> listActiveIocHosts() {
        try {

            return fetchPrometheusResult(PROMETHEUS_HOSTS_QUERY);

        } catch (MalformedURLException | PrometheusException | RemoteServiceException e) {
            logger.error("Prometheus service or ioc host status information cannot be reached", e);
        } catch (Exception e) {
            logger.error("Unexpected error during active ioc hosts listing from Prometheus", e);
        }
        return Collections.emptyList();
    }

    /**
     * Fetches all IOC hosts from Prometheus. The results are cached for a short period of time.
     *
     * @return fetched response from Prometheus for all IOC hosts.
     * If error occurs an empty list will be the response.
     */
    @Cacheable(value = "prometheus_all_ioc_host")
    public List<Result> listAllHost() {
        logger.debug("Trying to fetch all IOC host from Prometheus");

        try {

            return fetchPrometheusResult(PROMETHEUS_HOSTS_QUERY);

        } catch (PrometheusException | RemoteServiceException | MalformedURLException e) {
            logger.error("Error while trying to fetch all IOC hosts", e);
        } catch (Exception e) {
            logger.error("Unexpected error while fetching all hosts from Prometheus", e);
        }

        return Collections.emptyList();
    }

    /**
     * Fetches all local commits for all IOCs from Prometheus.
     * Response will be cached for a short amount of time.
     *
     * @return response from Prometheus for fetching local commits for all IOCS.
     *         If error occurs during request an empty list will be the response.
     */
    @Cacheable(value = "local_commits")
    public List<Result> allLocalCommits() {
        logger.debug("Trying to fetch all IOC local commits from Prometheus");

        try {

            return fetchPrometheusResult(PROMETHEUS_ALL_E3_LOCAL_COMMITS_QUERY);

        } catch (PrometheusException | RemoteServiceException | MalformedURLException e) {
            logger.error("Can not get Local Commit list from Prometheus", e);
        } catch (Exception e) {
            logger.error("Unexpected error while fetching Commit list from Prometheus", e);
        }

        return Collections.emptyList();
    }

    @Cacheable(value = "prometheus_host_cache")
    public List<String> listIocHosts() {
        try {
            PrometheusResponse response = callPrometheusForResult(buildUrl(PROMETHEUS_HOSTS_QUERY));
            return response.getData().getResult().stream().map(r -> r.getMetric().getInstance()).collect(Collectors.toList());
        } catch (MalformedURLException | PrometheusException | RemoteServiceException e) {
            logger.error("Prometheus service or host status information cannot be reached", e);
        } catch (Exception e) {
            logger.error("Unexpected error during active hosts listing from Prometheus", e);
        }
        return Collections.emptyList();
    }

    /**
     *  Gets status information about specific IOC from Prometheus.
     *  Requests are cached for a small amount of time (for specific host) in order to reduce network traffic!
     *  (Checks for .service along with state=active flag in Prometheus)
     *  IF POSSIBLE, PROVIDE HOST TO CHECK ON FOR IOC!
     *
     * @param iocName the name of the ioc for which the status has to be determined
     * @return <code>true</code>, if ioc is available
     *         <code>false</code>, if ioc is not available
     *         <code>null</code> if there was a problem during the Prometheus query
     */
    @Cacheable(value = "prometheus_ioc_cache")
    public List<Result> checkIocIsActive(String iocName) {

        try {

            return callPrometheus(buildUrl(PROMETHEUS_IOC_QUERY_PREFIX,
                    ConversionUtil.transformIocName(iocName),
                    PROMETHEUS_IOC_QUERY_POSTFIX + PROMETHEUS_REQUEST_POSTFIX));

        } catch (MalformedURLException e) {
            logger.error("Wrong IOC status URL format", e);
        }

        return null;
    }

    private List<Result> callPrometheus(String url) {
        logger.debug("Checking status with prometheus: {}", url);

        try {
            PrometheusResponse promEnt = callPrometheusForResult(url);
            return promEnt.getData().getResult();
        } catch (PrometheusException e) {
            logger.error("Prometheus host status query failed", e);
        } catch (RemoteServiceException e) {
            logger.error("Prometheus service cannot be reached", e);
        }

        return Collections.emptyList();
    }

    private PrometheusResponse callPrometheusForResult(String url)
            throws PrometheusException, RemoteServiceException {
        logger.debug("Call prometheus: {}", url);

        OkHttpClient client = Utils.getTestOnlyClient(new OkHttpClient());
        HttpClientService.ServiceResponse<PrometheusResponse> prometheusResponse =
                httpClientService.executeGetRequest(
                        client, Headers.of(), url, PrometheusResponse.class);

        if (HttpClientService.isSuccessHttpStatusCode(prometheusResponse.getStatusCode())) {
            return prometheusResponse.getEntity();
        }

        throw new PrometheusException(
                "Error during getting Prometheus response (URL: " + url + "), status code: "
                        + prometheusResponse.getStatusCode());
    }

    private List<Result> fetchPrometheusResult(String url)
            throws MalformedURLException, RemoteServiceException, PrometheusException {

        PrometheusResponse response = callPrometheusForResult(
                buildUrl(url));

        return response
                .getData()
                .getResult();
    }
}
