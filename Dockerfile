FROM tomcat:9.0.20-jre8-alpine
LABEL maintainer="anders.harrisson@ess.eu"

# Remove ROOT folder
RUN rm -rf /usr/local/tomcat/webapps/ROOT

# Deployment unit
ADD target/*.war /usr/local/tomcat/webapps/ROOT.war

EXPOSE 8080
