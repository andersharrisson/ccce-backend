This tool is meant to help integrators with deploying one or more IOCs, and monitor hosts an IOCs.

## Developing environment:
* Java JDK 8
* Maven 3.0+
* PostgreSQL DB
* IDE for Java (IntelliJ, Visual Studio Code,...)

## Docker

There are 2 docker files contained in the project:
- `Dockerfile`
- `Dockerfile-build`

`Dockerfile` is used when WAR file is already created, and it needs to be deployed to an application server.

`Dockerfile-build` builds, and hosts the server (without the DB)!

#### Command to build docker-image:

`docker build -f Dockerfile-build -t <tagName> .` - you can use any kind of name in `tagName`.

#### Command to run docker-image:
`docker run -p 8080:8080 <tagName>` - it also exposes the port `8080`.

### Environment Variables
| Environment variable     | Default    | Description |
| -------------------------|------------|-------------|
| CCCE_DB_USERNAME | _ccceuser_ | User name for the DB connection |
| CCCE_DB_PASSWORD | _cccepassword_ | Password for the DB connection |
| CCCE_DB_URL | _jdbc:postgresql://localhost:5432/ccce_db_ | JDBC URL for the DB connection |
| CCCE_DB_MAX_POOL_SIZE | _25_ | The maximum size of the DB connection pool |
| CCCE_DB_MIN_POOL_SIZE | _15_ | The minimum size of the DB connection pool |
| CCCCE_DB_POOL_NAME | _ccce_connection_pool_ | Name of the DB connection pool (for monitoring purpose |
| CCCE_DB_KEEPALIVE_INT | _30000_ | DB connection keepalive sending interval (ms) |
| CCCE_DB_CONN_TIMEOUT | _30000_ | DB connection timeout interval (ms) |
| CCCE_DB_IDLE_TIMEOUT | _600000_ | DB connection IDLE timeout |
| CCCE_DB_MAX_LIFETIME | _1800000_ | DB connection max lifetime (ms) |
| API_PATH | _/ccce-api_ | Path of the OpenAPI documentation |
| SWAGGER_PATH | _/ccce-api.html_ | Path of the swagger-ui HTML documentation|
| GITLAB_HOST | _https://gitlab.esss.lu.se/_ | Path of the GitLab server for getting IOCs|
| GITLAB_TECHNICAL_USER_TOKEN | | The technical user's GitLab token to use the GiLab REST API endpoints | 
| OPENAPI_DEV_SERVER | _http://localhost:8080_ | Development server address that will be generated into the swagger.json |
| OPENAPI_TEST_SERVER | _https://ccce-test-02.cslab.esss.lu.se/api_ | Test server address that will be generated into the swagger.json |
| AWX_HOST | _https://awx-lab-01.cslab.esss.lu.se_ | AWX server address |
| AWX_TOKEN | | The AWX-technical user's token |
| AWX_JOB_TEMPLATE_NAME | _ccce-dummy-playbook_ | The name of the AWX playbook-template that the deployment tool will try to run |
| CSENTRY_HOST | _https://csentry-test.esss.lu.se_ | CSentry server address |
| CSENTRY_TOKEN | | Token used for communicating with the CSentry server |
| RESPONSE_MAX_LIMIT | _150_ | The maximum size of the response-list that the deployment server will give for any request |
| PROMETHEUS_HOST | _https://prometheus-01.tn.esss.lu.se/_ | The URL of the Prometheus server |
| CACHE_EXP_IN_SEC | _6_ | The TTL interval (in seconds) for every object that gets in the internal Caffeine cache |
| RBAC_SERVER_ADDRESS | _https://icsvd-app01.esss.lu.se:8453/rbac/service-test/_ | The RBAC server address used for log in users |
| JWT_SECRET | _secret_password_to_hash_token_ | Secret that is used sign the JWT token |
| JWT_EXP_MIN | _240_ | Time interval until the JWT token is valid (in minutes) |
| AES_KEY | _aes_secret_key_ | Secret that is used to hash the tokens in the JWT token |
| IOC_METADATA_PATH | _ioc.yml_ | The relative path to the IOC metadata file in the GitLab repo that contains mandatory information to create/update/deploy an IOC |
| LOG_CONFIG_FILE | _classpath:logback-spring.xml_ | Path to the external logback-config file.|
| CCDB_BASE_URL | _https://ccdb.esss.lu.se/_ | The CCDB base URL (including the ending '/' character) |
